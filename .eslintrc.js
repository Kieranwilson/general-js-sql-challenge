module.exports = {
  extends: ["prettier"],
  plugins: ["prettier"],
  rules: {
    "prettier/prettier": "error",
    "max-len": "off"
  },
  "parserOptions": {
    ecmaVersion: 2018,
    "sourceType": "module"
  },
  "overrides": [
    {
      "files": [ "**/__mocks__/*.js", "**/__tests__/*.js", "*.test.js", "setupTests.js" ],
      "env": {
        "jest": true
      }
    }
  ]
};
