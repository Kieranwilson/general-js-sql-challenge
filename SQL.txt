Question 5 (1 point, SQL)
## A. Return the names of all salespeople that have an order with George

SELECT Salesperson.Name
FROM Salesperson
JOIN Orders ON Salesperson.SalespersonID = Orders.SalespersonID
JOIN Customer on Customer.CustomerID = Orders.CustomerID
WHERE Customer.Name = 'George';


## B. Return the names of all salespeople that do not have any order with George

SELECT Salesperson.Name
FROM Salesperson
WHERE Salesperson.SalespersonID NOT IN (
    SELECT Orders.SalespersonID
    FROM Orders JOIN Customer ON Orders.CustomerID = Customer.CustomerID
    WHERE Customer.Name = 'George'
);


## C. Return the names of salespeople that have 2 or more orders.

SELECT Salesperson.Name
FROM Salesperson
JOIN Orders ON Salesperson.SalespersonID = Orders.SalespersonID
GROUP BY Orders.SalespersonID
HAVING COUNT(*) >= 2;

Question 6 (1 point, SQL)
## A. Return the name of the salesperson with the 3rd highest salary.

SELECT Name
FROM Salesperson
ORDER BY Salary
DESC LIMIT 2,1

## B. Create a new roll­up table BigOrders(where columns are CustomerID, TotalOrderValue),
## and insert into that table customers whose total Amount across all orders is greater than 1000
CREATE TABLE BigOrders AS
SELECT Orders.CustomerID, Sum(Orders.CostOfUnit*Orders.NumberOfUnits) as TotalOrderValue
FROM Orders
GROUP BY Orders.CustomerID
HAVING SUM(Orders.CostOfUnit * Orders.NumberOfUnits) > 1000;

## C. Return the total Amount of orders for each month, ordered by year, then month (both in descending order)

SELECT Year(Orders.OrderDate) as Year, Month(Orders.OrderDate) as Month, COUNT(OrderID) as TotalOrders
FROM Orders
GROUP BY Year(Orders.OrderDate), Month(Orders.OrderDate)
ORDER BY Year(Orders.OrderDate) DESC, Month(Orders.OrderDate) DESC
