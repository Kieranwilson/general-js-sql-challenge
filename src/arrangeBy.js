/**
 * Write a function which combines an array of objects, grouped by a key you
 * provide (this key will correspond to a key found in the objects. The
 * function will index the new object with the value of those keys.
 */

const curryFunction = (fn, ...args) =>
  // Do we have enough arguments to satisfy the curried function?
  fn.length <= args.length
    ? // We do so now call it
      fn(...args)
    : // We do not, return a function to accept more arguments until we are satisfied
      (...more) => curryFunction(fn, ...args, ...more);

/**
 Requirements
 The function must not mutate the original array, or any of the objects in the array.
 If the key you provided is not present in an object, exclude that from the output.
 If an element of the array is null/undefined, or not an object ­ exclude that from the output.
 It should handle one or more objects being attached to a key.
 Finally, write the function so it can store the key, and later be invoked with an array.
 Example:
 const arrangeByName = arrangeBy('name'); arrangeByName(users);
 Please provide unit tests which prove this functionality.
 */
const arrangeBy = (key, data) =>
  data.reduce((accumulated, current) => {
    if (!current || !current[key]) {
    } else if (!accumulated[current[key]]) {
      accumulated[current[key]] = [current];
    } else {
      accumulated[current[key]].push(current);
    }
    return accumulated;
  }, {});

export default curryFunction(arrangeBy);
