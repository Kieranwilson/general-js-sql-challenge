/**
 * Write a function that takes a single positive integer, and returns a
 * collection / sequence (e.g. array) of integers. The return value should
 * contain those integers that are positive divisors of the input integer.
 */

export const firstAttempt = num => {
  // We know that the highest positive number other then itself will be the sqrt
  const sqRt = Math.sqrt(num);
  const arr = [];
  // So we go from 1 up until the square root
  for (let i = 1; i <= sqRt; i++) {
    if (num % i === 0) {
      arr.push(i);
      if (num !== i && i !== sqRt) {
        arr.push(num / i);
      }
    }
  }
  return arr.sort((a, b) => a - b);
};

// So the above was fine but then I read about prime factorization and how
// it works, so I wanted to have a comparison between the two. Below is the prime
// factorization approach to find the divisors which was faster

export const findPrimeFactorization = num => {
  let primeFactors = [];
  let workingNum = num;
  for (let prime = 2; workingNum >= prime * prime; ) {
    if (workingNum % prime === 0) {
      let exponent = 0;
      do {
        workingNum /= prime;
        exponent++;
      } while (workingNum % prime === 0);
      primeFactors.push([prime, exponent]);
    } else {
      prime += 1;
    }
  }
  if (workingNum !== 1) {
    primeFactors.push([workingNum, 1]);
  }
  return primeFactors;
};

export const findDivisorsFromPrimeFactors = primeFactors => {
  const recursiveMultiplication = (currentDivisor, currentResult, currentArray) => {
    if (currentDivisor === primeFactors.length) {
      currentArray.push(currentResult);
      return;
    }

    for (let exponent = 0; exponent <= primeFactors[currentDivisor][1]; exponent++) {
      recursiveMultiplication(currentDivisor + 1, currentResult, currentArray);
      currentResult *= primeFactors[currentDivisor][0];
    }
    return currentArray;
  };
  return recursiveMultiplication(0, 1, []).sort((a, b) => a - b);
};

export const findFactors = num => {
  if (num < 4) {
    if (num < 1) {
      throw new Error('Not a positive number');
    }
    // Because I start at 2 and use prime squared, for efficiency
    // I would just manually handle cases less then 4
    return num === 1 ? [1] : [1, num];
  }
  const primeFactors = findPrimeFactorization(num);
  return findDivisorsFromPrimeFactors(primeFactors);
};

export default findFactors;
