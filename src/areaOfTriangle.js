/**
 * Write a function that takes three integer inputs and returns a single output.
 * The inputs are the lengths of the sides of a triangle. The output is the area
 * of that triangle.
 */

export class InvalidTriangleException extends Error {
  constructor(a, b, c) {
    super(`${a}, ${b}, ${c} cannot form a valid triangle`);
    this.name = 'InvalidTriangleException'; // (2)
  }
}

/**
 * Using Heron's Formula, which is Area	=	 √	p(p−a)(p−b)(p−c)
 * And p is half the perimeter, or (a + b + c) / 2
 */
const calcPerimeter = (a, b, c) => (a + b + c) / 2;
const isInvalidNumber = num => isNaN(num) || num <= 0;

export default (...sides) => {
  const [a, b, c] = sides.map(Number);
  if ([a, b, c].some(isInvalidNumber) || a + b <= c || a + c <= b || c + b <= a) {
    throw new InvalidTriangleException(...sides);
  }
  const p = calcPerimeter(a, b, c);
  return Math.sqrt(p * (p - a) * (p - b) * (p - c));
};
