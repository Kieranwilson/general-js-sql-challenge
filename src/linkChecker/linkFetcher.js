import EventEmitter from 'events';
import axios from 'axios';

const myEmitter = new EventEmitter();

const axiosStatusCode = url =>
  new Promise((resolve, reject) => {
    axios
      .get(url)
      .then(response => resolve(response.status))
      .catch(error => {
        if (error.response && error.response.status) {
          return resolve(error.response.status);
        }
        return reject(error);
      });
  });

const memoryCache = {};

export default url =>
  new Promise(async resolve => {
    if (memoryCache[url] && memoryCache[url].data !== undefined) {
      // We have done this call already
      return resolve(memoryCache[url].data);
    }
    if (memoryCache[url] && memoryCache[url].pending) {
      // Wait until the call resolves then resolve with the same result
      myEmitter.once(`request_${url}_resolved`, resolve);
    } else {
      memoryCache[url] = {
        pending: true
      };
      try {
        memoryCache[url].data = await axiosStatusCode(url);
        myEmitter.emit(`request_${url}_resolved`, memoryCache[url].data);
        return resolve(memoryCache[url].data);
      } catch (e) {
        memoryCache[url].data = new Error('failure');
        memoryCache[url].data.error = e;
        myEmitter.emit(`request_${url}_resolved`, memoryCache[url].data);
        return resolve(memoryCache[url].data);
      }
    }
  });
