const successFulPromise = data => ({ success: true, data });
const unSuccessFulPromise = data => ({ success: false, data });

/**
 * @typedef PromiseReturn
 * @property {boolean} success True if the promise resolved
 * @property {*} data The result of the promise, reject or resolve value
 */

/**
 * Wrap all promises so that one rejection does not kill off the rest
 * @param  {Promise[]} promises An array of promises to wrap
 * @return {Promise<PromiseReturn[]>} The wrapped promises with the specified structure
 */
export default promises => {
  // Wrap all Promises in a Promise that will always "resolve"
  const resolvingPromises = promises.map(
    promise =>
      new Promise(resolve => {
        let resVal;
        promise
          .then(result => (resVal = successFulPromise(result)))
          .catch(result => (resVal = unSuccessFulPromise(result)))
          .then(resolve);
      })
  );

  // Execute all wrapped Promises
  return Promise.all(resolvingPromises);
};
