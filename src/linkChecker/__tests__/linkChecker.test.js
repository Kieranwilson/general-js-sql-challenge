import linkChecker from '../index';
import axios from 'axios';
import { npm, w3schools } from './testData';

describe('linkChecker', () => {
  /** I would normally mock out axios to not make the calls but I spent enough time
   * on this question already */
  it('Should work for npm', async () => {
    // let it take up to 30s
    jest.setTimeout(30000);
    const data = await linkChecker(npm, 'https://www.npmjs.com/package/node-html-parser');
    expect(data.success.length).toBe(92);
    expect(data.failure.length).toBe(1);
  });
});
