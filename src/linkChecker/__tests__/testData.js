export const npm = `<!doctype html>
<html>
<head>
<meta charset="utf-8">
    <link rel="stylesheet" href="doesnotexist.js" />
  <link rel="stylesheet" href="https://static.npmjs.com/styles.b1f46f881f2ce6ef8915.css" />
  <title data-react-helmet="true">node-html-parser  -  npm</title>
  <meta data-react-helmet="true" http-equiv="cleartype" content="on"/><meta data-react-helmet="true" name="apple-mobile-web-app-capable" content="yes"/><meta data-react-helmet="true" name="viewport" content="width=device-width,minimum-scale=1.0,initial-scale=1,user-scalable=yes"/><meta data-react-helmet="true" property="og:image" content="https://static.npmjs.com/338e4905a2684ca96e08c7780fc68412.png"/><meta data-react-helmet="true" name="msapplication-TileColor" content="#cb3837"/><meta data-react-helmet="true" name="msapplication-TileImage" content="https://static.npmjs.com/7a7ffabbd910fc60161bc04f2cee4160.png"/><meta data-react-helmet="true" name="msapplication-config" content="https://static.npmjs.com/668aac888e52ae13cac9cfd71fabd31f.xml"/><meta data-react-helmet="true" name="theme-color" content="#cb3837"/><meta data-react-helmet="true" name="description" content="A very fast HTML parser, generating a simplified DOM, with basic element query support."/><meta data-react-helmet="true" property="og:description" content="A very fast HTML parser, generating a simplified DOM, with basic element query support."/><meta data-react-helmet="true" property="og:title" content="node-html-parser"/><meta data-react-helmet="true" property="og:url" content="https://www.npmjs.com/package/node-html-parser"/><meta data-react-helmet="true" property="og:site_name" content="npm"/><meta data-react-helmet="true" name="keywords" content="fast html parser nodejs typescript"/><meta data-react-helmet="true" name="twitter:card" content="summary"/><meta data-react-helmet="true" name="twitter:url" content="https://www.npmjs.com/package/node-html-parser"/><meta data-react-helmet="true" name="twitter:title" content="npm: node-html-parser"/><meta data-react-helmet="true" name="twitter:description" content="A very fast HTML parser, generating a simplified DOM, with basic element query support."/>
  <link data-react-helmet="true" rel="apple-touch-icon" sizes="120x120" href="https://static.npmjs.com/58a19602036db1daee0d7863c94673a4.png"/><link data-react-helmet="true" rel="apple-touch-icon" sizes="144x144" href="https://static.npmjs.com/7a7ffabbd910fc60161bc04f2cee4160.png"/><link data-react-helmet="true" rel="apple-touch-icon" sizes="152x152" href="https://static.npmjs.com/34110fd7686e2c90a487ca98e7336e99.png"/><link data-react-helmet="true" rel="apple-touch-icon" sizes="180x180" href="https://static.npmjs.com/3dc95981de4241b35cd55fe126ab6b2c.png"/><link data-react-helmet="true" rel="icon" type="image/png" href="https://static.npmjs.com/b0f1a8318363185cc2ea6a40ac23eeb2.png" sizes="32x32"/><link data-react-helmet="true" rel="icon" type="image/png" href="https://static.npmjs.com/1996fcfdf7ca81ea795f67f093d7f449.png" sizes="230x230"/><link data-react-helmet="true" rel="icon" type="image/png" href="https://static.npmjs.com/f1786e9b7cba9753ca7b9c40e8b98f67.png" sizes="96x96"/><link data-react-helmet="true" rel="icon" type="image/png" href="https://static.npmjs.com/5f6e93af5bf0f5dcdd1eecdac99f51ee.png" sizes="192x192"/><link data-react-helmet="true" rel="icon" type="image/png" href="https://static.npmjs.com/c426a1116301d1fd178c51522484127a.png" sizes="228x228"/><link data-react-helmet="true" rel="icon" type="image/png" href="https://static.npmjs.com/da3ab40fb0861d15c83854c29f5f2962.png" sizes="16x16"/><link data-react-helmet="true" rel="stylesheet" href="https://static.npmjs.com/6f0fec69a6599ac07cbe906fef441123.css"/>

  <script data-react-helmet="true" >
window.dataLayer = window.dataLayer || []
window.dataLayer.push({
  'gtm.start': new Date().getTime(),
  event: 'gtm.js'
})
window.dataLayer.push(['config',' AW-965451981',{'send_page_view': false}])
</script><script data-react-helmet="true" async="true" src="https://www.googletagmanager.com/gtm.js?id=GTM-PZL5X6M"></script><script data-react-helmet="true" type="text/javascript" id="hs-script-loader" async="true" defer="true" src="https://js.hs-scripts.com/5326678.js"></script>

  </head>
<body>
<div id="app"><div class="_452c3ec4 d68c66a8"><ul class="cf325dbd list ma0 pa0 tr z-999" aria-live="polite"></ul><div class="flex flex-column vh-100"><header class="_755f5b0f bg-white z-2 bb b--black-20"><div></div><div class="e7070742 pv1 pv3-ns ph1-ns bg-white"><div class="ef7c6e62 center mw9 border-box ph4-ns ph3 flex"><span class="_0edb515f f6 pr3 dn dib-ns link lh-title dim br b--white-30 mr3">❤</span><span class="_99e3854f f6 fw4 link dn dib-ns tl dim mr4 nowrap">Narcoleptic Pasta Manufacturer</span><nav class="_4a5f2a79 db w-100 w-auto-ns tr truncate"><ul class="list pl0"><li class="dib dim mr2 mr3-ns"><a class="c6c55db4 link f6-ns f7 fw5 npme-hidden" id="nav-enterprise-link" data-event-name="npm Enterprise" href="/products/enterprise">npm Enterprise</a></li><li class="dib dim mr2 mr3-ns"><a class="c6c55db4 link f6-ns f7 fw5 npme-hidden" id="nav-products-link" href="/products">Products</a></li><li class="dib dim mr2 mr3-ns"><a class="c6c55db4 link f6-ns f7 fw5 npme-hidden" id="nav-solutions-link" href="/solutions">Solutions</a></li><li class="dib dim mr2 mr3-ns"><a class="c6c55db4 link f6-ns f7 fw5 npme-hidden" id="nav-resources-link" href="/resources">Resources</a></li><li class="dib dim mr2 mr3-ns"><a class="c6c55db4 link f6-ns f7 fw5" id="nav-docs-link" href="https://docs.npmjs.com">Docs</a></li><li class="dib dim"><a class="c6c55db4 link f6-ns f7 fw5" id="nav-support-link" href="/support">Support</a></li></ul></nav></div></div><div class="_4ea0e50d bt b--black-10"><div class="c5b9953b center mw9 flex flex-wrap ph4-ns border-box"><h1 class="_657f443d">npm</h1><div class="af43375d mt3 db mr3-ns pb3 pb0-ns flex-auto 2-ns ph3 ph0-ns"><a href="/"><svg viewBox="0 0 780 250"><path fill="#231F20" d="M240,250h100v-50h100V0H240V250z M340,50h50v100h-50V50z M480,0v200h100V50h50v150h50V50h50v150h50V0H480z M0,200h100V50h50v150h50V0H0V200z"></path></svg></a></div><div class="_581ebd89 relative flex-auto w-100 w-50-ns pv2-ns order-1-ns order-2"><form id="search" method="GET" action="/search" class="_13c93d41 relative flex bg-transparent ph3 ph2 pv2 ph0-ns pv0-ns bt b--black-10 bn-ns"><div class="e82b10fd relative flex-grow-1"><div class="_2f299eeb nowrap flex"><span class="_705cdf4f db fl pl3 pr1 bg-black-05"><svg width="15px" height="15px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18" aria-hidden="true"><g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="Artboard-1" stroke="#777777" stroke-width="1.3"><g id="Group"><path d="M13.4044,7.0274 C13.4044,10.5494 10.5494,13.4044 7.0274,13.4044 C3.5054,13.4044 0.6504,10.5494 0.6504,7.0274 C0.6504,3.5054 3.5054,0.6504 7.0274,0.6504 C10.5494,0.6504 13.4044,3.5054 13.4044,7.0274 Z" id="Stroke-3"></path><path d="M11.4913,11.4913 L17.8683,17.8683" id="Stroke-7"></path></g></g></g></svg></span><input type="search" name="q" hotkeys="[object Object]" placeholder="Search packages" autoComplete="off" aria-label="Search packages" inputref="[object Object]" class="_390acbc5 f5 fw3 black bg-black-05 relative" value="" element="input"/></div></div><button type="submit" class="_0da775bb bn pv2 ph3 f6 white pointer">Search</button><input type="hidden" name="csrftoken" value="mKR-Y9lHQOQpy5WNIL0q0AyKOzPBBQOoLRWHoOYdP3I"/></form></div><div class="_6f31c82b ml3 mt2 mt0-ns pv2-ns flex-auto tr nowrap relative pointer fw6 order-1 order-2-ns"><div class="_1066d9e7 flex items-center self-center pt1 pl3"><a href="/signup" class="_9752c8b3 pa1 lh-copy br2 f6 no-underline fw6 black-70 dim ph3 mr3">Join</a><a class="_352269a0 pa1 lh-copy br2 f6 no-underline fw5 black-70 dim" href="/login">Log In</a></div></div></div></div></header><main><div data-promotion-id="npm Orgs ongoing promo" data-promotion-spot="below-header"><div><div class="_98714ae6"><span rel="npm:upsellbox:text" class="d321523e">Need private packages and team management tools?</span><span class="c7d083b3"><a href="https://www.npmjs.com/products/orgs?utm_source=house&amp;utm_medium=upsellbox&amp;utm_campaign=Orgsongoingpromo">Check out npm Orgs.<!-- --> »</a></span></div></div></div><div class="_36ae70e9 flex flex-row flex-wrap pb5 ph3-l mw-100 mt4-ns mt1 center-ns mh1" id="top"><div class="w-100 ph0-l ph3 ph4-m"><h2 class="cd6ce1fd flex flex-column w-100 fw6 mt3 black dib ma0 tracked-tight no-underline hover-black f3-ns"><span class="_50685029 truncate flex-auto" title="node-html-parser">node-html-parser</span></h2><span class="_76473bea f6 dib ph0 pv2 mb2-ns black-80 nowrap f5 fw4 lh-copy">1.1.16<!-- --> • </span><span class="_813b53b2 _76473bea f6 dib ph0 pv2 mb2-ns black-80 nowrap f5 fw4 lh-copy green">Public</span><span class="_76473bea f6 dib ph0 pv2 mb2-ns black-80 nowrap f5 fw4 lh-copy"> • Published <time dateTime="2019-07-01T01:24:08.902Z" title="2019-7-1 01:24:08">4 months ago</time></span></div><ul class="cfb2a888 flex flex-column flex-row-l w-100 ma0 pa0 mr2 mr0-l list"><li class="_8055e658 f5 fw5 tc pointer d9f99065 ac5f7ef8 f4 fw6 black-50"><a class="_38ce9a85 link db ph4 fw6" href="?activeTab=readme"><span>Readme</span></a></li><li class="_8055e658 f5 fw5 tc pointer undefined"><a class="_38ce9a85 link db ph4 fw6" href="?activeTab=dependencies"><span><span class="c3fc8940">1</span>Dependency</span></a></li><li class="_8055e658 f5 fw5 tc pointer c1f85151"><a class="_38ce9a85 link db ph4 fw6" href="?activeTab=dependents"><span><span class="c3fc8940">110</span>Dependents</span></a></li><li class="_8055e658 f5 fw5 tc pointer _7cec0316"><a class="_38ce9a85 link db ph4 fw6" href="?activeTab=versions"><span><span class="c3fc8940">17</span>Versions</span></a></li></ul><div class="_6620a4fd mw8-l mw-100 w-100 w-two-thirds-l ph3-m pt2 pl0-ns pl2 order-1-m order-0-ns order-1 order-2-m"><section class="e22ba268 ph2 ph0-ns ml0-ns mr3-ns black-80"><div class=""><article> <div class="_6d9832ac pr4-ns pl0-ns ph1-m pr3-m pr2  markdown" id="readme"><h1><a id="user-content-fast-html-parser" class="deep-link" href="#fast-html-parser" aria-hidden="true" rel="nofollow"><svg aria-hidden="true" class="deep-link-icon" height="16" version="1.1" viewbox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Fast HTML Parser <a href="http://badge.fury.io/js/node-html-parser" rel="nofollow"><img src="https://badge.fury.io/js/node-html-parser.png" alt="NPM version" /></a> <a href="https://travis-ci.org/taoqf/node-fast-html-parser" rel="nofollow"><img src="https://travis-ci.org/taoqf/node-fast-html-parser.svg?branch=master" alt="Build Status" /></a></h1>
  <p class="package-description-redundant">Fast HTML Parser is a <em>very fast</em> HTML parser. Which will generate a simplified
DOM tree, with basic element query support.</p>
<p>Per the design, it intends to parse massive HTML files in lowest price, thus the
performance is the top priority.  For this reason, some malformatted HTML may not
be able to parse correctly, but most usual errors are covered (eg. HTML4 style
no closing <code>&lt;li&gt;</code>, <code>&lt;td&gt;</code> etc).</p>
<h2><a id="user-content-install" class="deep-link" href="#install" aria-hidden="true" rel="nofollow"><svg aria-hidden="true" class="deep-link-icon" height="16" version="1.1" viewbox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Install</h2>
<div class="highlight shell"><pre class="editor editor-colors"><div class="line"><span class="source shell"><span>npm install --save node-html-parser</span></span></div></pre></div>
<h2><a id="user-content-performance" class="deep-link" href="#performance" aria-hidden="true" rel="nofollow"><svg aria-hidden="true" class="deep-link-icon" height="16" version="1.1" viewbox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Performance</h2>
<p>Faster than htmlparser2!</p>
<div class="highlight shell"><pre class="editor editor-colors"><div class="line"><span class="source shell"><span>fast-html-parser: 2.18409 ms/file ± 1.37431</span></span></div><div class="line"><span class="source shell"><span>high5           </span><span class="support function builtin shell"><span>:</span></span><span> 4.55435 ms/file ± 2.51132</span></span></div><div class="line"><span class="source shell"><span>htmlparser      </span><span class="support function builtin shell"><span>:</span></span><span> 27.6920 ms/file ± 171.588</span></span></div><div class="line"><span class="source shell"><span>htmlparser2-dom </span><span class="support function builtin shell"><span>:</span></span><span> 6.22320 ms/file ± 3.48772</span></span></div><div class="line"><span class="source shell"><span>htmlparser2     </span><span class="support function builtin shell"><span>:</span></span><span> 3.58360 ms/file ± 2.23658</span></span></div><div class="line"><span class="source shell"><span>hubbub          </span><span class="support function builtin shell"><span>:</span></span><span> 16.1774 ms/file ± 8.95079</span></span></div><div class="line"><span class="source shell"><span>libxmljs        </span><span class="support function builtin shell"><span>:</span></span><span> 7.19406 ms/file ± 7.04495</span></span></div><div class="line"><span class="source shell"><span>parse5          </span><span class="support function builtin shell"><span>:</span></span><span> 10.7590 ms/file ± 8.09687</span></span></div></pre></div>
<p>Tested with <a href="https://github.com/AndreasMadsen/htmlparser-benchmark" rel="nofollow">htmlparser-benchmark</a>.</p>
<h2><a id="user-content-usage" class="deep-link" href="#usage" aria-hidden="true" rel="nofollow"><svg aria-hidden="true" class="deep-link-icon" height="16" version="1.1" viewbox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Usage</h2>
<div class="highlight ts"><pre class="editor editor-colors"><div class="line"><span class="source"><span class="meta import"><span class="keyword control import"><span>import</span></span><span> </span><span class="meta block"><span class="punctuation definition block"><span>{</span></span><span> </span><span class="variable other readwrite alias"><span>parse</span></span><span> </span><span class="punctuation definition block"><span>}</span></span></span><span> </span><span class="keyword control"><span>from</span></span><span> </span><span class="string quoted single"><span class="punctuation definition string begin"><span>'</span></span><span>node-html-parser</span><span class="punctuation definition string end"><span>'</span></span></span></span><span class="punctuation terminator statement"><span>;</span></span></span></div><div class="line"><span class="source"><span> </span></span></div><div class="line"><span class="source"><span class="meta"><span class="storage type"><span>const</span></span><span> </span><span class="meta"><span class="meta definition variable variable other readwrite"><span>root</span></span><span> </span></span><span class="keyword operator assignment"><span>=</span></span><span> </span><span class="entity name function"><span>parse</span></span><span class="meta brace round"><span>(</span></span><span class="string quoted single"><span class="punctuation definition string begin"><span>'</span></span><span>&lt;ul id=&quot;list&quot;&gt;&lt;li&gt;Hello World&lt;/li&gt;&lt;/ul&gt;</span><span class="punctuation definition string end"><span>'</span></span></span><span class="meta brace round"><span>)</span></span></span><span class="punctuation terminator statement"><span>;</span></span></span></div><div class="line"><span class="source"><span> </span></span></div><div class="line"><span class="source"><span class="support class console"><span>console</span></span><span class="punctuation"><span>.</span></span><span class="support function console"><span>log</span></span><span class="meta brace round"><span>(</span></span><span class="support variable object"><span>root</span></span><span class="punctuation"><span>.</span></span><span class="support variable dom"><span>firstChild</span></span><span class="punctuation"><span>.</span></span><span class="variable other"><span>structure</span></span><span class="meta brace round"><span>)</span></span><span class="punctuation terminator statement"><span>;</span></span></span></div><div class="line"><span class="source"><span class="comment line double-slash"><span class="punctuation definition comment"><span>//</span></span><span> ul#list</span></span></span></div><div class="line"><span class="source"><span class="comment line double-slash"><span class="punctuation definition comment"><span>//</span></span><span>   li</span></span></span></div><div class="line"><span class="source"><span class="comment line double-slash"><span class="punctuation definition comment"><span>//</span></span><span>     #text</span></span></span></div><div class="line"><span class="source"><span> </span></span></div><div class="line"><span class="source"><span class="support class console"><span>console</span></span><span class="punctuation"><span>.</span></span><span class="support function console"><span>log</span></span><span class="meta brace round"><span>(</span></span><span class="support variable object"><span>root</span></span><span class="punctuation"><span>.</span></span><span class="support function dom"><span>querySelector</span></span><span class="meta brace round"><span>(</span></span><span class="string quoted single"><span class="punctuation definition string begin"><span>'</span></span><span>#list</span><span class="punctuation definition string end"><span>'</span></span></span><span class="meta brace round"><span>)</span><span>)</span></span><span class="punctuation terminator statement"><span>;</span></span></span></div><div class="line"><span class="source"><span class="comment line double-slash"><span class="punctuation definition comment"><span>//</span></span><span> { tagName: 'ul',</span></span></span></div><div class="line"><span class="source"><span class="comment line double-slash"><span class="punctuation definition comment"><span>//</span></span><span>   rawAttrs: 'id=&quot;list&quot;',</span></span></span></div><div class="line"><span class="source"><span class="comment line double-slash"><span class="punctuation definition comment"><span>//</span></span><span>   childNodes:</span></span></span></div><div class="line"><span class="source"><span class="comment line double-slash"><span class="punctuation definition comment"><span>//</span></span><span>    [ { tagName: 'li',</span></span></span></div><div class="line"><span class="source"><span class="comment line double-slash"><span class="punctuation definition comment"><span>//</span></span><span>        rawAttrs: '',</span></span></span></div><div class="line"><span class="source"><span class="comment line double-slash"><span class="punctuation definition comment"><span>//</span></span><span>        childNodes: [Object],</span></span></span></div><div class="line"><span class="source"><span class="comment line double-slash"><span class="punctuation definition comment"><span>//</span></span><span>        classNames: [] } ],</span></span></span></div><div class="line"><span class="source"><span class="comment line double-slash"><span class="punctuation definition comment"><span>//</span></span><span>   id: 'list',</span></span></span></div><div class="line"><span class="source"><span class="comment line double-slash"><span class="punctuation definition comment"><span>//</span></span><span>   classNames: [] }</span></span></span></div><div class="line"><span class="source"><span class="support class console"><span>console</span></span><span class="punctuation"><span>.</span></span><span class="support function console"><span>log</span></span><span class="meta brace round"><span>(</span></span><span class="support variable object"><span>root</span></span><span class="punctuation"><span>.</span></span><span class="support function"><span>toString</span></span><span class="meta brace round"><span>(</span><span>)</span><span>)</span></span><span class="punctuation terminator statement"><span>;</span></span></span></div><div class="line"><span class="source"><span class="comment line double-slash"><span class="punctuation definition comment"><span>//</span></span><span> &lt;ul id=&quot;list&quot;&gt;&lt;li&gt;Hello World&lt;/li&gt;&lt;/ul&gt;</span></span></span></div><div class="line"><span class="source"><span class="support variable object"><span>root</span></span><span class="punctuation"><span>.</span></span><span class="entity name function"><span>set_content</span></span><span class="meta brace round"><span>(</span></span><span class="string quoted single"><span class="punctuation definition string begin"><span>'</span></span><span>&lt;li&gt;Hello World&lt;/li&gt;</span><span class="punctuation definition string end"><span>'</span></span></span><span class="meta brace round"><span>)</span></span><span class="punctuation terminator statement"><span>;</span></span></span></div><div class="line"><span class="source"><span class="support variable object"><span>root</span></span><span class="punctuation"><span>.</span></span><span class="support function"><span>toString</span></span><span class="meta brace round"><span>(</span><span>)</span></span><span class="punctuation terminator statement"><span>;</span></span><span>	</span><span class="comment line double-slash"><span class="punctuation definition comment"><span>//</span></span><span> &lt;li&gt;Hello World&lt;/li&gt;</span></span></span></div></pre></div>
  <div class="highlight js"><pre class="editor editor-colors"><div class="line"><span class="source js"><span class="storage type js"><span>var</span></span><span> HTMLParser </span><span class="keyword operator assignment js"><span>=</span></span><span> </span><span class="meta function-call js"><span class="support function js"><span>require</span></span><span class="meta js"><span class="punctuation definition begin round js"><span>(</span></span><span class="string quoted single js"><span class="punctuation definition string begin js"><span>'</span></span><span>node-html-parser</span><span class="punctuation definition string end js"><span>'</span></span></span><span class="punctuation definition end round js"><span>)</span></span></span></span><span class="punctuation terminator statement js"><span>;</span></span></span></div><div class="line"><span class="source js"><span> </span></span></div><div class="line"><span class="source js"><span class="storage type js"><span>var</span></span><span> root </span><span class="keyword operator assignment js"><span>=</span></span><span> </span><span class="variable other object js"><span>HTMLParser</span></span><span class="meta js"><span class="meta delimiter method period js"><span>.</span></span><span class="support function js"><span>parse</span></span><span class="meta js"><span class="punctuation definition begin round js"><span>(</span></span><span class="string quoted single js"><span class="punctuation definition string begin js"><span>'</span></span><span>&lt;ul id=&quot;list&quot;&gt;&lt;li&gt;Hello World&lt;/li&gt;&lt;/ul&gt;</span><span class="punctuation definition string end js"><span>'</span></span></span><span class="punctuation definition end round js"><span>)</span></span></span></span><span class="punctuation terminator statement js"><span>;</span></span></span></div></pre></div>
<h2><a id="user-content-api" class="deep-link" href="#api" aria-hidden="true" rel="nofollow"><svg aria-hidden="true" class="deep-link-icon" height="16" version="1.1" viewbox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>API</h2>
<h3><a id="user-content-parsedata-options" class="deep-link" href="#parsedata-options" aria-hidden="true" rel="nofollow"><svg aria-hidden="true" class="deep-link-icon" height="16" version="1.1" viewbox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>parse(data[, options])</h3>
<p>Parse given data, and return root of the generated DOM.</p>
<ul>
<li>
<p><strong>data</strong>, data to parse</p>
</li>
<li>
<p><strong>options</strong>, parse options</p>
<div class="highlight js"><pre class="editor editor-colors"><div class="line"><span class="source js"><span class="meta brace curly js"><span>{</span></span></span></div><div class="line"><span class="source js"><span>  lowerCaseTagName</span><span class="keyword operator assignment js"><span>:</span></span><span> </span><span class="constant language boolean false js"><span>false</span></span><span class="meta delimiter object comma js"><span>,</span></span><span>  </span><span class="comment line double-slash js"><span class="punctuation definition comment js"><span>//</span></span><span> convert tag name to lower case (hurt performance heavily)</span></span></span></div><div class="line"><span class="source js"><span>  script</span><span class="keyword operator assignment js"><span>:</span></span><span> </span><span class="constant language boolean false js"><span>false</span></span><span class="meta delimiter object comma js"><span>,</span></span><span>            </span><span class="comment line double-slash js"><span class="punctuation definition comment js"><span>//</span></span><span> retrieve content in &lt;script&gt; (hurt performance slightly)</span></span></span></div><div class="line"><span class="source js"><span>  style</span><span class="keyword operator assignment js"><span>:</span></span><span> </span><span class="constant language boolean false js"><span>false</span></span><span class="meta delimiter object comma js"><span>,</span></span><span>             </span><span class="comment line double-slash js"><span class="punctuation definition comment js"><span>//</span></span><span> retrieve content in &lt;style&gt; (hurt performance slightly)</span></span></span></div><div class="line"><span class="source js"><span>  pre</span><span class="keyword operator assignment js"><span>:</span></span><span> </span><span class="constant language boolean false js"><span>false</span></span><span>                </span><span class="comment line double-slash js"><span class="punctuation definition comment js"><span>//</span></span><span> retrieve content in &lt;pre&gt; (hurt performance slightly)</span></span></span></div><div class="line"><span class="source js"><span class="meta brace curly js"><span>}</span></span></span></div></pre></div>
  </li>
  </ul>
  <h3><a id="user-content-htmlelementtext" class="deep-link" href="#htmlelementtext" aria-hidden="true" rel="nofollow"><svg aria-hidden="true" class="deep-link-icon" height="16" version="1.1" viewbox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>HTMLElement#text</h3>
<p>Get unescaped text value of current node and its children. Like <code>innerText</code>.
(slow for the first time)</p>
<h3><a id="user-content-htmlelementrawtext" class="deep-link" href="#htmlelementrawtext" aria-hidden="true" rel="nofollow"><svg aria-hidden="true" class="deep-link-icon" height="16" version="1.1" viewbox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>HTMLElement#rawText</h3>
<p>Get escpaed (as-it) text value of current node and its children. May have
<code>&amp;amp;</code> in it. (fast)</p>
<h3><a id="user-content-htmlelementstructuredtext" class="deep-link" href="#htmlelementstructuredtext" aria-hidden="true" rel="nofollow"><svg aria-hidden="true" class="deep-link-icon" height="16" version="1.1" viewbox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>HTMLElement#structuredText</h3>
<p>Get structured Text</p>
<h3><a id="user-content-htmlelementtrimright" class="deep-link" href="#htmlelementtrimright" aria-hidden="true" rel="nofollow"><svg aria-hidden="true" class="deep-link-icon" height="16" version="1.1" viewbox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>HTMLElement#trimRight()</h3>
<p>Trim element from right (in block) after seeing pattern in a TextNode.</p>
<h3><a id="user-content-htmlelementstructure" class="deep-link" href="#htmlelementstructure" aria-hidden="true" rel="nofollow"><svg aria-hidden="true" class="deep-link-icon" height="16" version="1.1" viewbox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>HTMLElement#structure</h3>
<p>Get DOM structure</p>
<h3><a id="user-content-htmlelementremovewhitespace" class="deep-link" href="#htmlelementremovewhitespace" aria-hidden="true" rel="nofollow"><svg aria-hidden="true" class="deep-link-icon" height="16" version="1.1" viewbox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>HTMLElement#removeWhitespace()</h3>
<p>Remove whitespaces in this sub tree.</p>
<h3><a id="user-content-htmlelementqueryselectorallselector" class="deep-link" href="#htmlelementqueryselectorallselector" aria-hidden="true" rel="nofollow"><svg aria-hidden="true" class="deep-link-icon" height="16" version="1.1" viewbox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>HTMLElement#querySelectorAll(selector)</h3>
<p>Query CSS selector to find matching nodes.</p>
<p>Note: only <code>tagName</code>, <code>#id</code>, <code>.class</code> selectors supported. And not behave the
same as standard <code>querySelectorAll()</code> as it will <em>stop</em> searching sub tree after
find a match.</p>
<h3><a id="user-content-htmlelementqueryselectorselector" class="deep-link" href="#htmlelementqueryselectorselector" aria-hidden="true" rel="nofollow"><svg aria-hidden="true" class="deep-link-icon" height="16" version="1.1" viewbox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>HTMLElement#querySelector(selector)</h3>
<p>Query CSS Selector to find matching node.</p>
<h3><a id="user-content-htmlelementappendchildnode" class="deep-link" href="#htmlelementappendchildnode" aria-hidden="true" rel="nofollow"><svg aria-hidden="true" class="deep-link-icon" height="16" version="1.1" viewbox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>HTMLElement#appendChild(node)</h3>
<p>Append a child node to childNodes</p>
<h3><a id="user-content-htmlelementfirstchild" class="deep-link" href="#htmlelementfirstchild" aria-hidden="true" rel="nofollow"><svg aria-hidden="true" class="deep-link-icon" height="16" version="1.1" viewbox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>HTMLElement#firstChild</h3>
<p>Get first child node</p>
<h3><a id="user-content-htmlelementlastchild" class="deep-link" href="#htmlelementlastchild" aria-hidden="true" rel="nofollow"><svg aria-hidden="true" class="deep-link-icon" height="16" version="1.1" viewbox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>HTMLElement#lastChild</h3>
<p>Get last child node</p>
<h3><a id="user-content-htmlelementattributes" class="deep-link" href="#htmlelementattributes" aria-hidden="true" rel="nofollow"><svg aria-hidden="true" class="deep-link-icon" height="16" version="1.1" viewbox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>HTMLElement#attributes</h3>
<p>Get attributes</p>
<h3><a id="user-content-htmlelementrawattributes" class="deep-link" href="#htmlelementrawattributes" aria-hidden="true" rel="nofollow"><svg aria-hidden="true" class="deep-link-icon" height="16" version="1.1" viewbox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>HTMLElement#rawAttributes</h3>
<p>Get escaped (as-it) attributes</p>
<h3><a id="user-content-htmlelementtostring" class="deep-link" href="#htmlelementtostring" aria-hidden="true" rel="nofollow"><svg aria-hidden="true" class="deep-link-icon" height="16" version="1.1" viewbox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>HTMLElement#toString()</h3>
<p>Same as <a href="#htmlelementouterhtml" rel="nofollow">outerHTML</a></p>
<h3><a id="user-content-htmlelementinnerhtml" class="deep-link" href="#htmlelementinnerhtml" aria-hidden="true" rel="nofollow"><svg aria-hidden="true" class="deep-link-icon" height="16" version="1.1" viewbox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>HTMLElement#innerHTML</h3>
<p>Get innerHTML.</p>
<h3><a id="user-content-htmlelementouterhtml" class="deep-link" href="#htmlelementouterhtml" aria-hidden="true" rel="nofollow"><svg aria-hidden="true" class="deep-link-icon" height="16" version="1.1" viewbox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>HTMLElement#outerHTML</h3>
<p>Get outerHTML.</p>
<h3><a id="user-content-htmlelementset_contentcontent-string--node--node" class="deep-link" href="#htmlelementset_contentcontent-string--node--node" aria-hidden="true" rel="nofollow"><svg aria-hidden="true" class="deep-link-icon" height="16" version="1.1" viewbox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>HTMLElement#set_content(content: string | Node | Node[])</h3>
<p>Set content. <strong>Notice</strong>: Do not set content of the <strong>root</strong> node.</p>
</div></article></div><div class="pv4"><h2 id="user-content-keywords" class="a0dff0b1 mt2 pt2 mb3 pb3 f4 fw6 b--black-10">Keywords</h2><ul class="list pl0"><li class="dib mr2"><a class="_75a5f581 f4 fw6 fl db pv1 ma1 black-70 link hover-black animate" href="/search?q=keywords:fast html parser nodejs typescript">fast html parser nodejs typescript</a></li></ul></div></section></div><div class="fdbf4038 w-third-l mt3 w-100 ph3 ph4-m pv3 pv0-l order-1-ns order-0"><h3 class="c84e15be f5 mt2 pt2 mb0 black-50">install</h3><p class="d767adf4 lh-copy truncate br2 ph3 mb3 black-80 b5be2af6 f6 flex flex-row"><svg viewBox="0 0 12.32 9.33"><g><line class="st1" x1="7.6" y1="8.9" x2="7.6" y2="6.9"></line><rect width="1.9" height="1.9"></rect><rect x="1.9" y="1.9" width="1.9" height="1.9"></rect><rect x="3.7" y="3.7" width="1.9" height="1.9"></rect><rect x="1.9" y="5.6" width="1.9" height="1.9"></rect><rect y="7.5" width="1.9" height="1.9"></rect></g></svg><code class="flex-auto truncate db"><span>npm i <!-- -->node-html-parser</span></code></p><div><div><h3 class="c84e15be f5 mt2 pt2 mb0 black-50 _5cfc0900"><svg viewBox="0 0 7.22 11.76"><title>Downloads</title><g><polygon points="4.59 4.94 4.59 0 2.62 0 2.62 4.94 0 4.94 3.28 9.53 7.22 4.94 4.59 4.94"></polygon><rect x="0.11" y="10.76" width="7" height="1"></rect></g></svg>weekly downloads</h3><div class="_000ae427 flex flex-row-reverse items-end"><svg class="_418c4939 flex-none" width="200" height="40" stroke-width="3" stroke="#8956FF" fill="rgba(137, 86, 255, .2)"></svg><p class="_9ba9a726 f4 tl flex-auto fw6 black-80 ma0 pr2 pb1">26,718</p></div></div></div><div class="_702d723c dib w-50 bb b--black-10 pr2"><h3 class="c84e15be f5 mt2 pt2 mb0 black-50">version</h3><p class="f2874b88 fw6 mb3 mt2 truncate black-80 f4">1.1.16</p></div><div class="_702d723c dib w-50 bb b--black-10 pr2"><h3 class="c84e15be f5 mt2 pt2 mb0 black-50">license</h3><p class="f2874b88 fw6 mb3 mt2 truncate black-80 f4">MIT</p></div><div class="_702d723c dib w-50 bb b--black-10 pr2"><h3 class="c84e15be f5 mt2 pt2 mb0 black-50">homepage</h3><p class="f2874b88 fw6 mb3 mt2 truncate black-80 f4"><a class="b2812e30 f2874b88 fw6 mb3 mt2 truncate black-80 f4 link" href="https://github.com/taoqf/node-fast-html-parser">github.com</a></p></div><div class="_702d723c dib w-50 bb b--black-10 pr2"><h3 class="c84e15be f5 mt2 pt2 mb0 black-50">repository</h3><p class="f2874b88 fw6 mb3 mt2 truncate black-80 f4"><a class="b2812e30 f2874b88 fw6 mb3 mt2 truncate black-80 f4 link" href="https://github.com/taoqf/node-fast-html-parser"><span class="_9e13d83d dib v-mid"><svg viewBox="0 0 92 92" version="1.1" xmlns="http://www.w3.org/2000/svg"><title>Git</title><g stroke="none" fill="#0a0b09"><path d="M90.155,41.965 L50.036,1.847 C47.726,-0.464 43.979,-0.464 41.667,1.847 L33.336,10.179 L43.904,20.747 C46.36,19.917 49.176,20.474 51.133,22.431 C53.102,24.401 53.654,27.241 52.803,29.706 L62.989,39.891 C65.454,39.041 68.295,39.59 70.264,41.562 C73.014,44.311 73.014,48.768 70.264,51.519 C67.512,54.271 63.056,54.271 60.303,51.519 C58.235,49.449 57.723,46.409 58.772,43.861 L49.272,34.362 L49.272,59.358 C49.942,59.69 50.575,60.133 51.133,60.69 C53.883,63.44 53.883,67.896 51.133,70.65 C48.383,73.399 43.924,73.399 41.176,70.65 C38.426,67.896 38.426,63.44 41.176,60.69 C41.856,60.011 42.643,59.497 43.483,59.153 L43.483,33.925 C42.643,33.582 41.858,33.072 41.176,32.389 C39.093,30.307 38.592,27.249 39.661,24.691 L29.243,14.271 L1.733,41.779 C-0.578,44.092 -0.578,47.839 1.733,50.15 L41.854,90.268 C44.164,92.578 47.91,92.578 50.223,90.268 L90.155,50.336 C92.466,48.025 92.466,44.275 90.155,41.965"></path></g></svg></span><span>github</span></a></p></div><div class="_702d723c dib w-50 bb b--black-10 pr2 w-100"><h3 class="c84e15be f5 mt2 pt2 mb0 black-50">last publish</h3><p class="f2874b88 fw6 mb3 mt2 truncate black-80 f4"><time dateTime="2019-07-01T01:24:08.902Z" title="2019-7-1 01:24:08">4 months ago</time></p></div><div><h3 class="c84e15be f5 mt2 pt2 mb0 black-50">collaborators</h3><ul class="list pl0 cf"><li class="_426b8533 fl dib mr2 mb2"><div><a href="/~taoqf"><img src="https://s.gravatar.com/avatar/620b0b738b80e19ee7b6cf0126df855a?size=100&amp;default=retro" style="width:42px;height:42px;min-width:42px;border-radius:4%" alt="avatar" title="taoqf"/></a></div></li></ul></div><div class="_33c89934 flex flex-column-l items-start-l"><p class="a9a9a788 _24a1e9c7 dab9924c br2 pv2 ph3 fw5 f5 tc button button-reset pointer ma2 truncate ml0 no-underline _44c90bfa inline-flex flex-row f5 link black-40 hover-black pr3-l npme-hidden"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.44 22.44"><defs><style>
  .line,.line-round{
  fill:none; stroke:#000; stroke-miterlimit:10;
}
.line-round{ stroke-linecap:round; }</style></defs><g id="72f6004a-0c55-4362-98d7-753234fe6a5d" data-name="Layer 1"><polygon class="line" points="0.5 5.9 0.5 16.54 9.72 21.87 18.94 16.54 18.94 5.9 9.72 0.58 0.5 5.9"></polygon><line class="line-round" x1="9.72" y1="6.9" x2="9.72" y2="15.54"></line><line class="line" x1="12.63" y1="12.63" x2="9.72" y2="15.54"></line><line class="line" x1="6.81" y1="12.63" x2="9.72" y2="15.54"></line></g></svg><a class="link black-80 pr2" href="https://runkit.com/npm/node-html-parser">Test with RunKit</a></p><a class="_4a3f8f21 _24a1e9c7 dab9924c br2 pv2 ph3 fw5 f5 tc button button-reset pointer ma2 truncate ml0 no-underline mt2 dib" href="/advisories/report?package=node-html-parser">Report a vulnerability</a></div></div></div></main><footer class="cd2827bb bt b--black-10 mt4"><div class="center mw9 pa3 flex flex-column flex-wrap-m flex-row-ns"><div class="flex-auto pa4-ns pa3 w-100 w-10-l"><div class="_802b5507 pt4"><svg viewBox="0 0 27.23 27.23"><rect fill="#cccccc" width="27.23" height="27.23" rx="2"></rect><polygon fill="#fff" points="5.8 21.75 13.66 21.75 13.67 9.98 17.59 9.98 17.58 21.76 21.51 21.76 21.52 6.06 5.82 6.04 5.8 21.75"></polygon></svg></div></div><div class="flex-auto pa4-ns pa3 w-30-ns w-50-m"><h3 class="_1b8c0a9f pv3">Help</h3><ul class="list pl0"><li class="pv1"><a class="link" href="https://docs.npmjs.com">Documentation</a></li><li class="pv1"><a class="link" href="/resources/">Resources</a></li><li class="pv1"><a class="link" href="/support">Support / Contact Us</a></li><li class="pv1 npme-hidden"><a class="link" href="http://status.npmjs.org/">Registry Status</a></li><li class="pv1"><a class="link" href="https://npm.community/c/bugs">Report Issues</a></li><li class="pv1"><a class="link" href="https://npm.community">npm Community Site</a></li><li class="pv1"><a class="link" href="/policies/security">Security</a></li></ul></div><div class="flex-auto pa4-ns pa3 w-30-ns w-50-m"><h3 class="_1b8c0a9f pv3">About npm</h3><ul class="list pl0"><li class="pv1"><a class="link" href="/about">About npm, Inc</a></li><li class="pv1"><a class="link" href="https://javascriptsurvey.com">JavaScriptSurvey.com</a></li><li class="pv1"><a class="link" href="/events">Events</a></li><li class="pv1 npme-hidden"><a class="link" href="/jobs">Jobs</a></li><li class="pv1"><a class="link" href="/press">Press</a></li><li class="pv1"><a class="link" href="/npm-weekly">npm Weekly</a></li><li class="pv1"><a class="link" href="http://blog.npmjs.org">Blog</a></li><li class="pv1"><a class="link" href="https://twitter.com/npmjs">Twitter</a></li><li class="pv1"><a class="link" href="https://github.com/npm">GitHub</a></li></ul></div><div class="flex-auto pa4-ns pa3 w-30-ns w-50-m"><h3 class="_1b8c0a9f pv3">Terms &amp; Policies</h3><ul class="list pl0"><li class="pv1"><a class="link" href="/policies/terms">Terms of Use</a></li><li class="pv1 npme-hidden"><a class="link" href="/policies/conduct">Code of Conduct</a></li><li class="pv1"><a class="link" href="/policies/disputes">Package Name Disputes</a></li><li class="pv1"><a class="link" href="/policies/privacy">Privacy Policy</a></li><li class="pv1 npme-hidden"><a class="link" href="/policies/receiving-reports">Reporting Abuse</a></li><li class="pv1"><a class="link" href="/policies/">Other policies</a></li></ul></div></div></footer></div><div class="_8a62ca63 fixed top-0 left-0 z-999" style="opacity:0;transform:scaleX(1);-webkit-transform:scaleX(1);-moz-transform:scaleX(1)"></div></div></div><script integrity="sha512-ZTdFssxQa3o3jN/7QMwehMijAxUKXdTi3uz6y2tUiuA92Pc8ZbWMc3vF3+jl3BVV7tDWaCf7/2xCVA7wGPGWKw==">window.__context__ = {"context":{"isNpme":false,"npmExpansions":["NASA: Pluto Matters!","Notably Polite Mariner","Newton's Poleless Magnet","Nodes Per Minute","Nachos Pillage Milwaukee","Natural Potato Magnet","Nefarious Plastic Mannequins","Naivete Precedes Misrepresentation","Neural Prediction Model","Narcoleptic Pasta Manufacturer"],"notifications":[],"csrftoken":"mKR-Y9lHQOQpy5WNIL0q0AyKOzPBBQOoLRWHoOYdP3I","user":null,"documentContext":{"readme.data":"readme"},"undefined":true,"readme":{"ref":"readme","data":null},"upsell":{"_id":"cjvplhesugfdk2a74fseqwz6w","published":true,"trash":false,"type":"upsell-box","title":"npm Orgs ongoing promo","upsellText":"Need private packages and team management tools?","linkText":"Check out npm Orgs.","linkURL":"https://www.npmjs.com/products/orgs?utm_source=house\u0026utm_medium=upsellbox\u0026utm_campaign=Orgsongoingpromo","slug":"npm-orgs-ongoing-promo","tags":[],"workflowLocale":"default","workflowGuid":"cjvplhes7gfdg2a745itdsw7w","createdAt":"2019-05-15T19:05:08.910Z","updatedAt":"2019-10-08T17:27:31.361Z","titleSortified":"npm orgs ongoing promo","highSearchText":"npm orgs ongoing promo npm orgs ongoing promo npm orgs ongoing promo need private packages and team management tools check out npm orgs https www npmjs com products orgs utm source house utm medium upsellbox utm campaign orgsongoingpromo npm orgs ongoing promo","highSearchWords":["npm","orgs","ongoing","promo","need","private","packages","and","team","management","tools","check","out","https","www","npmjs","com","products","utm","source","house","medium","upsellbox","campaign","orgsongoingpromo"],"lowSearchText":"npm orgs ongoing promo npm orgs ongoing promo npm orgs ongoing promo need private packages and team management tools check out npm orgs https www npmjs com products orgs utm source house utm medium upsellbox utm campaign orgsongoingpromo npm orgs ongoing promo","searchSummary":"","docPermissions":[],"loginRequired":null,"viewUsersIds":[],"viewGroupsIds":[],"editUsersIds":[],"editGroupsIds":[],"viewUsersRelationships":{},"viewGroupsRelationships":{},"editUsersRelationships":{},"editGroupsRelationships":{},"_edit":true,"_publish":true,"_originalWidgets":{}},"starAction":"/package/node-html-parser/star","packageUrl":"/package/node-html-parser","canEditPackage":false,"dependents":{"dependentsCount":110,"dependentsTruncated":["crawlerjobnew","@brokeh/proxy.js","@deja-vu/schematics","a7js","html-extractor-loader","@webcomponents-dev/sfc-parser","iosratingfetcher","yomoujs","vsf-cms-block-mixin","cttpie","onemsdk","score-notification","matrix-discord-parser","@firestudio/core","@ibis-app/app","shayder-pac","@avensia-oss/critical-css","@noodlex/node-torrent9-api","@xbenjii/renovate","rollup-plugin-html2","slide2pdf","masked-web-crawler","katexify","notion-as-a-service","junte-ui","ng-documentation","esi-processor","fbmockserver","mofron-tag","crawl-lottie-files","@dfeidao/desktop","@feidao-factory/desktop","ufrgs-vagas","rollup-plugin-script-tag","npm-dlc","homebridge-cg-smarthouse-platform","url-metadata-parser","renovate","spectrum-bot","treekoditor","amigo-spanish","tea-seo-detector","idbt","@dillonchr/domget","vue-native-web","nebulae","right-track-agency-lirr","right-track-agency-mnr","@nebulae/cli","alexa-parser"]},"downloads":[{"downloads":2110,"label":"2018-10-18 to 2018-10-24"},{"downloads":6061,"label":"2018-10-25 to 2018-10-31"},{"downloads":2290,"label":"2018-11-01 to 2018-11-07"},{"downloads":2242,"label":"2018-11-08 to 2018-11-14"},{"downloads":2274,"label":"2018-11-15 to 2018-11-21"},{"downloads":2224,"label":"2018-11-22 to 2018-11-28"},{"downloads":2958,"label":"2018-11-29 to 2018-12-05"},{"downloads":2640,"label":"2018-12-06 to 2018-12-12"},{"downloads":3004,"label":"2018-12-13 to 2018-12-19"},{"downloads":2153,"label":"2018-12-20 to 2018-12-26"},{"downloads":1856,"label":"2018-12-27 to 2019-01-02"},{"downloads":3415,"label":"2019-01-03 to 2019-01-09"},{"downloads":3470,"label":"2019-01-10 to 2019-01-16"},{"downloads":5731,"label":"2019-01-17 to 2019-01-23"},{"downloads":8344,"label":"2019-01-24 to 2019-01-30"},{"downloads":7567,"label":"2019-01-31 to 2019-02-06"},{"downloads":7280,"label":"2019-02-07 to 2019-02-13"},{"downloads":7551,"label":"2019-02-14 to 2019-02-20"},{"downloads":7742,"label":"2019-02-21 to 2019-02-27"},{"downloads":7974,"label":"2019-02-28 to 2019-03-06"},{"downloads":8273,"label":"2019-03-07 to 2019-03-13"},{"downloads":9049,"label":"2019-03-14 to 2019-03-20"},{"downloads":11223,"label":"2019-03-21 to 2019-03-27"},{"downloads":10756,"label":"2019-03-28 to 2019-04-03"},{"downloads":15525,"label":"2019-04-04 to 2019-04-10"},{"downloads":16979,"label":"2019-04-11 to 2019-04-17"},{"downloads":15867,"label":"2019-04-18 to 2019-04-24"},{"downloads":16365,"label":"2019-04-25 to 2019-05-01"},{"downloads":13477,"label":"2019-05-02 to 2019-05-08"},{"downloads":17461,"label":"2019-05-09 to 2019-05-15"},{"downloads":20325,"label":"2019-05-16 to 2019-05-22"},{"downloads":20265,"label":"2019-05-23 to 2019-05-29"},{"downloads":22834,"label":"2019-05-30 to 2019-06-05"},{"downloads":26708,"label":"2019-06-06 to 2019-06-12"},{"downloads":23149,"label":"2019-06-13 to 2019-06-19"},{"downloads":24080,"label":"2019-06-20 to 2019-06-26"},{"downloads":27056,"label":"2019-06-27 to 2019-07-03"},{"downloads":24150,"label":"2019-07-04 to 2019-07-10"},{"downloads":26437,"label":"2019-07-11 to 2019-07-17"},{"downloads":24054,"label":"2019-07-18 to 2019-07-24"},{"downloads":24016,"label":"2019-07-25 to 2019-07-31"},{"downloads":23939,"label":"2019-08-01 to 2019-08-07"},{"downloads":24336,"label":"2019-08-08 to 2019-08-14"},{"downloads":24552,"label":"2019-08-15 to 2019-08-21"},{"downloads":25622,"label":"2019-08-22 to 2019-08-28"},{"downloads":22823,"label":"2019-08-29 to 2019-09-04"},{"downloads":26177,"label":"2019-09-05 to 2019-09-11"},{"downloads":28818,"label":"2019-09-12 to 2019-09-18"},{"downloads":26877,"label":"2019-09-19 to 2019-09-25"},{"downloads":27261,"label":"2019-09-26 to 2019-10-02"},{"downloads":27700,"label":"2019-10-03 to 2019-10-09"},{"downloads":26718,"label":"2019-10-10 to 2019-10-16"}],"packument":{"author":{"name":"Xiaoyi Shi","avatars":{"small":"https://s.gravatar.com/avatar/ca9a9d2d1f1e3bdaa4ed21d336166a7a?size=50\u0026default=retro","medium":"https://s.gravatar.com/avatar/ca9a9d2d1f1e3bdaa4ed21d336166a7a?size=100\u0026default=retro","large":"https://s.gravatar.com/avatar/ca9a9d2d1f1e3bdaa4ed21d336166a7a?size=496\u0026default=retro"},"created":{"ts":null,"rel":"Invalid date"},"email":"ashi009@gmail.com"},"description":"A very fast HTML parser, generating a simplified DOM, with basic element query support.","homepage":"https://github.com/taoqf/node-fast-html-parser","repository":"https://github.com/taoqf/node-fast-html-parser","distTags":{"latest":"1.1.16"},"keywords":["fast html parser nodejs typescript"],"maintainers":[{"name":"taoqf","avatars":{"small":"https://s.gravatar.com/avatar/620b0b738b80e19ee7b6cf0126df855a?size=50\u0026default=retro","medium":"https://s.gravatar.com/avatar/620b0b738b80e19ee7b6cf0126df855a?size=100\u0026default=retro","large":"https://s.gravatar.com/avatar/620b0b738b80e19ee7b6cf0126df855a?size=496\u0026default=retro"},"created":{"ts":null,"rel":"Invalid date"},"email":"tao_qiufeng@126.com"}],"name":"node-html-parser","license":"MIT","version":"1.1.16","versions":[{"version":"1.1.16","date":{"ts":1561944248902,"rel":"4 months ago"}},{"version":"1.1.15","date":{"ts":1554978675604,"rel":"6 months ago"}},{"version":"1.1.14","date":{"ts":1554264021574,"rel":"6 months ago"}},{"version":"1.1.13","date":{"ts":1553136579404,"rel":"7 months ago"}},{"version":"1.1.12","date":{"ts":1548042874866,"rel":"9 months ago"}},{"version":"1.1.11","date":{"ts":1542003209986,"rel":"a year ago"}},{"version":"1.1.10","date":{"ts":1535345880845,"rel":"a year ago"}},{"version":"1.1.9","date":{"ts":1534497943935,"rel":"a year ago"}},{"version":"1.1.8","date":{"ts":1531730539490,"rel":"a year ago"}},{"version":"1.1.7","date":{"ts":1531724936693,"rel":"a year ago"}},{"version":"1.1.6","date":{"ts":1513241617226,"rel":"2 years ago"}},{"version":"1.1.5","date":{"ts":1498105853453,"rel":"2 years ago"}},{"version":"1.1.4","date":{"ts":1497576790149,"rel":"2 years ago"}},{"version":"1.1.3","date":{"ts":1497522571838,"rel":"2 years ago"}},{"version":"1.1.2","date":{"ts":1497432562863,"rel":"2 years ago"}},{"version":"1.1.1","date":{"ts":1497432276396,"rel":"2 years ago"}},{"version":"1.1.0","date":{"ts":1497431746839,"rel":"2 years ago"}}],"deprecations":[]},"package":"node-html-parser","isStarred":false,"packageVersion":{"author":{"name":"Xiaoyi Shi","avatars":{"small":"https://s.gravatar.com/avatar/ca9a9d2d1f1e3bdaa4ed21d336166a7a?size=50\u0026default=retro","medium":"https://s.gravatar.com/avatar/ca9a9d2d1f1e3bdaa4ed21d336166a7a?size=100\u0026default=retro","large":"https://s.gravatar.com/avatar/ca9a9d2d1f1e3bdaa4ed21d336166a7a?size=496\u0026default=retro"},"created":{"ts":null,"rel":"Invalid date"},"email":"ashi009@gmail.com"},"description":"A very fast HTML parser, generating a simplified DOM, with basic element query support.","homepage":"https://github.com/taoqf/node-fast-html-parser","repository":"https://github.com/taoqf/node-fast-html-parser","keywords":["fast html parser nodejs typescript"],"dependencies":{"he":"1.1.1"},"devDependencies":{"@types/entities":"latest","@types/he":"latest","@types/node":"latest","blanket":"latest","del-cli":"latest","mocha":"latest","should":"latest","spec":"latest","travis-cov":"latest","typescript":"next"},"maintainers":[{"name":"taoqf","avatars":{"small":"https://s.gravatar.com/avatar/620b0b738b80e19ee7b6cf0126df855a?size=50\u0026default=retro","medium":"https://s.gravatar.com/avatar/620b0b738b80e19ee7b6cf0126df855a?size=100\u0026default=retro","large":"https://s.gravatar.com/avatar/620b0b738b80e19ee7b6cf0126df855a?size=496\u0026default=retro"},"created":{"ts":null,"rel":"Invalid date"},"email":"tao_qiufeng@126.com"}],"name":"node-html-parser","license":"MIT","version":"1.1.16","versions":[],"deprecations":[]},"private":false,"ghapi":"https://api.github.com/repos/taoqf/node-fast-html-parser"},"chunks":{"commons":["commons.81abc4c4bc0dc173a51f.js","commons.81abc4c4bc0dc173a51f.js.map"],"styles":["styles.b1f46f881f2ce6ef8915.css","minicssextractbug.608694f20ce046e1f914.js","styles.b1f46f881f2ce6ef8915.css.map","minicssextractbug.608694f20ce046e1f914.js.map"],"advisories/detail":["advisories/detail.1278951e835281f1137c.js","advisories/detail.1278951e835281f1137c.js.map"],"advisories/list":["advisories/list.6d28a45c777782f57476.js","advisories/list.6d28a45c777782f57476.js.map"],"advisories/report":["advisories/report.085fa019d35d4466a4b7.js","advisories/report.085fa019d35d4466a4b7.js.map"],"advisories/versions":["advisories/versions.30dd2dda24c4183be7e0.js","advisories/versions.30dd2dda24c4183be7e0.js.map"],"auth/cli":["auth/cli.58d50f813635a7b7bca4.js","auth/cli.58d50f813635a7b7bca4.js.map"],"auth/common-passwords":["auth/common-passwords.919fd18903fc5c0c7773.js","auth/common-passwords.919fd18903fc5c0c7773.js.map"],"auth/escalate":["auth/escalate.24fc26ec1aa0a9761261.js","auth/escalate.24fc26ec1aa0a9761261.js.map"],"auth/forgot":["auth/forgot.2e4bfe5cd1ea7337ace1.js","auth/forgot.2e4bfe5cd1ea7337ace1.js.map"],"auth/forgot-sent":["auth/forgot-sent.c5bb6def5af9de42cc97.js","auth/forgot-sent.c5bb6def5af9de42cc97.js.map"],"auth/invite-signup":["auth/invite-signup.5b60b6e5806296e7325e.js","auth/invite-signup.5b60b6e5806296e7325e.js.map"],"auth/login":["auth/login.c748f8edb77158ac5f7d.js","auth/login.c748f8edb77158ac5f7d.js.map"],"auth/otp":["auth/otp.85099f5d9a5e112a39cb.js","auth/otp.85099f5d9a5e112a39cb.js.map"],"auth/reset-password":["auth/reset-password.9421476ea62a62a7e253.js","auth/reset-password.9421476ea62a62a7e253.js.map"],"auth/signup":["auth/signup.a3d09b274845eead36d6.js","auth/signup.a3d09b274845eead36d6.js.map"],"auth/sso-signup":["auth/sso-signup.35be503c5c003e23068e.js","auth/sso-signup.35be503c5c003e23068e.js.map"],"billing/detail":["billing/detail.5e8b89ef9320e5c61b23.js","billing/detail.5e8b89ef9320e5c61b23.js.map"],"billing/downgrade":["billing/downgrade.1691597316da27450705.js","billing/downgrade.1691597316da27450705.js.map"],"billing/upgrade":["billing/upgrade.fef59d1e8edd8bf8d84b.js","billing/upgrade.fef59d1e8edd8bf8d84b.js.map"],"contact/contact":["contact/contact.6c2941823c4f421600a1.js","contact/contact.6c2941823c4f421600a1.js.map"],"debug/badstatus":["debug/badstatus.ebef9744b5ce793ae0e3.js","debug/badstatus.ebef9744b5ce793ae0e3.js.map"],"debug/detail":["debug/detail.cd2307529d3866ddaa03.js","debug/detail.cd2307529d3866ddaa03.js.map"],"debug/failcomponent":["debug/failcomponent.65957e8ec24ed22e9790.js","debug/failcomponent.65957e8ec24ed22e9790.js.map"],"egg/egg":["egg/egg.0cf3e6954071c488fbd4.js","egg/egg.0cf3e6954071c488fbd4.js.map"],"enterprise/complete":["enterprise/complete.6c1c6c8769fceddf67cc.js","enterprise/complete.6c1c6c8769fceddf67cc.js.map"],"enterprise/license-paid":["enterprise/license-paid.17df1ebcaccb24e0cf23.js","enterprise/license-paid.17df1ebcaccb24e0cf23.js.map"],"enterprise/license-purchase":["enterprise/license-purchase.ad261faf2a5960f45252.js","enterprise/license-purchase.ad261faf2a5960f45252.js.map"],"enterprise/on-site-buy-now":["enterprise/on-site-buy-now.758bcd76dd475858b0e2.js","enterprise/on-site-buy-now.758bcd76dd475858b0e2.js.map"],"enterprise/on-site-contact-confirmation":["enterprise/on-site-contact-confirmation.ef73f46976e258ab5e26.js","enterprise/on-site-contact-confirmation.ef73f46976e258ab5e26.js.map"],"enterprise/on-site-trial":["enterprise/on-site-trial.23382511e1b34d2d347d.js","enterprise/on-site-trial.23382511e1b34d2d347d.js.map"],"enterprise/orgs-terms":["enterprise/orgs-terms.6822148eb0f20bb26595.js","enterprise/orgs-terms.6822148eb0f20bb26595.js.map"],"enterprise/signup-confirmation":["enterprise/signup-confirmation.a253a8884513dab3ded1.js","enterprise/signup-confirmation.a253a8884513dab3ded1.js.map"],"errors/not-found":["errors/not-found.e080442936442a988554.js","errors/not-found.e080442936442a988554.js.map"],"errors/server":["errors/server.70493127e8ab76fbf3f8.js","errors/server.70493127e8ab76fbf3f8.js.map"],"errors/template":["errors/template.394798b6cc55c551c15b.js","errors/template.394798b6cc55c551c15b.js.map"],"flatpage/flatpage":["flatpage/flatpage.c43864a0dd54e0cfc208.js","flatpage/flatpage.c43864a0dd54e0cfc208.js.map"],"homepage/homepage":["homepage/homepage.f93937394384c8c4e241.js","homepage/homepage.f93937394384c8c4e241.js.map"],"homepage/homepage-logged-in":["homepage/homepage-logged-in.e057a2c975582c5f7340.js","homepage/homepage-logged-in.e057a2c975582c5f7340.js.map"],"npme/invite":["npme/invite.d1494c9a5de9d4a51bb6.js","npme/invite.d1494c9a5de9d4a51bb6.js.map"],"npme/invites":["npme/invites.2bb54aa2bd256cf9422c.js","npme/invites.2bb54aa2bd256cf9422c.js.map"],"npme/login":["npme/login.680bafea2604fedb9ce1.js","npme/login.680bafea2604fedb9ce1.js.map"],"npme/overrides/components/tutorials/creating-org":["npme/overrides/components/tutorials/creating-org.70d7fc4c304d1ffb2a90.js","npme/overrides/components/tutorials/creating-org.70d7fc4c304d1ffb2a90.js.map"],"npme/overrides/components/tutorials/default-registry":["npme/overrides/components/tutorials/default-registry.972bdf9cc2833fcecea5.js","npme/overrides/components/tutorials/default-registry.972bdf9cc2833fcecea5.js.map"],"npme/overrides/components/tutorials/installing-package":["npme/overrides/components/tutorials/installing-package.2a926aefda0f0bd11abe.js","npme/overrides/components/tutorials/installing-package.2a926aefda0f0bd11abe.js.map"],"npme/overrides/components/tutorials/publishing-package":["npme/overrides/components/tutorials/publishing-package.3c0d788b11ffe6a7dc9d.js","npme/overrides/components/tutorials/publishing-package.3c0d788b11ffe6a7dc9d.js.map"],"npme/overrides/components/tutorials/tabs":["npme/overrides/components/tutorials/tabs.c0733e0183c167e5d59d.js","npme/overrides/components/tutorials/tabs.c0733e0183c167e5d59d.js.map"],"npme/overrides/homepage":["npme/overrides/homepage.648c16564b99f2f845e0.js","npme/overrides/homepage.648c16564b99f2f845e0.js.map"],"npme/overrides/orgs/create":["npme/overrides/orgs/create.45be9f86e39a9ec5d0f8.js","npme/overrides/orgs/create.45be9f86e39a9ec5d0f8.js.map"],"npme/settings":["npme/settings.9a8081a13081b19fec65.js","npme/settings.9a8081a13081b19fec65.js.map"],"npme/setup":["npme/setup.002f7bcf531d5256aac8.js","npme/setup.002f7bcf531d5256aac8.js.map"],"npme/sso-config":["npme/sso-config.8d8ce9dd551e4f9edead.js","npme/sso-config.8d8ce9dd551e4f9edead.js.map"],"npme/users":["npme/users.d2ab15b54ade371294b1.js","npme/users.d2ab15b54ade371294b1.js.map"],"orgs/create":["orgs/create.583ac88532c785f4b24b.js","orgs/create.583ac88532c785f4b24b.js.map"],"orgs/detail":["orgs/detail.7e7de930e29ae7edb60b.js","orgs/detail.7e7de930e29ae7edb60b.js.map"],"orgs/invite":["orgs/invite.fa683d5df5d12a5c6b33.js","orgs/invite.fa683d5df5d12a5c6b33.js.map"],"orgs/upgrade":["orgs/upgrade.f65ffa5f4c61cb8b0f64.js","orgs/upgrade.f65ffa5f4c61cb8b0f64.js.map"],"package-list/dependents-list":["package-list/dependents-list.cf3390cdbc2e09e20004.js","package-list/dependents-list.cf3390cdbc2e09e20004.js.map"],"package-list/most-depended":["package-list/most-depended.b8040a23a47675e8bd7f.js","package-list/most-depended.b8040a23a47675e8bd7f.js.map"],"package-list/recently-updated":["package-list/recently-updated.8f6476d34c967b1534f7.js","package-list/recently-updated.8f6476d34c967b1534f7.js.map"],"package/package":["package/package.d62a72f2cdf88a5c02df.js","package/package.d62a72f2cdf88a5c02df.js.map"],"partners/detail":["partners/detail.41d61525e2e174c3918d.js","partners/detail.41d61525e2e174c3918d.js.map"],"partners/join":["partners/join.c0522db04f0e0a8a49f0.js","partners/join.c0522db04f0e0a8a49f0.js.map"],"partners/thanks":["partners/thanks.999787eb74af7024993c.js","partners/thanks.999787eb74af7024993c.js.map"],"profile/profile":["profile/profile.03a60febb0012fc75386.js","profile/profile.03a60febb0012fc75386.js.map"],"search/search":["search/search.c678db0f8ebf91816e23.js","search/search.c678db0f8ebf91816e23.js.map"],"settings/change-password":["settings/change-password.4728100be24a8c503541.js","settings/change-password.4728100be24a8c503541.js.map"],"settings/email":["settings/email.b2a8240f50794acc8da6.js","settings/email.b2a8240f50794acc8da6.js.map"],"settings/memberships":["settings/memberships.835702ad8a4343a62e63.js","settings/memberships.835702ad8a4343a62e63.js.map"],"settings/packages":["settings/packages.c2bd183a14b27586e549.js","settings/packages.c2bd183a14b27586e549.js.map"],"settings/profile":["settings/profile.1ef1a9fde5d9d189ad38.js","settings/profile.1ef1a9fde5d9d189ad38.js.map"],"teams/create":["teams/create.9e9846756a7aa7768daf.js","teams/create.9e9846756a7aa7768daf.js.map"],"teams/detail":["teams/detail.33218108af52bea8ed84.js","teams/detail.33218108af52bea8ed84.js.map"],"teams/list":["teams/list.9d30a32383a1176493dd.js","teams/list.9d30a32383a1176493dd.js.map"],"teams/packages":["teams/packages.c815b0143a43791f7f4a.js","teams/packages.c815b0143a43791f7f4a.js.map"],"teams/users":["teams/users.10a22939e81691d7b20a.js","teams/users.10a22939e81691d7b20a.js.map"],"tfa/enable":["tfa/enable.bc126c94e9e956827883.js","tfa/enable.bc126c94e9e956827883.js.map"],"tfa/showTFAQRCode":["tfa/showTFAQRCode.a9e7c86fd989d6bd5aa9.js","tfa/showTFAQRCode.a9e7c86fd989d6bd5aa9.js.map"],"tfa/showTFASuccess":["tfa/showTFASuccess.9c1d71ca983bae7daa05.js","tfa/showTFASuccess.9c1d71ca983bae7daa05.js.map"],"tfa/tfa-mode-selection":["tfa/tfa-mode-selection.b36c48cc5546bb07e9fb.js","tfa/tfa-mode-selection.b36c48cc5546bb07e9fb.js.map"],"tfa/tfa-password-entry":["tfa/tfa-password-entry.4ce3812c8be4cd56e659.js","tfa/tfa-password-entry.4ce3812c8be4cd56e659.js.map"],"tokens/create":["tokens/create.4c76a6d732bcaa671b5d.js","tokens/create.4c76a6d732bcaa671b5d.js.map"],"tokens/list":["tokens/list.dbb4a75b02e81a94efc9.js","tokens/list.dbb4a75b02e81a94efc9.js.map"],"vouchers/view":["vouchers/view.cde6ec1cf90f9f4ac657.js","vouchers/view.cde6ec1cf90f9f4ac657.js.map"]},"hash":"81abc4c4bc0dc173a51f","name":"package/package","containerId":"app","headerName":"x-spiferack","publicPath":"https://static.npmjs.com/"}</script><script crossorigin="anonymous" src="https://static.npmjs.com/commons.81abc4c4bc0dc173a51f.js"></script>
  <script crossorigin="anonymous" src="https://static.npmjs.com/minicssextractbug.608694f20ce046e1f914.js"></script>
  <script crossorigin="anonymous" src="https://static.npmjs.com/package/package.d62a72f2cdf88a5c02df.js"></script>
  </body>
  </html>`;

export const w3schools = `

<!DOCTYPE html>
<html lang="en-US">
<head>
<title>JavaScript Array includes() Method</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="Keywords" content="HTML,CSS,JavaScript,SQL,PHP,jQuery,XML,DOM,Bootstrap,Python,Java,Web development,W3C,tutorials,programming,training,learning,quiz,primer,lessons,references,examples,exercises,source code,colors,demos,tips">
<meta name="Description" content="Well organized and easy to understand Web building tutorials with lots of examples of how to use HTML, CSS, JavaScript, SQL, PHP, Python, Bootstrap, Java and XML.">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="/w3css/4/w3.css">
<link href='https://fonts.googleapis.com/css?family=Source Code Pro' rel='stylesheet'>

<style>
a:hover,a:active{color:#4CAF50}
table.w3-table-all{margin:20px 0}
/*OPPSETT AV TOP, TOPNAV, SIDENAV, MAIN, RIGHT OG FOOTER:*/
.top {
position:relative;
background-color:#ffffff;
height:68px;
padding-top:20px;
line-height:50px;
overflow:hidden;
z-index:2;
}
.w3schools-logo {
font-family:fontawesome;
text-decoration:none;
line-height:1;
-webkit-font-smoothing:antialiased;
-moz-osx-font-smoothing:grayscale;
font-size:37px;
letter-spacing:3px;
color:#555555;
display:block;
position:absolute;
top:17px;
}
.w3schools-logo .dotcom {color:#4CAF50}
.topnav {
position:relative;
z-index:2;
font-size:17px;
background-color:#5f5f5f;
color:#f1f1f1;
width:100%;
padding:0;
letter-spacing:1px;
font-family:"Segoe UI",Arial,sans-serif;
}
.topnav a{
padding:10px 15px 9px 15px !important;
}
.topnav .w3-bar a:hover,.topnav .w3-bar a:focus{
background-color:#000000 !important;
color:#ffffff !important;
}
.topnav .w3-bar a.active {
background-color:#4CAF50;
color:#ffffff;
}
a.topnav-icons {
width:52px !important;
font-size:20px !important;
padding-top:11px !important;
padding-bottom:13px !important;
}
a.topnav-icons.fa-home {font-size:22px !important}
a.topnav-icons.fa-menu {font-size:22px !important}
a.topnav-localicons {
font-size:20px !important;
padding-top:6px !important;
padding-bottom:12px !important;
}
i.fa-caret-down,i.fa-caret-up{width:10px}
#sidenav h2 {
font-size:21px;
padding-left:16px;
margin:-4px 0 4px 0;
width:204px;
}
#sidenav a {font-family:"Segoe UI",Arial,sans-serif;text-decoration:none;display:block;padding:2px 1px 1px 16px}
#sidenav a:hover,#sidenav a:focus {color:#000000;background-color:#cccccc;}
#sidenav a.active {background-color:#4CAF50;color:#ffffff}
#sidenav a.activesub:link,#sidenav a.activesub:visited {background-color:#ddd;color:#000;}
#sidenav a.activesub:hover,#sidenav a.activesub:active {background-color:#ccc;color:#000;}
#leftmenuinner {
position:fixed;
top:0;
padding-top:112px;
padding-bottom:0;    
height:100%;
width:220px;
background-color:transparent;
}
#leftmenuinnerinner {
height:100%;
width:100%;
overflow-y:scroll;
overflow-x:hidden;
padding-top:20px;
}
#main {padding:16px}
#mainLeaderboard {height:90px}
#right {text-align:center;padding:16px 16px 0 0}
#right a {text-decoration:none}
#right a:hover {text-decoration:underline}
#skyscraper {min-height:600px}
.sidesection {margin-bottom:32px;}
#sidesection_exercise a{display:block;padding:4px 10px;}
#sidesection_exercise a:hover,#sidesection_exercise a:active{background-color:#ccc;text-decoration:none;color:#000000;}
.bottomad {padding:0 16px 16px 0;float:left;width:auto;}
.footer a {text-decoration:none;}
.footer a:hover{text-decoration:underline;}
#nav_tutorials,#nav_references,#nav_exercises{-webkit-overflow-scrolling:touch;overflow:auto;}
#nav_tutorials::-webkit-scrollbar,#nav_references::-webkit-scrollbar,#nav_exercises::-webkit-scrollbar {width: 12px;}
#nav_tutorials::-webkit-scrollbar-track,#nav_references::-webkit-scrollbar-track,#nav_exercises::-webkit-scrollbar-track {background:#555555;}
#nav_tutorials::-webkit-scrollbar-thumb,#nav_references::-webkit-scrollbar-thumb,#nav_exercises::-webkit-scrollbar-thumb {background: #999999;}
#nav_tutorials,#nav_references,#nav_exercises {
display:none;
letter-spacing:0;
margin-top:44px;
}
#nav_tutorials a,#nav_references a,#nav_exercises a{
padding:2px 0 2px 6px!important;
}
#nav_tutorials a:focus,#nav_references a:focus,#nav_exercises a:focus{
color: #000;
background-color: #ccc;
}
#nav_tutorials h3,#nav_references h3,#nav_exercises h3{
padding-left:6px;
}
.ref_overview{display:none}
.tut_overview{
 display :none;
 margin-left:10px;
 background-color :#ddd;
 line-height:1.8em;
}
#sidenav a.activesub:link,#sidenav a.activesub:visited {background-color:#ddd;color:#000;}
#sidenav a.activesub:hover,#sidenav a.activesub:active {background-color:#ccc;color:#000;}
#sidenav a.active_overview:link,#sidenav a.active_overview:visited {background-color:#ccc;color:#000;}
.w3-example{background-color:#f1f1f1;padding:0.01em 16px;margin:20px 0;box-shadow:0 2px 4px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12)!important}
.nextprev a {font-size:17px;border:1px solid #cccccc;}
.nextprev a:link,.nextprev a:visited {background-color:#ffffff;color:#000000;}
.w3-example a:focus,.nextprev a:focus{box-shadow:0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);}
.nextprev a.w3-right,.nextprev a.w3-left {background-color:#4CAF50;color:#ffffff;border-color:#4CAF50}
#w3-exerciseform {background-color:#555555;padding:16px;color:#ffffff;}
#w3-exerciseform .exercisewindow {background-color:#ffffff;padding:16px;color:#000000;}
#w3-exerciseform .exerciseprecontainer {background-color:#f1f1f1;padding:16px;font-size:120%;font-family:Consolas,"Courier New", Courier, monospace;}
#w3-exerciseform .exerciseprecontainer pre[class*="language-"] {padding:1px;}
#w3-exerciseform .exerciseprecontainer pre {display: block;}
#w3-exerciseform .exerciseprecontainer input {padding:1px;border: 1px solid transparent;height:1.3em;}
.w3-theme {color:#fff !important;background-color:#73AD21 !important;background-color:#4CAF50 !important}
.w3-theme-border {border-color:#4CAF50 !important}
.sharethis a:hover {color:inherit;}
.fa-facebook-square,.fa-twitter-square,.fa-google-plus-square {padding:0 8px;}
.fa-facebook-square:hover, .fa-thumbs-o-up:hover {color:#3B5998;}
.fa-twitter-square:hover {color:#55acee;}
.fa-google-plus-square:hover {color:#dd4b39;}
#google_translate_element img {margin-bottom:-1px;}
#googleSearch {color:#000000;}
#googleSearch a {padding:0 !important;}
.searchdiv {max-width:400px;margin:auto;text-align:left;font-size:16px}
div.cse .gsc-control-cse, div.gsc-control-cse {background-color:transparent;border:none;padding:6px;margin:0px}
td.gsc-search-button input.gsc-search-button {background-color:#4CAF50;border-color:#4CAF50}
td.gsc-search-button input.gsc-search-button:hover {background-color:#46a049;}
input.gsc-input, .gsc-input-box, .gsc-input-box-hover, .gsc-input-box-focus, .gsc-search-button {
box-sizing:content-box; line-height:normal;}
.gsc-tabsArea div {overflow:visible;}
/*"nullstille" w3css:*/
.w3-main{transition:margin-left 0s;}
/*"nullstilling" slutt*/
@media (min-width:1675px) {
#main {width:79%}
#right {width:21%}
}
@media (max-width:992px) {
.top {height:100px}
.top img {display:block;margin:auto;}
.top .w3schools-logo {position:relative;top:0;width:100%;text-align:center;margin:auto}
.toptext {width:100%;text-align:center}
#sidenav {width:260px;
box-shadow:0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
}
#sidenav h2 {font-size:26px;width:100%;}
#sidenav a {padding:3px 2px 3px 24px;font-size:17px}
#leftmenuinner {  
overflow:auto;
-webkit-overflow-scrolling:touch;
height:100%;
position:relative;
width:auto;
padding-top:0;
background-color:#f1f1f1;
}
#leftmenuinnerinner {overflow-y:scroll}
.bottomad {float:none;text-align:center}
#skyscraper {min-height:60px}
}
@media screen and (max-width:600px) {
.w3-example, .w3-note, #w3-exerciseform {margin-left:-16px;margin-right:-16px;}
.top {height:68px}
.toptext {display:none}
}
@font-face {
font-family:'fontawesome';
src: url('../lib/fonts/fontawesome.eot?14663396');
src:url('../lib/fonts/fontawesome.eot?14663396#iefix') format('embedded-opentype'),
url('../lib/fonts/fontawesome.woff?14663396') format('woff'),
url('../lib/fonts/fontawesome.ttf?14663396') format('truetype'),
url('../lib/fonts/fontawesome.svg?14663396#fontawesome') format('svg');
font-weight:normal;
font-style:normal;
}
.fa {
display:inline-block;
font:normal normal normal 14px/1 FontAwesome;
font-size:inherit;
text-rendering:auto;
-webkit-font-smoothing:antialiased;
-moz-osx-font-smoothing:grayscale;
transform:translate(0, 0);
}
.fa-2x {
 font-size:2em;
}
.fa-home:before {content:'\\e800';}
.fa-menu:before {content: '\\f0c9';}
.fa-globe:before {content:'\\e801';}
.fa-search:before {content:'\\e802'; }
.fa-thumbs-o-up:before {content:'\\e803';}
.fa-left-open:before {content:'\\e804';}
.fa-right-open:before {content:'\\e805';}
.fa-facebook-square:before {content:'\\e806';}
.fa-google-plus-square:before {content:'\\e807';}
.fa-twitter-square:before {content:'\\e808';}
.fa-caret-down:before {content:'\\e809';}
.fa-caret-up:before {content:'\\e80a';}
.fa.fa-adjust:before { content: '\\e80b'; }
span.marked, span.deprecated {
 color:#e80000;
 background-color:transparent;
}
.intro {font-size:16px}
.w3-btn, .w3-btn:link, .w3-btn:visited {color:#FFFFFF;background-color:#4CAF50}
a.w3-btn[href*="exercise.asp"],a.w3-btn[href*="exercise_js.asp"] {margin:10px 5px 0 0}
a.btnplayit,a.btnplayit:link,a.btnplayit:visited {background-color:#FFAD33;padding:1px 10px 2px 10px}
a.btnplayit:hover,a.btnplayit:active {background-color:#ffffff;color:#FFAD33}
a.btnplayit:hover {box-shadow:0 4px 8px 0 rgba(0,0,0,0.2);}
a.btnsmall:link,a.btnsmall:visited,a.btnsmall:active,a.btnsmall:hover {
float:right;padding:1px 10px 2px 10px;font:15px Verdana, sans-serif;}
a.btnsmall:hover {box-shadow:0 4px 8px 0 rgba(0,0,0,0.2);}
a.btnsmall:active,a.btnsmall:hover {color:#4CAF50;background-color:#ffffff}
.tagcolor{color:mediumblue}
.tagnamecolor{color:brown}
.attributecolor{color:red}
.attributevaluecolor{color:mediumblue}
.commentcolor{color:green}
.cssselectorcolor{color:brown}
.csspropertycolor{color:red}
.csspropertyvaluecolor{color:mediumblue}
.cssdelimitercolor{color:black}
.cssimportantcolor{color:red}  
.jscolor{color:black}
.jskeywordcolor{color:mediumblue}
.jsstringcolor{color:brown}
.jsnumbercolor{color:red}
.jspropertycolor{color:black}
.javacolor{color:black}
.javakeywordcolor{color:mediumblue}
.javastringcolor{color:brown}
.javanumbercolor{color:red}
.javapropertycolor{color:black}
.kotlincolor{color:black}
.kotlinkeywordcolor{color:mediumblue}
.kotlinstringcolor{color:brown}
.kotlinnumbercolor{color:red}
.kotlinpropertycolor{color:black}
.phptagcolor{color:red}
.phpcolor{color:black}
.phpkeywordcolor{color:mediumblue}
.phpglobalcolor{color:goldenrod}
.phpstringcolor{color:brown}
.phpnumbercolor{color:red}  
.pythoncolor{color:black}
.pythonkeywordcolor{color:mediumblue}
.pythonstringcolor{color:brown}
.pythonnumbercolor{color:red}  
.angularstatementcolor{color:red}
.sqlcolor{color:black}
.sqlkeywordcolor{color:mediumblue}
.sqlstringcolor{color:brown}
.sqlnumbercolor{color:} 
.darktheme {background-color:rgb(40,44,52);color:white;}
.darktheme .tagcolor{color:#88ccbb/*green2*/!important}
.darktheme .tagnamecolor{color:#ff9999/*red*/!important}
.darktheme .attributecolor{color:#c5a5c5/*purple*/!important}
.darktheme .attributevaluecolor{color:#88c999/*green*/!important}
.darktheme .commentcolor{color:color: #999;!important}
.darktheme .cssselectorcolor{color:#ff9999/*red*/!important}
.darktheme .csspropertycolor{color:#c5a5c5/*purple*/!important}
.darktheme .csspropertyvaluecolor{color:#88c999/*green*/!important}
.darktheme .cssdelimitercolor{color:white!important}
.darktheme .cssimportantcolor{color:color:#ff9999/*red*/!important}
.darktheme .jscolor{color:white!important}
.darktheme .jskeywordcolor{color:#c5a5c5/*purple*/!important}
.darktheme .jsstringcolor{color:#88c999/*green*/!important}
.darktheme .jsnumbercolor{color:#80b6ff/*blue*/!important}
.darktheme .jspropertycolor{color:white!important}
.darktheme .javacolor{color:white!important}
.darktheme .javakeywordcolor{color:#88c999/*green*/!important}
.darktheme .javastringcolor{color:#88c999/*green*/!important}
.darktheme .javanumbercolor{color:#88c999/*green*/!important}
.darktheme .javapropertycolor{color:white!important}
.darktheme .kotlincolor{color:white!important}
.darktheme .kotlinkeywordcolor{color:#88c999/*green*/!important}
.darktheme .kotlinstringcolor{color:#88c999/*green*/!important}
.darktheme .kotlinnumbercolor{color:#88c999/*green*/!important}
.darktheme .kotlinpropertycolor{color:white!important}
.darktheme .phptagcolor{color:#999!important}
.darktheme .phpcolor{color:white!important}
.darktheme .phpkeywordcolor{color:#ff9999/*red*/!important}
.darktheme .phpglobalcolor{color:white!important}
.darktheme .phpstringcolor{color:color:#88c999/*green*/!important}
.darktheme .phpnumbercolor{color:#88c999/*green*/!important}
.darktheme .pythoncolor{color:white!important}
.darktheme .pythonkeywordcolor{color:#ff9999/*red*/!important}
.darktheme .pythonstringcolor{color:#88c999/*green*/!important}
.darktheme .pythonnumbercolor{color:#88c999/*green*/!important}
.darktheme .angularstatementcolor{color:#ff9999/*red*/!important}
.darktheme .sqlcolor{color:white!important}
.darktheme .sqlkeywordcolor{color:#80b6ff/*blue*/!important}
.darktheme .sqlstringcolor{color:#88c999/*green*/!important}
.darktheme .sqlnumbercolor{color:}
@media only screen and (max-device-width: 480px) {
.w3-code, .w3-codespan,#w3-exerciseform .exerciseprecontainer {font-family: 'Source Code Pro',Menlo,Consolas,monospace;}
.w3-code {font-size:14px;}
.w3-codespan {font-size:15px;}
#w3-exerciseform .exerciseprecontainer {font-size:15px;}
#w3-exerciseform .exerciseprecontainer input {padding:0;height:1.5em}
}
@media screen and (max-width:700px) {
#mainLeaderboard {height:60px}
#div-gpt-ad-1422003450156-0 {float:none;margin-left:auto;margin-right:auto}
#div-gpt-ad-1422003450156-3 {float:none;margin-left:auto;margin-right:auto}
}
@media (max-width:1700px) {#topnav .w3-bar:nth-of-type(1) a:nth-of-type(17){display:none;}}
@media (max-width:1600px) {#topnav .w3-bar:nth-of-type(1) a:nth-of-type(13){display:none;}}
@media (max-width:1510px) {#topnav .w3-bar:nth-of-type(1) a:nth-of-type(12){display:none;}}
@media (max-width:1450px) {#topnav .w3-bar:nth-of-type(1) a:nth-of-type(11){display:none;}}
@media (max-width:1330px) {#topnav .w3-bar:nth-of-type(1) a:nth-of-type(10){display:none;}}
@media (max-width:1200px) {#topnav .w3-bar:nth-of-type(1) a:nth-of-type(9){display:none;}}
@media (max-width:1100px) {#topnav .w3-bar:nth-of-type(1) a:nth-of-type(8){display:none;}}
@media (max-width:1000px) {#topnav .w3-bar:nth-of-type(1) a:nth-of-type(7){display:none;}}
@media (max-width:992px) {#topnav .w3-bar:nth-of-type(1) a:nth-of-type(6){display:none;}}
@media (max-width:930px) {#topnav .w3-bar:nth-of-type(1) a:nth-of-type(18){display:none;}}
@media (max-width:800px) {#topnav .w3-bar:nth-of-type(1) a:nth-of-type(19){display:none;}}
@media (max-width:650px) {#topnav .w3-bar:nth-of-type(1) a:nth-of-type(5){display:none;} #topnav .w3-bar:nth-of-type(1) a:nth-of-type(16){display:none;}}
@media (max-width:460px) {#topnav .w3-bar:nth-of-type(1) a:nth-of-type(4){display:none;}}
@media (max-width:400px) {#topnav .w3-bar:nth-of-type(1) a:nth-of-type(3){display:none;}}
.w3-note{background-color:#ffffcc;border-left:6px solid #ffeb3b}
.w3-warning{background-color:#ffdddd;border-left:6px solid #f44336}
.w3-info{background-color:#ddffdd;border-left:6px solid #4CAF50}
hr[id^="ez-insert-after-placeholder"] {margin-top: 0;}
.phonebr {display:none;}
@media screen and (max-width: 475px) {.phonebr {display:initial;}}

/*NYTT:*/
#main {
  padding:16px 32px 32px 32px;
  border-right: 1px solid #f1f1f1;
}
#right {
  padding:16px 8px;
}
.sidesection .w3-left-align {
  text-align:center!important;
}
#footer {padding:32px;border-top:1px solid #f1f1f1;}
#footer hr:first-child {
  display:none;
}
.w3-info {
  background-color: #d4edda;
  border-left: none;
  padding:32px;
  margin:24px;
  margin-left:-32px;
  margin-right:-32px;
}
.w3-example {
  padding: 8px 20px;
  margin: 24px -20px;
  box-shadow:none!important;
}
.w3-note, .w3-warning {
  border-left: none;
}
.w3-panel {
  margin-top: 24px;
  margin-bottom: 24px;
  margin-left:-32px;
  margin-right:-32px;
  padding:16px 32px;
}
h1 {
  font-size: 42px;
}
h2 {
  font-size: 32px;
}
.w3-btn:hover,.w3-btn:active,.w3-example a:focus,.nextprev a:focus {
  box-shadow: none;
  background-color: #46a049!important;
}
.w3-btn:hover.w3-blue,.w3-btn:active.w3-blue,.w3-button:hover.w3-blue,.w3-button:active.w3-blue {
  background-color: #0d8bf2!important;color: #fff!important;
}
.nextprev .w3-btn:not(.w3-left):not(.w3-right):hover,.nextprev .w3-btn:not(.w3-left):not(.w3-right):active,.nextprev .w3-btn:not(.w3-left):not(.w3-right):focus {
  background-color: #f1f1f1!important;
}
a.btnsmall:hover {box-shadow:none;}
a.btnsmall:active,a.btnsmall:hover {color:#fff;}
a.btnplayit:hover,a.btnplayit:active {background-color:#ff9900!important;color:#fff}
a.btnplayit:hover {box-shadow:none;}
#w3-exerciseform {
  padding: 20px;
  margin:32px -20px;
}
p {
  margin-top: 1.2em;
  margin-bottom: 1.2em;
  font-size: 15px;
}
hr {
  margin:20px -16px;
}
.w3-codespan {
  font-size:105%;
}
.w3-example p,.w3-panel p {
  margin-top: 1em;
  margin-bottom: 1em;
}

#midcontentadcontainer,#mainLeaderboard {
  text-align:center;
  margin-left:-20px;
  margin-right:-20px;
}
@media screen and (max-width:600px) {
.w3-example, #w3-exerciseform {margin-left:-32px;margin-right:-32px;}
}

@media only screen and (max-device-width: 480px) {
#main {padding:24px}
h1 {
  font-size: 30px;
}
h2 {
  font-size: 25px;
}
.w3-example {
  padding: 8px 16px;
  margin: 24px -24px;
}
#w3-exerciseform {
  padding: 8px 16px 16px 16px;
  margin: 24px -24px;
}
.w3-panel,.w3-info {
  margin-left:-24px;
  margin-right:-24px;
}

}

</style>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-3855518-1', 'auto');
ga('require', 'displayfeatures');
ga('send', 'pageview');
</script>

<script src="//snigelweb-com.videoplayerhub.com/videoloader.js" async></script>

<script type='text/javascript'>
var k42 = false;
var googletag = googletag || {}; googletag.cmd = googletag.cmd || [];

k42 = true;
(adsbygoogle=window.adsbygoogle||[]).pauseAdRequests=1;

var snhb = snhb || {}; snhb.queue = snhb.queue || [];
snhb.options = {
               logOutputEnabled : false,
               autoStartAuction: false,
               gdpr: {
                     mainGeo: "us",
                     reconsiderationAppealIntervalSeconds: 0
                     }
               };

</script>
<script src="//static.h-bid.com/sncmp/sncmp_stub.min.js" type="text/javascript"></script>
<script>
\t\t\twindow.__cmp('setLogo', "//www.w3schools.com/images/w3schoolscom_gray.gif", function(result){
\t       \t\tsnhb.console.log("Logo set");
\t    \t});
\t\t\twindow.__cmp('setPrivacyPolicy', "//www.w3schools.com/about/about_privacy.asp", function(result){
\t       \t\tsnhb.console.log("Privacy policy URI set");
\t    \t});
\t\t\twindow.__cmp('enableBanner', null, function(result) {
\t       \t\tsnhb.console.log("Banner mode enabled");
\t\t\t});
\t\t\t__cmp('enablePopupDismissable', null, function(result) {});
\t\t\twindow.__cmp('disableBannerPrivacyPolicyButton', null, function(result) {
\t       \t\tsnhb.console.log("Banner mode without privacy policy button enabled");
\t\t\t});
      window.__cmp('setTranslationFiles', { path: '//www.w3schools.com/lib/', locales: ["en"] }, function(result) {});
      __cmp('setCSS', '//www.w3schools.com/lib/cmp.css', function(result){} );
</script>
<script async type="text/javascript" src="//static.h-bid.com/w3schools.com/20190327/snhb-w3schools.com.min.js?20190327"></script>
<script>
  snhb.queue.push(function(){

    snhb.startAuction(["main_leaderboard", "wide_skyscraper", "bottom_medium_rectangle", "right_bottom_medium_rectangle"]);

  });
</script>
<script type='text/javascript'>
var stickyadstatus = "";
function fix_stickyad() {
  document.getElementById("stickypos").style.position = "sticky";
  var elem = document.getElementById("stickyadcontainer");
  if (!elem) {return false;}
  if (document.getElementById("skyscraper")) {
    var skyWidth = Number(w3_getStyleValue(document.getElementById("skyscraper"), "width").replace("px", ""));  
    }
  else {
    var skyWidth = Number(w3_getStyleValue(document.getElementById("right"), "width").replace("px", ""));  
  }
  elem.style.width = skyWidth + "px";
  if (window.innerWidth <= 992) {
    elem.style.position = "";
    elem.style.top = stickypos + "px";
    return false;
  }
  var stickypos = document.getElementById("stickypos").offsetTop;
  var docTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;
  var adHeight = Number(w3_getStyleValue(elem, "height").replace("px", ""));
  if (stickyadstatus == "") {
    if ((stickypos - docTop) < 60) {
      elem.style.position = "fixed";
      elem.style.top = "60px";
      stickyadstatus = "sticky";
      document.getElementById("stickypos").style.position = "sticky";

    }
  } else {
    if ((docTop + 60) - stickypos < 0) {  
      elem.style.position = "";
      elem.style.top = stickypos + "px";
      stickyadstatus = "";
      document.getElementById("stickypos").style.position = "static";
    }
  }
  if (stickyadstatus == "sticky") {
    if ((docTop + adHeight + 60) > document.getElementById("footer").offsetTop) {
      elem.style.position = "absolute";
      elem.style.top = (document.getElementById("footer").offsetTop - adHeight) + "px";
      document.getElementById("stickypos").style.position = "static";
    } else {
        elem.style.position = "fixed";
        elem.style.top = "60px";
        stickyadstatus = "sticky";
        document.getElementById("stickypos").style.position = "sticky";
    }
  }
}
function w3_getStyleValue(elmnt,style) {
  if (window.getComputedStyle) {
    return window.getComputedStyle(elmnt,null).getPropertyValue(style);
  } else {
    return elmnt.currentStyle[style];
  }
}
</script>
<link rel="stylesheet" type="text/css" href="/browserref.css">
</head>
<body>
<div class='w3-container top'>
  <a class='w3schools-logo notranslate' href='//www.w3schools.com'>w3schools<span class='dotcom'>.com</span></a>
  <div class='w3-right w3-hide-small w3-wide toptext' style="font-family:'Segoe UI',Arial,sans-serif">THE WORLD'S LARGEST WEB DEVELOPER SITE</div>
</div>

<div style='display:none;position:absolute;z-index:4;right:52px;height:44px;background-color:#5f5f5f;letter-spacing:normal;' id='googleSearch'>
  <div class='gcse-search'></div>
</div>
<div style='display:none;position:absolute;z-index:3;right:111px;height:44px;background-color:#5f5f5f;text-align:right;padding-top:9px;' id='google_translate_element'></div>

<div class='w3-card-2 topnav notranslate' id='topnav'>
  <div style="overflow:auto;">
    <div class="w3-bar w3-left" style="width:100%;overflow:hidden;height:44px">
      <a href='javascript:void(0);' class='topnav-icons fa fa-menu w3-hide-large w3-left w3-bar-item w3-button' onclick='open_menu()' title='Menu'></a>
      <a href='/default.asp' class='topnav-icons fa fa-home w3-left w3-bar-item w3-button' title='Home'></a>
      <a class="w3-bar-item w3-button" href='/html/default.asp' title='HTML Tutorial'>HTML</a>
      <a class="w3-bar-item w3-button" href='/css/default.asp' title='CSS Tutorial'>CSS</a>
      <a class="w3-bar-item w3-button" href='/js/default.asp' title='JavaScript Tutorial'>JAVASCRIPT</a>
      <a class="w3-bar-item w3-button" href='/sql/default.asp' title='SQL Tutorial'>SQL</a>
      <a class="w3-bar-item w3-button" href='/python/default.asp' title='Python Tutorial'>PYTHON</a>
      <a class="w3-bar-item w3-button" href='/php/default.asp' title='PHP Tutorial'>PHP</a>
      <a class="w3-bar-item w3-button" href='/bootstrap/bootstrap_ver.asp' title='Bootstrap Tutorial'>BOOTSTRAP</a>
      <a class="w3-bar-item w3-button" href='/howto/default.asp' title='How To'>HOW TO</a>
      <a class="w3-bar-item w3-button" href='/w3css/default.asp' title='W3.CSS Tutorial'>W3.CSS</a>
      <a class="w3-bar-item w3-button" href='/jquery/default.asp' title='jQuery Tutorial'>JQUERY</a>
      <a class="w3-bar-item w3-button" href='/xml/default.asp' title='XML Tutorial'>XML</a>
      <a class="w3-bar-item w3-button" id='topnavbtn_tutorials' href='javascript:void(0);' onclick='w3_open_nav("tutorials")' title='Tutorials'>MORE <i class='fa fa-caret-down'></i><i class='fa fa-caret-up' style='display:none'></i></a>
      <a href='javascript:void(0);' class='topnav-icons fa w3-right w3-bar-item w3-button' onclick='open_search(this)' title='Search W3Schools'>&#xe802;</a>
      <a href='javascript:void(0);' class='topnav-icons fa w3-right w3-bar-item w3-button' onclick='open_translate(this)' title='Translate W3Schools'>&#xe801;</a>
      <a class="w3-bar-item w3-button w3-right" target="_blank" href='/forum/default.asp'>FORUM</a>
      <a class="w3-bar-item w3-button w3-right" id='topnavbtn_exercises' href='javascript:void(0);' onclick='w3_open_nav("exercises")' title='Exercises'>EXERCISES <i class='fa fa-caret-down'></i><i class='fa fa-caret-up' style='display:none'></i></a>
      <a class="w3-bar-item w3-button w3-right" id='topnavbtn_references' href='javascript:void(0);' onclick='w3_open_nav("references")' title='References'>REFERENCES <i class='fa fa-caret-down'></i><i class='fa fa-caret-up' style='display:none'></i></a>
    </div>
    <div id='nav_tutorials' class='w3-bar-block w3-card-2' style="display:none;">
      <span onclick='w3_close_nav("tutorials")' class='w3-button w3-xlarge w3-right' style="position:absolute;right:0;font-weight:bold;">&times;</span>
      <div class='w3-row-padding' style="padding:24px 48px">
        <div class='w3-col l3 m6'>
          <h3>HTML and CSS</h3>
          <a class="w3-bar-item w3-button" href='/html/default.asp'>Learn HTML</a>
          <a class="w3-bar-item w3-button" href='/css/default.asp'>Learn CSS</a>
          <a class="w3-bar-item w3-button" href='/bootstrap/bootstrap_ver.asp'>Learn Bootstrap</a>
          <a class="w3-bar-item w3-button" href='/w3css/default.asp'>Learn W3.CSS</a>
          <a class="w3-bar-item w3-button" href='/colors/default.asp'>Learn Colors</a>
          <a class="w3-bar-item w3-button" href='/icons/default.asp'>Learn Icons</a>
          <a class="w3-bar-item w3-button" href='/graphics/default.asp'>Learn Graphics</a>
          <a class="w3-bar-item w3-button" href='/graphics/svg_intro.asp'>Learn SVG</a>
          <a class="w3-bar-item w3-button" href='/graphics/canvas_intro.asp'>Learn Canvas</a>
          <a class="w3-bar-item w3-button" href='/howto/default.asp'>Learn How To</a>
          <a class="w3-bar-item w3-button" href='/sass/default.asp'>Learn Sass</a>          
          <div class="w3-hide-large w3-hide-small">
            <h3>XML</h3>
            <a class="w3-bar-item w3-button" href='/xml/default.asp'>Learn XML</a>
            <a class="w3-bar-item w3-button" href='/xml/ajax_intro.asp'>Learn XML AJAX</a>
            <a class="w3-bar-item w3-button" href="/xml/dom_intro.asp">Learn XML DOM</a>
            <a class="w3-bar-item w3-button" href='/xml/xml_dtd_intro.asp'>Learn XML DTD</a>
            <a class="w3-bar-item w3-button" href='/xml/schema_intro.asp'>Learn XML Schema</a>
            <a class="w3-bar-item w3-button" href='/xml/xsl_intro.asp'>Learn XSLT</a>
            <a class="w3-bar-item w3-button" href='/xml/xpath_intro.asp'>Learn XPath</a>
            <a class="w3-bar-item w3-button" href='/xml/xquery_intro.asp'>Learn XQuery</a>
          </div>
        </div>
        <div class='w3-col l3 m6'>
          <h3>JavaScript</h3>
          <a class="w3-bar-item w3-button" href='/js/default.asp'>Learn JavaScript</a>
          <a class="w3-bar-item w3-button" href='/jquery/default.asp'>Learn jQuery</a>
          <a class="w3-bar-item w3-button" href='/react/default.asp'>Learn React</a>
          <a class="w3-bar-item w3-button" href='/angular/default.asp'>Learn AngularJS</a>
          <a class="w3-bar-item w3-button" href="/js/js_json_intro.asp">Learn JSON</a>
          <a class="w3-bar-item w3-button" href='/js/js_ajax_intro.asp'>Learn AJAX</a>
          <a class="w3-bar-item w3-button" href="/w3js/default.asp">Learn W3.JS</a>
          <h3>Programming</h3>
          <a class="w3-bar-item w3-button" href='/python/default.asp'>Learn Python</a>
          <a class="w3-bar-item w3-button" href='/java/default.asp'>Learn Java</a>
          <a class="w3-bar-item w3-button" href='/cpp/default.asp'>Learn C++</a>
          <div class="w3-hide-small"><br class="w3-hide-medium w3_hide-small"><br class="w3-hide-medium w3_hide-small"></div>
        </div>
        <div class='w3-col l3 m6'>
          <h3>Server Side</h3>
          <a class="w3-bar-item w3-button" href='/sql/default.asp'>Learn SQL</a>
          <a class="w3-bar-item w3-button" href='/php/default.asp'>Learn PHP</a>
          <a class="w3-bar-item w3-button" href='/asp/default.asp'>Learn ASP</a>
          <a class="w3-bar-item w3-button" href='/nodejs/default.asp'>Learn Node.js</a>
          <a class="w3-bar-item w3-button" href='/nodejs/nodejs_raspberrypi.asp'>Learn Raspberry Pi</a>          
          <h3>Web Building</h3>
          <a class="w3-bar-item w3-button" href="/w3css/w3css_templates.asp">Web Templates</a>
          <a class="w3-bar-item w3-button" href='/browsers/default.asp'>Web Statistics</a>
          <a class="w3-bar-item w3-button" href='/cert/default.asp'>Web Certificates</a>
          <a class="w3-bar-item w3-button" href='/tryit/default.asp'>Web Editor</a>
          <a class="w3-bar-item w3-button" href="/whatis/default.asp">Web Development</a>
        </div>
        <div class='w3-col l3 m6 w3-hide-medium'>
          <h3>XML</h3>
          <a class="w3-bar-item w3-button" href='/xml/default.asp'>Learn XML</a>
          <a class="w3-bar-item w3-button" href='/xml/ajax_intro.asp'>Learn XML AJAX</a>
          <a class="w3-bar-item w3-button" href="/xml/dom_intro.asp">Learn XML DOM</a>
          <a class="w3-bar-item w3-button" href='/xml/xml_dtd_intro.asp'>Learn XML DTD</a>
          <a class="w3-bar-item w3-button" href='/xml/schema_intro.asp'>Learn XML Schema</a>
          <a class="w3-bar-item w3-button" href='/xml/xsl_intro.asp'>Learn XSLT</a>
          <a class="w3-bar-item w3-button" href='/xml/xpath_intro.asp'>Learn XPath</a>
          <a class="w3-bar-item w3-button" href='/xml/xquery_intro.asp'>Learn XQuery</a>
        </div>
      </div>
      <br>
    </div>
    <div id='nav_references' class='w3-bar-block w3-card-2'>
      <span onclick='w3_close_nav("references")' class='w3-button w3-xlarge w3-right' style="position:absolute;right:0;font-weight:bold;">&times;</span>
      <div class='w3-row-padding' style="padding:24px 48px">
        <div class='w3-col l3 m6'>
          <h3>HTML</h3>
          <a class="w3-bar-item w3-button" href='/tags/default.asp'>HTML Tag Reference</a>
          <a class="w3-bar-item w3-button" href='/tags/ref_eventattributes.asp'>HTML Event Reference</a>
          <a class="w3-bar-item w3-button" href='/colors/default.asp'>HTML Color Reference</a>
          <a class="w3-bar-item w3-button" href='/tags/ref_attributes.asp'>HTML Attribute Reference</a>
          <a class="w3-bar-item w3-button" href='/tags/ref_canvas.asp'>HTML Canvas Reference</a>
          <a class="w3-bar-item w3-button" href='/graphics/svg_reference.asp'>HTML SVG Reference</a>
          <a class="w3-bar-item w3-button" href='/charsets/default.asp'>HTML Character Sets</a>
          <a class="w3-bar-item w3-button" href='/graphics/google_maps_reference.asp'>Google Maps Reference</a>
          <h3>CSS</h3>
          <a class="w3-bar-item w3-button" href='/cssref/default.asp'>CSS Reference</a>
          <a class="w3-bar-item w3-button" href='/cssref/css3_browsersupport.asp'>CSS Browser Support</a>
          <a class="w3-bar-item w3-button" href='/cssref/css_selectors.asp'>CSS Selector Reference</a>
          <a class="w3-bar-item w3-button" href='/bootstrap/bootstrap_ref_all_classes.asp'>Bootstrap 3 Reference</a>
          <a class="w3-bar-item w3-button" href='/bootstrap4/bootstrap_ref_all_classes.asp'>Bootstrap 4 Reference</a>
          <a class="w3-bar-item w3-button" href='/w3css/w3css_references.asp'>W3.CSS Reference</a>
          <a class="w3-bar-item w3-button" href='/icons/icons_reference.asp'>Icon Reference</a>
          <a class="w3-bar-item w3-button" href='/sass/sass_functions_string.asp'>Sass Reference</a>
       </div>
        <div class='w3-col l3 m6'>
          <h3>JavaScript</h3>
          <a class="w3-bar-item w3-button" href='/jsref/default.asp'>JavaScript Reference</a>
          <a class="w3-bar-item w3-button" href='/jsref/default.asp'>HTML DOM Reference</a>
          <a class="w3-bar-item w3-button" href='/jquery/jquery_ref_overview.asp'>jQuery Reference</a>
          <a class="w3-bar-item w3-button" href='/angular/angular_ref_directives.asp'>AngularJS Reference</a>
          <a class="w3-bar-item w3-button" href="/w3js/w3js_references.asp">W3.JS Reference</a>
          <h3>Programming</h3>
          <a class="w3-bar-item w3-button" href='/python/python_reference.asp'>Python Reference</a>
          <a class="w3-bar-item w3-button" href='/java/java_ref_keywords.asp'>Java Reference</a>
        </div>
        <div class='w3-col l3 m6'>
          <h3>Server Side</h3>
          <a class="w3-bar-item w3-button" href='/sql/sql_ref_keywords.asp'>SQL Reference</a>
          <a class="w3-bar-item w3-button" href='/php/php_ref_overview.asp'>PHP Reference</a>
          <a class="w3-bar-item w3-button" href='/asp/asp_ref_response.asp'>ASP Reference</a>
          <h3>XML</h3>
          <a class="w3-bar-item w3-button" href='/xml/dom_nodetype.asp'>XML Reference</a>
          <a class="w3-bar-item w3-button" href='/xml/dom_http.asp'>XML Http Reference</a>
          <a class="w3-bar-item w3-button" href='/xml/xsl_elementref.asp'>XSLT Reference</a>
          <a class="w3-bar-item w3-button" href='/xml/schema_elements_ref.asp'>XML Schema Reference</a>
        </div>
        <div class='w3-col l3 m6 w3-hide-medium w3-hide-small'>
          <h3>Character Sets</h3>
          <a class="w3-bar-item w3-button" href='/charsets/default.asp'>HTML Character Sets</a>
          <a class="w3-bar-item w3-button" href='/charsets/ref_html_ascii.asp'>HTML ASCII</a>
          <a class="w3-bar-item w3-button" href='/charsets/ref_html_ansi.asp'>HTML ANSI</a>
          <a class="w3-bar-item w3-button" href='/charsets/ref_html_ansi.asp'>HTML Windows-1252</a>
          <a class="w3-bar-item w3-button" href='/charsets/ref_html_8859.asp'>HTML ISO-8859-1</a>
          <a class="w3-bar-item w3-button" href='/charsets/ref_html_symbols.asp'>HTML Symbols</a>
          <a class="w3-bar-item w3-button" href='/charsets/ref_html_utf8.asp'>HTML UTF-8</a>
        </div>
      </div>
      <br>
    </div>
    <div id='nav_exercises' class='w3-bar-block w3-card-2'>
      <span onclick='w3_close_nav("exercises")' class='w3-button w3-xlarge w3-right' style="position:absolute;right:0;font-weight:bold;">&times;</span>
      <div class='w3-row-padding' style="padding:24px 48px">
        <div class='w3-col l4 m6'>
          <h3>Exercises</h3>
          <a class="w3-bar-item w3-button" href="/html/html_exercises.asp">HTML Exercises</a>
          <a class="w3-bar-item w3-button" href="/css/css_exercises.asp">CSS Exercises</a>
          <a class="w3-bar-item w3-button" href="/js/js_exercises.asp">JavaScript Exercises</a>
          <a class="w3-bar-item w3-button" href="/sql/sql_exercises.asp">SQL Exercises</a>
          <a class="w3-bar-item w3-button" href="/php/php_exercises.asp">PHP Exercises</a>
          <a class="w3-bar-item w3-button" href="/python/python_exercises.asp">Python Exercises</a>
          <a class="w3-bar-item w3-button" href="/jquery/jquery_exercises.asp">jQuery Exercises</a>
          <a class="w3-bar-item w3-button" href="/bootstrap/bootstrap_exercises.asp">Bootstrap Exercises</a>
          <a class="w3-bar-item w3-button" href="/java/java_exercises.asp">Java Exercises</a>
          <a class="w3-bar-item w3-button" href="/cpp/cpp_exercises.asp">C++ Exercises</a>
        </div>
        <div class='w3-col l4 m6'>
          <h3>Quizzes</h3>
          <a class="w3-bar-item w3-button" href='/html/html_quiz.asp' target='_top'>HTML Quiz</a>
          <a class="w3-bar-item w3-button" href='/css/css_quiz.asp' target='_top'>CSS Quiz</a>
          <a class="w3-bar-item w3-button" href='/js/js_quiz.asp' target='_top'>JavaScript Quiz</a>
          <a class="w3-bar-item w3-button" href="/sql/sql_quiz.asp" target="_top">SQL Quiz</a>
          <a class="w3-bar-item w3-button" href='/php/php_quiz.asp' target='_top'>PHP Quiz</a>
          <a class="w3-bar-item w3-button" href='/python/python_quiz.asp' target='_top'>Python Quiz</a>
          <a class="w3-bar-item w3-button" href='/jquery/jquery_quiz.asp' target='_top'>jQuery Quiz</a>
          <a class="w3-bar-item w3-button" href='/bootstrap/bootstrap_quiz.asp' target='_top'>Bootstrap Quiz</a>
          <a class="w3-bar-item w3-button" href='/xml/xml_quiz.asp' target='_top'>XML Quiz</a>
        </div>
        <div class='w3-col l4 m12'>
         <h3>Certificates</h3>
         <a class="w3-bar-item w3-button" href="/cert/cert_html_new.asp" target="_top">HTML Certificate</a>
         <a class="w3-bar-item w3-button" href="/cert/cert_css.asp" target="_top">CSS Certificate</a>
         <a class="w3-bar-item w3-button" href="/cert/cert_javascript.asp" target="_top">JavaScript Certificate</a>
         <a class="w3-bar-item w3-button" href="/cert/cert_sql.asp" target="_top">SQL Certificate</a>
         <a class="w3-bar-item w3-button" href="/cert/cert_php.asp" target="_top">PHP Certificate</a>
         <a class="w3-bar-item w3-button" href="/cert/cert_python.asp" target="_top">Python Certificate</a>
         <a class="w3-bar-item w3-button" href="/cert/cert_jquery.asp" target="_top">jQuery Certificate</a>
         <a class="w3-bar-item w3-button" href="/cert/cert_bootstrap.asp" target="_top">Bootstrap Certificate</a>
         <a class="w3-bar-item w3-button" href="/cert/cert_xml.asp" target="_top">XML Certificate</a>
        </div>
      </div>
      <br>
    </div>
  </div>
</div>

<div class='w3-sidebar w3-collapse' id='sidenav'>
  <div id='leftmenuinner'>
    <div class='w3-light-grey' id='leftmenuinnerinner'>
<!--  <a href='javascript:void(0)' onclick='close_menu()' class='w3-button w3-hide-large w3-large w3-display-topright' style='right:16px;padding:3px 12px;font-weight:bold;'>&times;</a>-->
<div class="notranslate">
<h2 class="left" style="white-space: nowrap;"><span class="left_h2">JS</span> Reference</h2>
<a target="_top" href="default.asp">JS by Category</a>
<a target="_top" href="jsref_reference.asp">JS by Alphabet</a>
<br>
<h2 class="left"><span class="left_h2">JavaScript</span></h2>
<a target="_top" href="jsref_obj_array.asp">JS Array</a>
<div class="ref_overview" style="margin-left:10px;background-color:#ddd">
  <a target="_top" href="jsref_concat_array.asp">concat()</a>
  <a target="_top" href="jsref_constructor_array.asp">constructor</a>
  <a target="_top" href="jsref_copywithin.asp">copyWithin()</a>
  <a target="_top" href="jsref_entries.asp">entries()</a>
  <a target="_top" href="jsref_every.asp">every()</a>
  <a target="_top" href="jsref_fill.asp">fill()</a>
  <a target="_top" href="jsref_filter.asp">filter()</a>
  <a target="_top" href="jsref_find.asp">find()</a>
  <a target="_top" href="jsref_findindex.asp">findIndex()</a>
  <a target="_top" href="jsref_foreach.asp">forEach()</a>
  <a target="_top" href="jsref_from.asp">from()</a>
  <a target="_top" href="jsref_includes_array.asp">includes()</a>
  <a target="_top" href="jsref_indexof_array.asp">indexOf()</a>
  <a target="_top" href="jsref_isarray.asp">isArray()</a>
  <a target="_top" href="jsref_join.asp">join()</a>
  <a target="_top" href="jsref_keys.asp">keys()</a>
  <a target="_top" href="jsref_length_array.asp">length</a>
  <a target="_top" href="jsref_lastindexof_array.asp">lastIndexOf()</a>
  <a target="_top" href="jsref_map.asp">map()</a>
  <a target="_top" href="jsref_pop.asp">pop()</a>
  <a target="_top" href="jsref_prototype_array.asp">prototype</a>
  <a target="_top" href="jsref_push.asp">push()</a>
  <a target="_top" href="jsref_reduce.asp">reduce()</a>
  <a target="_top" href="jsref_reduceright.asp">reduceRight()</a>
  <a target="_top" href="jsref_reverse.asp">reverse()</a>
  <a target="_top" href="jsref_shift.asp">shift()</a>
  <a target="_top" href="jsref_slice_array.asp">slice()</a>
  <a target="_top" href="jsref_some.asp">some()</a>
  <a target="_top" href="jsref_sort.asp">sort()</a>
  <a target="_top" href="jsref_splice.asp">splice()</a>
  <a target="_top" href="jsref_tostring_array.asp">toString()</a>
  <a target="_top" href="jsref_unshift.asp">unshift()</a>
  <a target="_top" href="jsref_valueof_array.asp">valueOf()</a>
</div>

<a target="_top" href="jsref_obj_boolean.asp">JS Boolean</a>
<div class="ref_overview" style="margin-left:10px;background-color:#ddd">
  <a target="_top" href="jsref_constructor_boolean.asp">constructor</a>
  <a target="_top" href="jsref_prototype_boolean.asp">prototype</a>
  <a target="_top" href="jsref_tostring_boolean.asp">toString()</a>
  <a target="_top" href="jsref_valueof_boolean.asp">valueOf()</a>
</div>

<a target="_top" href="jsref_obj_date.asp">JS Date</a>
<div class="ref_overview" style="margin-left:10px;background-color:#ddd">
  <a target="_top" href="jsref_constructor_date.asp">constructor</a>
  <a target="_top" href="jsref_getdate.asp">getDate()</a>
  <a target="_top" href="jsref_getday.asp">getDay()</a>
  <a target="_top" href="jsref_getfullyear.asp">getFullYear()</a>
  <a target="_top" href="jsref_gethours.asp">getHours()</a>
  <a target="_top" href="jsref_getmilliseconds.asp">getMilliseconds()</a>
  <a target="_top" href="jsref_getminutes.asp">getMinutes()</a>
  <a target="_top" href="jsref_getmonth.asp">getMonth()</a>
  <a target="_top" href="jsref_getseconds.asp">getSeconds()</a>
  <a target="_top" href="jsref_gettime.asp">getTime()</a>
  <a target="_top" href="jsref_gettimezoneoffset.asp">getTimezoneOffset()</a>
  <a target="_top" href="jsref_getutcdate.asp">getUTCDate()</a>
  <a target="_top" href="jsref_getutcday.asp">getUTCDay()</a>
  <a target="_top" href="jsref_getutcfullyear.asp">getUTCFullYear()</a>
  <a target="_top" href="jsref_getutchours.asp">getUTCHours()</a>
  <a target="_top" href="jsref_getutcmilliseconds.asp">getUTCMilliseconds()</a>
  <a target="_top" href="jsref_getutcminutes.asp">getUTCMinutes()</a>
  <a target="_top" href="jsref_getutcmonth.asp">getUTCMonth()</a>
  <a target="_top" href="jsref_getutcseconds.asp">getUTCSeconds()</a>
  <a target="_top" href="jsref_now.asp">now()</a>
  <a target="_top" href="jsref_parse.asp">parse()</a>
  <a target="_top" href="jsref_prototype_date.asp">prototype</a>
  <a target="_top" href="jsref_setdate.asp">setDate()</a>
  <a target="_top" href="jsref_setfullyear.asp">setFullYear()</a>
  <a target="_top" href="jsref_sethours.asp">setHours()</a>
  <a target="_top" href="jsref_setmilliseconds.asp">setMilliseconds()</a>
  <a target="_top" href="jsref_setminutes.asp">setMinutes()</a>
  <a target="_top" href="jsref_setmonth.asp">setMonth()</a>
  <a target="_top" href="jsref_setseconds.asp">setSeconds()</a>
  <a target="_top" href="jsref_settime.asp">setTime()</a>
  <a target="_top" href="jsref_setutcdate.asp">setUTCDate()</a>
  <a target="_top" href="jsref_setutcfullyear.asp">setUTCFullYear()</a>
  <a target="_top" href="jsref_setutchours.asp">setUTCHours()</a>
  <a target="_top" href="jsref_setutcmilliseconds.asp">setUTCMilliseconds()</a>
  <a target="_top" href="jsref_setutcminutes.asp">setUTCMinutes()</a>
  <a target="_top" href="jsref_setutcmonth.asp">setUTCMonth()</a>
  <a target="_top" href="jsref_setutcseconds.asp">setUTCSeconds()</a>
  <a target="_top" href="jsref_todatestring.asp">toDateString()</a>
  <a target="_top" href="jsref_toisostring.asp">toISOString()</a>
  <a target="_top" href="jsref_tojson.asp">toJSON()</a>
  <a target="_top" href="jsref_tolocaledatestring.asp">toLocaleDateString()</a>
  <a target="_top" href="jsref_tolocaletimestring.asp">toLocaleTimeString()</a>
  <a target="_top" href="jsref_tolocalestring.asp">toLocaleString()</a>
  <a target="_top" href="jsref_tostring_date.asp">toString()</a>
  <a target="_top" href="jsref_totimestring.asp">toTimeString()</a>
  <a target="_top" href="jsref_toutcstring.asp">toUTCString()</a>
  <a target="_top" href="jsref_utc.asp">UTC()</a>
  <a target="_top" href="jsref_valueof_date.asp">valueOf()</a>
</div>

<a target="_top" href="jsref_obj_error.asp">JS Error</a>
<div class="ref_overview" style="margin-left:10px;background-color:#ddd">
  <a target="_top" href="prop_error_name.asp">name</a>
  <a target="_top" href="prop_error_message.asp">message</a>
</div>

<a target="_top" href="jsref_obj_global.asp">JS Global</a>
<div class="ref_overview" style="margin-left:10px;background-color:#ddd">
  <a target="_top" href="jsref_decodeuri.asp">decodeURI()</a>
  <a target="_top" href="jsref_decodeuricomponent.asp">decodeURIComponent()</a>
  <a target="_top" href="jsref_encodeuri.asp">encodeURI()</a>
  <a target="_top" href="jsref_encodeuricomponent.asp">encodeURIComponent()</a>
  <a target="_top" href="jsref_escape.asp">escape()</a>
  <a target="_top" href="jsref_eval.asp">eval()</a>
  <a target="_top" href="jsref_infinity.asp">Infinity</a>
  <a target="_top" href="jsref_isfinite.asp">isFinite()</a>
  <a target="_top" href="jsref_isnan.asp">isNaN()</a>
  <a target="_top" href="jsref_nan.asp">NaN</a>
  <a target="_top" href="jsref_number.asp">Number()</a>
  <a target="_top" href="jsref_parsefloat.asp">parseFloat()</a>
  <a target="_top" href="jsref_parseint.asp">parseInt()</a>
  <a target="_top" href="jsref_string.asp">String()</a>
  <a target="_top" href="jsref_undefined.asp">undefined</a>
  <a target="_top" href="jsref_unescape.asp">unescape()</a>
</div>

<a target="_top" href="jsref_obj_json.asp">JS JSON</a>
<div class="ref_overview" style="margin-left:10px;background-color:#ddd">
  <a target="_top" href="jsref_parse_json.asp">parse()</a>
  <a target="_top" href="jsref_stringify.asp">stringify()</a>
</div>

<a target="_top" href="jsref_obj_math.asp">JS Math</a>
<div class="ref_overview" style="margin-left:10px;background-color:#ddd">
  <a target="_top" href="jsref_abs.asp">abs()</a>
  <a target="_top" href="jsref_acos.asp">acos()</a>
  <a target="_top" href="jsref_acosh.asp">acosh()</a>
  <a target="_top" href="jsref_asin.asp">asin()</a>
  <a target="_top" href="jsref_asinh.asp">asinh()</a>
  <a target="_top" href="jsref_atan.asp">atan()</a>
  <a target="_top" href="jsref_atan2.asp">atan2()</a>
  <a target="_top" href="jsref_atanh.asp">atanh()</a>
  <a target="_top" href="jsref_cbrt.asp">cbrt()</a>
  <a target="_top" href="jsref_ceil.asp">ceil()</a>
  <a target="_top" href="jsref_cos.asp">cos()</a>
  <a target="_top" href="jsref_cosh.asp">cosh()</a>
  <a target="_top" href="jsref_e.asp">E</a>
  <a target="_top" href="jsref_exp.asp">exp()</a>
  <a target="_top" href="jsref_floor.asp">floor()</a>
  <a target="_top" href="jsref_ln2.asp">LN2</a>
  <a target="_top" href="jsref_ln10.asp">LN10</a>
  <a target="_top" href="jsref_log.asp">log()</a>
  <a target="_top" href="jsref_log2e.asp">LOG2E</a>
  <a target="_top" href="jsref_log10e.asp">LOG10E</a>
  <a target="_top" href="jsref_max.asp">max()</a>
  <a target="_top" href="jsref_min.asp">min()</a>
  <a target="_top" href="jsref_pi.asp">PI</a>
  <a target="_top" href="jsref_pow.asp">pow()</a>
  <a target="_top" href="jsref_random.asp">random()</a>
  <a target="_top" href="jsref_round.asp">round()</a>
  <a target="_top" href="jsref_sin.asp">sin()</a>
  <a target="_top" href="jsref_sqrt.asp">sqrt()</a>
  <a target="_top" href="jsref_sqrt1_2.asp">SQRT1_2</a>
  <a target="_top" href="jsref_sqrt2.asp">SQRT2</a>
  <a target="_top" href="jsref_tan.asp">tan()</a>
  <a target="_top" href="jsref_tanh.asp">tanh()</a>
  <a target="_top" href="jsref_trunc.asp">trunc()</a>
</div>

<a target="_top" href="jsref_obj_number.asp">JS Number</a>
<div class="ref_overview" style="margin-left:10px;background-color:#ddd">
  <a target="_top" href="jsref_constructor_number.asp">constructor</a>
  <a target="_top" href="jsref_isfinite_number.asp">isFinite()</a>
  <a target="_top" href="jsref_isinteger.asp">isInteger()</a>
  <a target="_top" href="jsref_isnan_number.asp">isNaN()</a>
  <a target="_top" href="jsref_issafeinteger.asp">isSafeInteger()</a>
  <a target="_top" href="jsref_max_value.asp">MAX_VALUE</a>
  <a target="_top" href="jsref_min_value.asp">MIN_VALUE</a>
  <a target="_top" href="jsref_negative_infinity.asp">NEGATIVE_INFINITY</a>
  <a target="_top" href="jsref_number_nan.asp">NaN</a>
  <a target="_top" href="jsref_positive_infinity.asp">POSITIVE_INFINITY</a>
  <a target="_top" href="jsref_prototype_num.asp">prototype</a>
  <a target="_top" href="jsref_toexponential.asp">toExponential()</a>
  <a target="_top" href="jsref_tofixed.asp">toFixed()</a>
  <a target="_top" href="jsref_tolocalestring_number.asp">toLocaleString()</a>
  <a target="_top" href="jsref_toprecision.asp">toPrecision()</a>
  <a target="_top" href="jsref_tostring_number.asp">toString()</a>
  <a target="_top" href="jsref_valueof_number.asp">valueOf()</a>
</div>

<a target="_top" href="jsref_operators.asp">JS Operators</a>
<a target="_top" href="jsref_obj_regexp.asp">JS RegExp</a>
<div class="ref_overview" style="margin-left:10px;background-color:#ddd">
  <a target="_top" href="jsref_regexp_constructor.asp">constructor</a>
  <a target="_top" href="jsref_regexp_compile.asp">compile()</a>
  <a target="_top" href="jsref_regexp_exec.asp">exec()</a>
  <a target="_top" href="jsref_regexp_g.asp">g</a>
  <a target="_top" href="jsref_regexp_global.asp">global</a>
  <a target="_top" href="jsref_regexp_i.asp">i</a>
  <a target="_top" href="jsref_regexp_ignorecase.asp">ignoreCase</a>
  <a target="_top" href="jsref_regexp_lastindex.asp">lastIndex</a>
  <a target="_top" href="jsref_regexp_m.asp">m</a>
  <a target="_top" href="jsref_regexp_multiline.asp">multiline</a>
  <a target="_top" href="jsref_regexp_onemore.asp">n+</a>
  <a target="_top" href="jsref_regexp_zeromore.asp">n*</a>
  <a target="_top" href="jsref_regexp_zeroone.asp">n?</a>
  <a target="_top" href="jsref_regexp_nx.asp">n{X}</a>
  <a target="_top" href="jsref_regexp_nxy.asp">n{X,Y}</a>
  <a target="_top" href="jsref_regexp_nxcomma.asp">n{X,}</a>
  <a target="_top" href="jsref_regexp_ndollar.asp">n$</a>
  <a target="_top" href="jsref_regexp_ncaret.asp">^n</a>
  <a target="_top" href="jsref_regexp_nfollow.asp">?=n</a>
  <a target="_top" href="jsref_regexp_nfollow_not.asp">?!n</a>
  <a target="_top" href="jsref_regexp_source.asp">source</a>
  <a target="_top" href="jsref_regexp_test.asp">test()</a>
  <a target="_top" href="jsref_regexp_tostring.asp">toString()</a>
  <a target="_top" href="jsref_regexp_charset.asp">[abc]</a>
  <a target="_top" href="jsref_regexp_charset_not.asp">[^abc]</a>
  <a target="_top" href="jsref_regexp_0-9.asp">[0-9]</a>
  <a target="_top" href="jsref_regexp_not_0-9.asp">[^0-9]</a>
  <a target="_top" href="jsref_regexp_xy.asp">(x|y)</a>
  <a target="_top" href="jsref_regexp_dot.asp">.</a>
  <a target="_top" href="jsref_regexp_wordchar.asp">\\w</a>
  <a target="_top" href="jsref_regexp_wordchar_non.asp">\\W</a>
  <a target="_top" href="jsref_regexp_digit.asp">\\d</a>
  <a target="_top" href="jsref_regexp_digit_non.asp">\\D</a>
  <a target="_top" href="jsref_regexp_whitespace.asp">\\s</a>
  <a target="_top" href="jsref_regexp_whitespace_non.asp">\\S</a>
  <a target="_top" href="jsref_regexp_begin.asp">\\b</a>
  <a target="_top" href="jsref_regexp_begin_not.asp">\\B</a>
  <a target="_top" href="jsref_regexp_nul.asp">\\0</a>
  <a target="_top" href="jsref_regexp_newline.asp">\\n</a>
  <a target="_top" href="jsref_regexp_formfeed.asp">\\f</a>
  <a target="_top" href="jsref_regexp_carriagereturn.asp">\\r</a>
  <a target="_top" href="jsref_regexp_tab.asp">\\t</a>
  <a target="_top" href="jsref_regexp_vtab.asp">\\v</a>
  <a target="_top" href="jsref_regexp_octal.asp">\\xxx</a>
  <a target="_top" href="jsref_regexp_hex.asp">\xdd</a>
  <a target="_top" href="jsref_regexp_unicode_hex.asp">\\uxxxx</a>
</div>

<a target="_top" href="jsref_statements.asp">JS Statements</a>
<div class="ref_overview" style="margin-left:10px;background-color:#ddd">
  <a target="_top" href="jsref_break.asp">break</a>
  <a target="_top" href="jsref_continue.asp">continue</a>
  <a target="_top" href="jsref_debugger.asp">debugger</a>
  <a target="_top" href="jsref_dowhile.asp">do...while</a>
  <a target="_top" href="jsref_for.asp">for</a>
  <a target="_top" href="jsref_forin.asp">for...in</a>
  <a target="_top" href="jsref_forof.asp">for...of</a>  
  <a target="_top" href="jsref_function.asp">function</a>
  <a target="_top" href="jsref_if.asp">if...else</a>
  <a target="_top" href="jsref_return.asp">return</a>
  <a target="_top" href="jsref_switch.asp">switch</a>
  <a target="_top" href="jsref_throw.asp">throw</a>
  <a target="_top" href="jsref_try_catch.asp">try...catch</a>
  <a target="_top" href="jsref_var.asp">var</a>
  <a target="_top" href="jsref_while.asp">while</a>
</div>

<a target="_top" href="jsref_obj_string.asp">JS String</a>
<div class="ref_overview" style="margin-left:10px;background-color:#ddd">
  <a target="_top" href="jsref_charat.asp">charAt()</a>
  <a target="_top" href="jsref_charcodeat.asp">charCodeAt()</a>
  <a target="_top" href="jsref_concat_string.asp">concat()</a>
  <a target="_top" href="jsref_constructor_string.asp">constructor</a>
  <a target="_top" href="jsref_endswith.asp">endsWith()</a>
  <a target="_top" href="jsref_fromcharcode.asp">fromCharCode()</a>
  <a target="_top" href="jsref_includes.asp">includes()</a>
  <a target="_top" href="jsref_indexof.asp">indexOf()</a>
  <a target="_top" href="jsref_lastindexof.asp">lastIndexOf()</a>
  <a target="_top" href="jsref_length_string.asp">length</a>
  <a target="_top" href="jsref_localecompare.asp">localeCompare()</a>
  <a target="_top" href="jsref_match.asp">match()</a>
  <a target="_top" href="jsref_prototype_string.asp">prototype</a>
  <a target="_top" href="jsref_repeat.asp">repeat()</a>
  <a target="_top" href="jsref_replace.asp">replace()</a>
  <a target="_top" href="jsref_search.asp">search()</a>
  <a target="_top" href="jsref_slice_string.asp">slice()</a>
  <a target="_top" href="jsref_split.asp">split()</a>
  <a target="_top" href="jsref_startswith.asp">startsWith()</a>
  <a target="_top" href="jsref_substr.asp">substr()</a>
  <a target="_top" href="jsref_substring.asp">substring()</a>
  <a target="_top" href="jsref_tolocalelowercase.asp">toLocaleLowerCase()</a>
  <a target="_top" href="jsref_tolocaleuppercase.asp">toLocaleUpperCase()</a>
  <a target="_top" href="jsref_tolowercase.asp">toLowerCase()</a>
  <a target="_top" href="jsref_tostring_string.asp">toString()</a>
  <a target="_top" href="jsref_touppercase.asp">toUpperCase()</a>
  <a target="_top" href="jsref_trim_string.asp">trim()</a>
  <a target="_top" href="jsref_valueof_string.asp">valueOf()</a>
</div>

<br>
<h2 class="left"><span class="left_h2">HTML DOM</span></h2>
<a target="_top" href="dom_obj_attributes.asp">DOM Attribute</a>
<div class="ref_overview" style="margin-left:10px;background-color:#ddd">
  <a target="_top" href="met_namednodemap_getnameditem.asp">getNamedItem()</a>
  <a target="_top" href="prop_attr_isid.asp">isId</a>
  <a target="_top" href="met_namednodemap_item.asp">item()</a>
  <a target="_top" href="prop_namednodemap_length.asp">length</a>
  <a target="_top" href="prop_attr_name.asp">name</a>  
  <a target="_top" href="met_namednodemap_removenameditem.asp">removeNamedItem()</a>
  <a target="_top" href="met_namednodemap_setnameditem.asp">setNamedItem()</a>
  <a target="_top" href="prop_attr_specified.asp">specified</a>
  <a target="_top" href="prop_attr_value.asp">value</a>
</div>

<a target="_top" href="obj_console.asp">DOM Console</a>
<div class="ref_overview" style="margin-left:10px;background-color:#ddd">
  <a target="_top"  href="met_console_assert.asp">assert()</a>
  <a target="_top"  href="met_console_clear.asp">clear()</a>
  <a target="_top"  href="met_console_count.asp">count()</a>
  <a target="_top"  href="met_console_error.asp">error()</a>
  <a target="_top"  href="met_console_group.asp">group()</a>
  <a target="_top"  href="met_console_groupcollapsed.asp">groupCollapsed()</a>
  <a target="_top"  href="met_console_groupend.asp">groupEnd()</a>
  <a target="_top"  href="met_console_info.asp">info()</a>
  <a target="_top"  href="met_console_log.asp">log()</a>
  <a target="_top"  href="met_console_table.asp">table()</a>
  <a target="_top"  href="met_console_time.asp">time()</a>
  <a target="_top"  href="met_console_timeend.asp">timeEnd()</a>
  <a target="_top"  href="met_console_trace.asp">trace()</a>
  <a target="_top"  href="met_console_warn.asp">warn()</a>
</div>

<a target="_top" href="dom_obj_document.asp">DOM Document</a>
<div class="ref_overview" style="margin-left:10px;background-color:#ddd">
  <a target="_top"  href="prop_document_activeelement.asp">activeElement</a>
  <a target="_top"  href="met_document_addeventlistener.asp">addEventListener()</a>
  <a target="_top"  href="met_document_adoptnode.asp">adoptNode()</a>
  <a target="_top"  href="coll_doc_anchors.asp">anchors</a>
  <a target="_top"  href="coll_doc_applets.asp">applets</a>
  <a target="_top"  href="prop_doc_baseuri.asp">baseURI</a>
  <a target="_top"  href="prop_doc_body.asp">body</a>
  <a target="_top"  href="met_doc_close.asp">close()</a>
  <a target="_top"  href="prop_doc_cookie.asp">cookie</a>
  <a target="_top"  href="prop_document_characterset.asp">charset</a>
  <a target="_top"  href="prop_document_characterset.asp">characterSet</a>
  <a target="_top"  href="met_document_createattribute.asp">createAttribute()</a>
  <a target="_top"  href="met_document_createcomment.asp">createComment()</a>
  <a target="_top"  href="met_document_createdocumentfragment.asp">createDocumentFragment()</a>
  <a target="_top"  href="met_document_createelement.asp">createElement()</a>
  <a target="_top"  href="event_createevent.asp">createEvent()</a>
  <a target="_top"  href="met_document_createtextnode.asp">createTextNode()</a>
  <a target="_top"  href="prop_document_defaultview.asp">defaultView</a>
  <a target="_top"  href="prop_document_designmode.asp">designMode</a>
  <a target="_top"  href="prop_document_doctype.asp">doctype</a>
  <a target="_top"  href="prop_document_documentelement.asp">documentElement</a>
  <a target="_top"  href="prop_doc_documentmode.asp">documentMode</a>
  <a target="_top"  href="prop_document_documenturi.asp">documentURI</a>
  <a target="_top"  href="prop_doc_domain.asp">domain</a>
  <a target="_top"  href="coll_doc_embeds.asp">embeds</a>
  <a target="_top"  href="met_document_execcommand.asp">execCommand()</a>
  <a target="_top"  href="coll_doc_forms.asp">forms</a>
  <a target="_top"  href="prop_document_fullscreenelement.asp">fullscreenElement</a>
  <a target="_top"  href="met_document_fullscreenenabled.asp">fullscreenEnabled()</a>
  <a target="_top"  href="met_document_getelementbyid.asp">getElementById()</a>
  <a target="_top"  href="met_document_getelementsbyclassname.asp">getElementsByClassName()</a>
  <a target="_top"  href="met_doc_getelementsbyname.asp">getElementsByName()</a>
  <a target="_top"  href="met_document_getelementsbytagname.asp">getElementsByTagName()</a>
  <a target="_top"  href="met_document_hasfocus.asp">hasFocus()</a>
  <a target="_top"  href="prop_doc_head.asp">head</a>
  <a target="_top"  href="coll_doc_images.asp">images</a>
  <a target="_top"  href="prop_document_implementation.asp">implementation</a>
  <a target="_top"  href="met_document_importnode.asp">importNode()</a>
  <a target="_top"  href="prop_document_inputencoding.asp">inputEncoding</a>
  <a target="_top"  href="prop_doc_lastmodified.asp">lastModified</a>
  <a target="_top"  href="coll_doc_links.asp">links</a>
  <a target="_top"  href="met_document_normalize.asp">normalize()</a>
  <a target="_top"  href="met_document_normalizedocument.asp">normalizeDocument()</a>
  <a target="_top"  href="met_doc_open.asp">open()</a>
  <a target="_top"  href="met_document_queryselector.asp">querySelector()</a>
  <a target="_top"  href="met_document_queryselectorall.asp">querySelectorAll()</a>
  <a target="_top"  href="prop_doc_readystate.asp">readyState</a>
  <a target="_top"  href="prop_doc_referrer.asp">referrer</a>
  <a target="_top"  href="met_document_removeeventlistener.asp">removeEventListener()</a>
  <a target="_top"  href="met_document_renamenode.asp">renameNode()</a>
  <a target="_top"  href="coll_doc_scripts.asp">scripts</a>
  <a target="_top"  href="prop_document_stricterrorchecking.asp">strictErrorChecking</a>
  <a target="_top"  href="prop_doc_title.asp">title</a>
  <a target="_top"  href="prop_doc_url.asp">URL</a>
  <a target="_top"  href="met_doc_write.asp">write()</a>
  <a target="_top"  href="met_doc_writeln.asp">writeln()</a>
</div>

<a target="_top" href="dom_obj_all.asp">DOM Element</a>
<div class="ref_overview" style="margin-left:10px;background-color:#ddd">
  <a target="_top"  href="prop_html_accesskey.asp">accessKey</a>
  <a target="_top" href="met_element_addeventlistener.asp">addEventListener()</a>
  <a target="_top" href="met_node_appendchild.asp">appendChild()</a>
  <a target="_top" href="prop_node_attributes.asp">attributes</a>
  <a target="_top" href="met_html_blur.asp">blur()</a>
  <a target="_top" href="prop_element_childelementcount.asp">childElementCount</a>
  <a target="_top" href="prop_node_childnodes.asp">childNodes</a>
  <a target="_top" href="prop_element_children.asp">children</a>
  <a target="_top" href="prop_element_classlist.asp">classList</a>
  <a target="_top" href="prop_html_classname.asp">className</a>
  <a target="_top" href="met_html_click.asp">click()</a>
  <a target="_top" href="prop_element_clientheight.asp">clientHeight</a>
  <a target="_top" href="prop_element_clientleft.asp">clientLeft</a>
  <a target="_top" href="prop_element_clienttop.asp">clientTop</a>
  <a target="_top" href="prop_element_clientwidth.asp">clientWidth</a>
  <a target="_top" href="met_node_clonenode.asp">cloneNode()</a>
  <a target="_top" href="met_node_comparedocumentposition.asp">compareDocumentPosition()</a>
  <a target="_top" href="met_node_contains.asp">contains()</a>
  <a target="_top" href="prop_html_contenteditable.asp">contentEditable</a>
  <a target="_top" href="prop_html_dir.asp">dir</a>
  <a target="_top" href="met_element_exitfullscreen.asp">exitFullscreen()</a>
  <a target="_top" href="prop_node_firstchild.asp">firstChild</a>
  <a target="_top" href="prop_element_firstelementchild.asp">firstElementChild</a>
  <a target="_top" href="met_html_focus.asp">focus()</a>
  <a target="_top" href="met_element_getattribute.asp">getAttribute()</a>
  <a target="_top" href="met_element_getattributenode.asp">getAttributeNode()</a>
  <a target="_top" href="met_element_getboundingclientrect.asp">getBoundingClientRect()</a>
  <a target="_top" href="met_element_getelementsbyclassname.asp">getElementsByClassName()</a>
  <a target="_top" href="met_element_getelementsbytagname.asp">getElementsByTagName()</a>
  <a target="_top" href="met_element_hasattribute.asp">hasAttribute()</a>
  <a target="_top" href="met_node_hasattributes.asp">hasAttributes()</a>
  <a target="_top" href="met_node_haschildnodes.asp">hasChildNodes()</a>
  <a target="_top" href="prop_html_id.asp">id</a>
  <a target="_top" href="prop_html_innerhtml.asp">innerHTML</a>
  <a target="_top" href="prop_node_innertext.asp">innerText</a>
  <a target="_top" href="met_node_insertadjacentelement.asp">insertAdjacentElement()</a>
  <a target="_top" href="met_node_insertadjacenthtml.asp">insertAdjacentHTML()</a>
  <a target="_top" href="met_node_insertadjacenttext.asp">insertAdjacentText()</a>
  <a target="_top" href="met_node_insertbefore.asp">insertBefore()</a>
  <a target="_top" href="prop_html_iscontenteditable.asp">isContentEditable</a>
  <a target="_top" href="met_node_isdefaultnamespace.asp">isDefaultNamespace()</a>
  <a target="_top" href="met_node_isequalnode.asp">isEqualNode()</a>
  <a target="_top" href="met_node_issamenode.asp">isSameNode()</a>
  <a target="_top" href="met_node_issupported.asp">isSupported()</a>
  <a target="_top" href="prop_html_lang.asp">lang</a>
  <a target="_top" href="prop_node_lastchild.asp">lastChild</a>
  <a target="_top" href="prop_element_lastelementchild.asp">lastElementChild</a>
  <a target="_top" href="prop_node_namespaceuri.asp">namespaceURI</a>
  <a target="_top" href="prop_node_nextsibling.asp">nextSibling</a>
  <a target="_top" href="prop_element_nextelementsibling.asp">nextElementSibling</a>
  <a target="_top" href="prop_node_nodename.asp">nodeName</a>
  <a target="_top" href="prop_node_nodetype.asp">nodeType</a>
  <a target="_top" href="prop_node_nodevalue.asp">nodeValue</a>
  <a target="_top" href="met_node_normalize.asp">normalize()</a>
  <a target="_top" href="prop_element_offsetheight.asp">offsetHeight</a>
  <a target="_top" href="prop_element_offsetwidth.asp">offsetWidth</a>
  <a target="_top" href="prop_element_offsetleft.asp">offsetLeft</a>
  <a target="_top" href="prop_element_offsetparent.asp">offsetParent</a>
  <a target="_top" href="prop_element_offsettop.asp">offsetTop</a>
  <a target="_top" href="prop_html_outerhtml.asp">outerHTML</a>
  <a target="_top" href="prop_node_outertext.asp">outerText</a>
  <a target="_top" href="prop_node_ownerdocument.asp">ownerDocument</a>
  <a target="_top" href="prop_node_parentnode.asp">parentNode</a>
  <a target="_top" href="prop_node_parentelement.asp">parentElement</a>
  <a target="_top" href="prop_node_previoussibling.asp">previousSibling</a>
  <a target="_top" href="prop_element_previouselementsibling.asp">previousElementSibling</a>
  <a target="_top" href="met_element_queryselector.asp">querySelector()</a>
  <a target="_top" href="met_element_queryselectorall.asp">querySelectorAll()</a>
  <a target="_top" href="met_element_remove.asp">remove()</a>
  <a target="_top" href="met_element_removeattribute.asp">removeAttribute()</a>
  <a target="_top" href="met_element_removeattributenode.asp">removeAttributeNode()</a>
  <a target="_top" href="met_node_removechild.asp">removeChild()</a>
  <a target="_top" href="met_element_removeeventlistener.asp">removeEventListener()</a>
  <a target="_top" href="met_node_replacechild.asp">replaceChild()</a>
  <a target="_top" href="met_element_requestfullscreen.asp">requestFullscreen()</a>
  <a target="_top" href="prop_element_scrollheight.asp">scrollHeight</a>
  <a target="_top" href="met_element_scrollintoview.asp">scrollIntoView()</a>
  <a target="_top" href="prop_element_scrollleft.asp">scrollLeft</a>
  <a target="_top" href="prop_element_scrolltop.asp">scrollTop</a>
  <a target="_top" href="prop_element_scrollwidth.asp">scrollWidth</a>
  <a target="_top" href="met_element_setattribute.asp">setAttribute()</a>
  <a target="_top" href="met_element_setattributenode.asp">setAttributeNode()</a>
  <a target="_top" href="prop_html_style.asp">style</a>
  <a target="_top" href="prop_html_tabindex.asp">tabIndex</a>
  <a target="_top" href="prop_element_tagname.asp">tagName</a>
  <a target="_top" href="prop_node_textcontent.asp">textContent</a>
  <a target="_top" href="prop_html_title.asp">title</a>
</div>

<a target="_top" href="dom_obj_event.asp">DOM Events</a>
<div class="ref_overview" style="margin-left:10px;background-color:#ddd">
  <a target="_top" href="event_onabort_media.asp">abort</a>
  <a target="_top" href="event_onafterprint.asp">afterprint</a>
  <a target="_top" href="event_animationend.asp">animationend</a>
  <a target="_top" href="event_animationiteration.asp">animationiteration</a>
  <a target="_top" href="event_animationstart.asp">animationstart</a>
  <a target="_top" href="event_onbeforeprint.asp">beforeprint</a>
  <a target="_top" href="event_onbeforeunload.asp">beforeunload</a>
  <a target="_top" href="event_onblur.asp">blur</a>
  <a target="_top" href="event_oncanplay.asp">canplay</a>
  <a target="_top" href="event_oncanplaythrough.asp">canplaythrough</a>
  <a target="_top" href="event_onchange.asp">change</a>
  <a target="_top" href="event_onclick.asp">click</a>
  <a target="_top" href="event_oncontextmenu.asp">contextmenu</a>
  <a target="_top" href="event_oncopy.asp">copy</a>
  <a target="_top" href="event_oncut.asp">cut</a>
  <a target="_top" href="event_ondblclick.asp">dblclick</a>
  <a target="_top" href="event_ondrag.asp">drag</a>
  <a target="_top" href="event_ondragend.asp">dragend</a>
  <a target="_top" href="event_ondragenter.asp">dragenter</a>
  <a target="_top" href="event_ondragleave.asp">dragleave</a>
  <a target="_top" href="event_ondragover.asp">dragover</a>
  <a target="_top" href="event_ondragstart.asp">dragstart</a>
  <a target="_top" href="event_ondrop.asp">drop</a>
  <a target="_top" href="event_ondurationchange.asp">durationchange</a>
  <a target="_top" href="event_onended.asp">ended</a>
  <a target="_top" href="event_onerror.asp">error</a>
  <a target="_top" href="event_onfocus.asp">focus</a>
  <a target="_top" href="event_onfocusin.asp">focusin</a>
  <a target="_top" href="event_onfocusout.asp">focusout</a>
  <a target="_top" href="event_fullscreenchange.asp">fullscreenchange</a>
  <a target="_top" href="event_fullscreenerror.asp">fullscreenerror</a>
  <a target="_top" href="event_onhashchange.asp">hashchange</a>
  <a target="_top" href="event_oninput.asp">input</a>
  <a target="_top" href="event_oninvalid.asp">invalid</a>
  <a target="_top" href="event_onkeydown.asp">keydown</a>
  <a target="_top" href="event_onkeypress.asp">keypress</a>
  <a target="_top" href="event_onkeyup.asp">keyup</a>
  <a target="_top" href="event_onload.asp">load</a>
  <a target="_top" href="event_onloadeddata.asp">loadeddata</a>
  <a target="_top" href="event_onloadedmetadata.asp">loadedmetadata</a>
  <a target="_top" href="event_onloadstart.asp">loadstart</a>
  <a target="_top" href="event_onmessage_sse.asp">message</a>
  <a target="_top" href="event_onmousedown.asp">mousedown</a>
  <a target="_top" href="event_onmouseenter.asp">mouseenter</a>
  <a target="_top" href="event_onmouseleave.asp">mouseleave</a>
  <a target="_top" href="event_onmousemove.asp">mousemove</a>
  <a target="_top" href="event_onmouseover.asp">mouseover</a>
  <a target="_top" href="event_onmouseout.asp">mouseout</a>
  <a target="_top" href="event_onmouseup.asp">mouseup</a>
  <a target="_top" href="event_onoffline.asp">offline</a>
  <a target="_top" href="event_ononline.asp">online</a>
  <a target="_top" href="event_onopen_sse.asp">open</a>
  <a target="_top" href="event_onpagehide.asp">pagehide</a>
  <a target="_top" href="event_onpageshow.asp">pageshow</a>
  <a target="_top" href="event_onpaste.asp">paste</a>
  <a target="_top" href="event_onpause.asp">pause</a>
  <a target="_top" href="event_onplay.asp">play</a>
  <a target="_top" href="event_onplaying.asp">playing</a>
  <a target="_top" href="event_onprogress.asp">progress</a>
  <a target="_top" href="event_onratechange.asp">ratechange</a>
  <a target="_top" href="event_onresize.asp">resize</a>
  <a target="_top" href="event_onreset.asp">reset</a>
  <a target="_top" href="event_onscroll.asp">scroll</a>
  <a target="_top" href="event_onsearch.asp">search</a>
  <a target="_top" href="event_onseeked.asp">seeked</a>
  <a target="_top" href="event_onseeking.asp">seeking</a>
  <a target="_top" href="event_onselect.asp">select</a>
  <a target="_top" href="event_onshow.asp">show</a>
  <a target="_top" href="event_onstalled.asp">stalled</a>
  <a target="_top" href="event_onsubmit.asp">submit</a>
  <a target="_top" href="event_onsuspend.asp">suspend</a>
  <a target="_top" href="event_ontimeupdate.asp">timeupdate</a>
  <a target="_top" href="event_ontoggle.asp">toggle</a>
  <a target="_top" href="event_touchcancel.asp">touchcancel</a>
  <a target="_top" href="event_touchend.asp">touchend</a>
  <a target="_top" href="event_touchmove.asp">touchmove</a>
  <a target="_top" href="event_touchstart.asp">touchstart</a>
  <a target="_top" href="event_transitionend.asp">transitionend</a>
  <a target="_top" href="event_onunload.asp">unload</a>
  <a target="_top" href="event_onvolumechange.asp">volumechange</a>
  <a target="_top" href="event_onwaiting.asp">waiting</a>
  <a target="_top" href="event_onwheel.asp">wheel</a>
  <a target="_top" href="event_altkey.asp">altKey</a>
  <a target="_top" href="event_key_altkey.asp">altKey</a>
  <a target="_top" href="event_animation_animationName.asp">animationName</a>
  <a target="_top" href="event_bubbles.asp">bubbles</a>
  <a target="_top" href="event_button.asp">button</a>
  <a target="_top" href="event_buttons.asp">buttons</a>
  <a target="_top" href="event_cancelable.asp">cancelable</a>
  <a target="_top" href="event_key_charcode.asp">charCode</a>
  <a target="_top" href="event_clientx.asp">clientX</a>
  <a target="_top" href="event_clienty.asp">clientY</a>
  <a target="_top" href="event_key_code.asp">code</a>
  <a target="_top" href="event_createevent.asp">createEvent()</a>
  <a target="_top" href="event_ctrlkey.asp">ctrlKey</a>
  <a target="_top" href="event_key_ctrlkey.asp">ctrlKey</a>
  <a target="_top" href="event_currenttarget.asp">currentTarget</a>
  <a target="_top" href="event_inputevent_data.asp">data</a>
  <a target="_top" href="event_defaultprevented.asp">defaultPrevented</a>
  <a target="_top" href="event_wheel_deltax.asp">deltaX</a>
  <a target="_top" href="event_wheel_deltay.asp">deltaY</a>
  <a target="_top" href="event_wheel_deltaz.asp">deltaZ</a>
  <a target="_top" href="event_wheel_deltamode.asp">deltaMode</a>
  <a target="_top" href="event_detail.asp">detail</a>
  <a target="_top" href="event_animation_elapsedtime.asp">elapsedTime</a>
  <a target="_top" href="event_transition_elapsedtime.asp">elapsedTime</a>
  <a target="_top" href="event_eventphase.asp">eventPhase</a>
  <a target="_top" href="event_mouse_getmodifierstate.asp">getModifierState()</a>
  <a target="_top" href="event_inputevent_inputtype.asp">inputType</a>
  <a target="_top" href="event_istrusted.asp">isTrusted</a>
  <a target="_top" href="event_key_key.asp">key</a>
  <a target="_top" href="event_key_keycode.asp">keyCode</a>
  <a target="_top" href="event_key_location.asp">location</a>
  <a target="_top" href="event_metakey.asp">metaKey</a>
  <a target="_top" href="event_key_metakey.asp">metaKey</a>
  <a target="_top" href="event_hashchange_newurl.asp">newURL</a>
  <a target="_top" href="event_hashchange_oldurl.asp">oldURL</a>
  <a target="_top" href="event_pagex.asp">pageX</a>
  <a target="_top" href="event_pagey.asp">pageY</a>
  <a target="_top" href="event_pagetransition_persisted.asp">persisted</a>
  <a target="_top" href="event_preventdefault.asp">preventDefault()</a>
  <a target="_top" href="event_transition_propertyName.asp">propertyName</a>
  <a target="_top" href="event_relatedtarget.asp">relatedTarget</a>
  <a target="_top" href="event_focus_relatedtarget.asp">relatedTarget</a>
  <a target="_top" href="event_screenx.asp">screenX</a>
  <a target="_top" href="event_screeny.asp">screenY</a>
  <a target="_top" href="event_shiftkey.asp">shiftKey</a>
  <a target="_top" href="event_key_shiftkey.asp">shiftKey</a>
  <a target="_top" href="event_stopimmediatepropagation.asp">stopImmediatePropagation()</a>
  <a target="_top" href="event_stoppropagation.asp">stopPropagation()</a>
  <a target="_top" href="event_target.asp">target</a>
  <a target="_top" href="event_touch_targettouches.asp">targetTouches</a>
  <a target="_top" href="event_timestamp.asp">timeStamp</a>
  <a target="_top" href="event_touch_touches.asp">touches</a>
  <a target="_top" href="event_transitionend.asp">transitionend</a>
  <a target="_top" href="event_type.asp">type</a>
  <a target="_top" href="event_which.asp">which</a>
  <a target="_top" href="event_key_which.asp">which</a>
  <a target="_top" href="event_view.asp">view</a>
</div>

<a target="_top" href="obj_events.asp">DOM Event Objects</a>
<div class="ref_overview" style="margin-left:10px;background-color:#ddd">
  <a href="obj_animationevent.asp">AnimationEvent</a>
  <a href="obj_clipboardevent.asp">ClipboardEvent</a>
  <a href="obj_dragevent.asp">DragEvent</a>
  <a href="obj_event.asp">Event</a>
  <a href="obj_focusevent.asp">FocusEvent</a>
  <a href="obj_hashchangeevent.asp">HashChangeEvent</a>
  <a href="obj_inputevent.asp">InputEvent</a>
  <a href="obj_keyboardevent.asp">KeyboardEvent</a>
  <a href="obj_mouseevent.asp">MouseEvent</a>
  <a href="obj_pagetransitionevent.asp">PageTransitionEvent</a>
  <a href="obj_popstateevent.asp">PopStateEvent</a>
  <a href="obj_progressevent.asp">ProgressEvent</a>
  <a href="obj_storageevent.asp">StorageEvent</a>
  <a href="obj_touchevent.asp">TouchEvent</a>
  <a href="obj_transitionevent.asp">TransitionEvent</a>
  <a href="obj_uievent.asp">UiEvent</a>
  <a href="obj_wheelevent.asp">WheelEvent</a>
</div>

<a target="_top" href="obj_geolocation.asp">DOM Geolocation</a>
<div class="ref_overview" style="margin-left:10px;background-color:#ddd">
  <a target="_top" href="prop_geo_coordinates.asp">coordinates</a>
  <a target="_top" href="prop_geo_position.asp">position</a>
</div>

<a target="_top" href="obj_history.asp">DOM History</a>
<div class="ref_overview" style="margin-left:10px;background-color:#ddd">
  <a target="_top" href="met_his_back.asp">back()</a>
  <a target="_top" href="met_his_forward.asp">forward()</a>
  <a target="_top" href="met_his_go.asp">go()</a>
  <a target="_top" href="prop_his_length.asp">length</a>
</div>

<a target="_top" href="dom_obj_htmlcollection.asp">DOM HTMLCollection</a>
<div class="ref_overview" style="margin-left:10px;background-color:#ddd">
  <a target="_top" href="met_htmlcollection_item.asp">item()</a>
  <a target="_top" href="prop_htmlcollection_length.asp">length</a>
  <a target="_top" href="met_htmlcollection_nameditem.asp">namedItem()</a>
</div>

<a target="_top" href="obj_location.asp">DOM Location</a>
<div class="ref_overview" style="margin-left:10px;background-color:#ddd">
  <a target="_top" href="prop_loc_hash.asp">hash</a>
  <a target="_top" href="prop_loc_host.asp">host</a>
  <a target="_top" href="prop_loc_hostname.asp">hostname</a>
  <a target="_top" href="prop_loc_href.asp">href</a>
  <a target="_top" href="prop_loc_origin.asp">origin</a>
  <a target="_top" href="prop_loc_pathname.asp">pathname</a>
  <a target="_top" href="prop_loc_port.asp">port</a>
  <a target="_top" href="prop_loc_protocol.asp">protocol</a>
  <a target="_top" href="prop_loc_search.asp">search</a>
  <a target="_top" href="met_loc_assign.asp">assign()</a>
  <a target="_top" href="met_loc_reload.asp">reload()</a>
  <a target="_top" href="met_loc_replace.asp">replace()</a>
</div>

<a target="_top" href="obj_navigator.asp">DOM Navigator</a>
<div class="ref_overview" style="margin-left:10px;background-color:#ddd">
  <a target="_top" href="prop_nav_appcodename.asp">appCodeName</a>
  <a target="_top" href="prop_nav_appname.asp">appName</a>
  <a target="_top" href="prop_nav_appversion.asp">appVersion</a>
  <a target="_top" href="prop_nav_cookieenabled.asp">cookieEnabled</a>
  <a target="_top" href="prop_nav_geolocation.asp">geolocation</a>
  <a target="_top" href="prop_nav_language.asp">language</a>
  <a target="_top" href="prop_nav_online.asp">onLine</a>
  <a target="_top" href="prop_nav_platform.asp">platform</a>
  <a target="_top" href="prop_nav_product.asp">product</a>
  <a target="_top" href="prop_nav_useragent.asp">userAgent</a>
  <a target="_top" href="met_nav_javaenabled.asp">javaEnabled()</a>
  <a target="_top" href="met_nav_taintenabled.asp">taintEnabled()</a>
</div>

<a target="_top" href="obj_screen.asp">DOM Screen</a>
<div class="ref_overview" style="margin-left:10px;background-color:#ddd">
  <a target="_top" href="prop_screen_availheight.asp">availHeight</a>
  <a target="_top" href="prop_screen_availwidth.asp">availWidth</a>
  <a target="_top" href="prop_screen_colordepth.asp">colorDepth</a>
  <a target="_top" href="prop_screen_height.asp">height</a>
  <a target="_top" href="prop_screen_pixeldepth.asp">pixelDepth</a>
  <a target="_top" href="prop_screen_width.asp">width</a>
</div>

<a target="_top" href="dom_obj_style.asp">DOM Style</a>
<div class="ref_overview" style="margin-left:10px;background-color:#ddd">
  <a target="_top" href="prop_style_aligncontent.asp">alignContent</a>  
  <a target="_top" href="prop_style_alignitems.asp">alignItems</a>  
  <a target="_top" href="prop_style_alignself.asp">alignSelf</a>  
  <a target="_top" href="prop_style_animation.asp">animation</a>  
  <a target="_top" href="prop_style_animationdelay.asp">animationDelay</a>  
  <a target="_top" href="prop_style_animationdirection.asp">animationDirection</a>  
  <a target="_top" href="prop_style_animationduration.asp">animationDuration</a>  
  <a target="_top" href="prop_style_animationfillmode.asp">animationFillMode</a>  
  <a target="_top" href="prop_style_animationiterationcount.asp">animationIterationCount</a>  
  <a target="_top" href="prop_style_animationname.asp">animationName</a>  
  <a target="_top" href="prop_style_animationtimingfunction.asp">animationTimingFunction</a>  
  <a target="_top" href="prop_style_animationplaystate.asp">animationPlayState</a>  
  <a target="_top" href="prop_style_background.asp">background</a>  
  <a target="_top" href="prop_style_backgroundattachment.asp">backgroundAttachment</a>  
  <a target="_top" href="prop_style_backgroundcolor.asp">backgroundColor</a>  
  <a target="_top" href="prop_style_backgroundimage.asp">backgroundImage</a>  
  <a target="_top" href="prop_style_backgroundposition.asp">backgroundPosition</a>  
  <a target="_top" href="prop_style_backgroundrepeat.asp">backgroundRepeat</a>  
  <a target="_top" href="prop_style_backgroundclip.asp">backgroundClip</a>  
  <a target="_top" href="prop_style_backgroundorigin.asp">backgroundOrigin</a>  
  <a target="_top" href="prop_style_backgroundsize.asp">backgroundSize</a>  
  <a target="_top" href="prop_style_backfacevisibility.asp">backfaceVisibility</a>  
  <a target="_top" href="prop_style_border.asp">border</a>  
  <a target="_top" href="prop_style_borderbottom.asp">borderBottom</a>  
  <a target="_top" href="prop_style_borderbottomcolor.asp">borderBottomColor</a>  
  <a target="_top" href="prop_style_borderbottomleftradius.asp">borderBottomLeftRadius</a>  
  <a target="_top" href="prop_style_borderbottomrightradius.asp">borderBottomRightRadius</a>  
  <a target="_top" href="prop_style_borderbottomstyle.asp">borderBottomStyle</a>  
  <a target="_top" href="prop_style_borderbottomwidth.asp">borderBottomWidth</a>  
  <a target="_top" href="prop_style_bordercollapse.asp">borderCollapse</a>  
  <a target="_top" href="prop_style_bordercolor.asp">borderColor</a>  
  <a target="_top" href="prop_style_borderimage.asp">borderImage</a>  
  <a target="_top" href="prop_style_borderimageoutset.asp">borderImageOutset</a>  
  <a target="_top" href="prop_style_borderimagerepeat.asp">borderImageRepeat</a>  
  <a target="_top" href="prop_style_borderimageslice.asp">borderImageSlice</a>  
  <a target="_top" href="prop_style_borderimagesource.asp">borderImageSource</a>  
  <a target="_top" href="prop_style_borderimagewidth.asp">borderImageWidth</a>  
  <a target="_top" href="prop_style_borderleft.asp">borderLeft</a>  
  <a target="_top" href="prop_style_borderleftcolor.asp">borderLeftColor</a>  
  <a target="_top" href="prop_style_borderleftstyle.asp">borderLeftStyle</a>  
  <a target="_top" href="prop_style_borderleftwidth.asp">borderLeftWidth</a>  
  <a target="_top" href="prop_style_borderradius.asp">borderRadius</a>  
  <a target="_top" href="prop_style_borderright.asp">borderRight</a>  
  <a target="_top" href="prop_style_borderrightcolor.asp">borderRightColor</a>  
  <a target="_top" href="prop_style_borderrightstyle.asp">borderRightStyle</a>  
  <a target="_top" href="prop_style_borderrightwidth.asp">borderRightWidth</a>  
  <a target="_top" href="prop_style_borderspacing.asp">borderSpacing</a>  
  <a target="_top" href="prop_style_borderstyle.asp">borderStyle</a>  
  <a target="_top" href="prop_style_bordertop.asp">borderTop</a>  
  <a target="_top" href="prop_style_bordertopcolor.asp">borderTopColor</a>  
  <a target="_top" href="prop_style_bordertopleftradius.asp">borderTopLeftRadius</a>  
  <a target="_top" href="prop_style_bordertoprightradius.asp">borderTopRightRadius</a>  
  <a target="_top" href="prop_style_bordertopstyle.asp">borderTopStyle</a>  
  <a target="_top" href="prop_style_bordertopwidth.asp">borderTopWidth</a>  
  <a target="_top" href="prop_style_borderwidth.asp">borderWidth</a>  
  <a target="_top" href="prop_style_bottom.asp">bottom</a>  
  <a target="_top" href="prop_style_boxshadow.asp">boxShadow</a>  
  <a target="_top" href="prop_style_boxsizing.asp">boxSizing</a>  
  <a target="_top" href="prop_style_captionside.asp">captionSide</a>  
  <a target="_top" href="prop_style_clear.asp">clear</a>  
  <a target="_top" href="prop_style_clip.asp">clip</a>  
  <a target="_top" href="prop_style_color.asp">color</a>  
  <a target="_top" href="prop_style_columncount.asp">columnCount</a>  
  <a target="_top" href="prop_style_columnfill.asp">columnFill</a>  
  <a target="_top" href="prop_style_columngap.asp">columnGap</a>  
  <a target="_top" href="prop_style_columnrule.asp">columnRule</a>  
  <a target="_top" href="prop_style_columnrulecolor.asp">columnRuleColor</a>  
  <a target="_top" href="prop_style_columnrulestyle.asp">columnRuleStyle</a>  
  <a target="_top" href="prop_style_columnrulewidth.asp">columnRuleWidth</a>  
  <a target="_top" href="prop_style_columns.asp">columns</a>  
  <a target="_top" href="prop_style_columnspan.asp">columnSpan</a>  
  <a target="_top" href="prop_style_columnwidth.asp">columnWidth</a>  
  <a target="_top" href="prop_style_counterincrement.asp">counterIncrement</a>  
  <a target="_top" href="prop_style_counterreset.asp">counterReset</a>  
  <a target="_top" href="prop_style_cursor.asp">cursor</a>  
  <a target="_top" href="prop_style_direction.asp">direction</a>  
  <a target="_top" href="prop_style_display.asp">display</a>  
  <a target="_top" href="prop_style_emptycells.asp">emptyCells</a>  
  <a target="_top" href="prop_style_filter.asp">filter</a>  
  <a target="_top" href="prop_style_flex.asp">flex</a>  
  <a target="_top" href="prop_style_flexbasis.asp">flexBasis</a>  
  <a target="_top" href="prop_style_flexdirection.asp">flexDirection</a>  
  <a target="_top" href="prop_style_flexflow.asp">flexFlow</a>  
  <a target="_top" href="prop_style_flexgrow.asp">flexGrow</a>  
  <a target="_top" href="prop_style_flexshrink.asp">flexShrink</a>  
  <a target="_top" href="prop_style_flexwrap.asp">flexWrap</a>  
  <a target="_top" href="prop_style_cssfloat.asp">cssFloat</a>  
  <a target="_top" href="prop_style_font.asp">font</a>  
  <a target="_top" href="prop_style_fontfamily.asp">fontFamily</a>  
  <a target="_top" href="prop_style_fontsize.asp">fontSize</a>  
  <a target="_top" href="prop_style_fontstyle.asp">fontStyle</a>  
  <a target="_top" href="prop_style_fontvariant.asp">fontVariant</a>  
  <a target="_top" href="prop_style_fontweight.asp">fontWeight</a>  
  <a target="_top" href="prop_style_fontsizeadjust.asp">fontSizeAdjust</a>  
  <a target="_top" href="prop_style_height.asp">height</a>  
  <a target="_top" href="prop_style_isolation.asp">isolation</a>  
  <a target="_top" href="prop_style_justifycontent.asp">justifyContent</a>  
  <a target="_top" href="prop_style_left.asp">left</a>  
  <a target="_top" href="prop_style_letterspacing.asp">letterSpacing</a>  
  <a target="_top" href="prop_style_lineheight.asp">lineHeight</a>  
  <a target="_top" href="prop_style_liststyle.asp">listStyle</a>  
  <a target="_top" href="prop_style_liststyleimage.asp">listStyleImage</a>  
  <a target="_top" href="prop_style_liststyleposition.asp">listStylePosition</a>  
  <a target="_top" href="prop_style_liststyletype.asp">listStyleType</a>  
  <a target="_top" href="prop_style_margin.asp">margin</a>  
  <a target="_top" href="prop_style_marginbottom.asp">marginBottom</a>  
  <a target="_top" href="prop_style_marginleft.asp">marginLeft</a>  
  <a target="_top" href="prop_style_marginright.asp">marginRight</a>  
  <a target="_top" href="prop_style_margintop.asp">marginTop</a>  
  <a target="_top" href="prop_style_maxheight.asp">maxHeight</a>  
  <a target="_top" href="prop_style_maxwidth.asp">maxWidth</a>  
  <a target="_top" href="prop_style_minheight.asp">minHeight</a>  
  <a target="_top" href="prop_style_minwidth.asp">minWidth</a>  
  <a target="_top" href="prop_style_objectfit.asp">objectFit</a>  
  <a target="_top" href="prop_style_objectposition.asp">objectPosition</a>  
  <a target="_top" href="prop_style_opacity.asp">opacity</a>  
  <a target="_top" href="prop_style_order.asp">order</a>  
  <a target="_top" href="prop_style_orphans.asp">orphans</a>  
  <a target="_top" href="prop_style_outline.asp">outline</a>  
  <a target="_top" href="prop_style_outlinecolor.asp">outlineColor</a>  
  <a target="_top" href="prop_style_outlineoffset.asp">outlineOffset</a>  
  <a target="_top" href="prop_style_outlinestyle.asp">outlineStyle</a>  
  <a target="_top" href="prop_style_outlinewidth.asp">outlineWidth</a>  
  <a target="_top" href="prop_style_overflow.asp">overflow</a>  
  <a target="_top" href="prop_style_overflowx.asp">overflowX</a>  
  <a target="_top" href="prop_style_overflowy.asp">overflowY</a>  
  <a target="_top" href="prop_style_padding.asp">padding</a>  
  <a target="_top" href="prop_style_paddingbottom.asp">paddingBottom</a>  
  <a target="_top" href="prop_style_paddingleft.asp">paddingLeft</a>  
  <a target="_top" href="prop_style_paddingright.asp">paddingRight</a>  
  <a target="_top" href="prop_style_paddingtop.asp">paddingTop</a>  
  <a target="_top" href="prop_style_pagebreakafter.asp">pageBreakAfter</a>  
  <a target="_top" href="prop_style_pagebreakbefore.asp">pageBreakBefore</a>  
  <a target="_top" href="prop_style_pagebreakinside.asp">pageBreakInside</a>  
  <a target="_top" href="prop_style_perspective.asp">perspective</a>  
  <a target="_top" href="prop_style_perspectiveorigin.asp">perspectiveOrigin</a>  
  <a target="_top" href="prop_style_position.asp">position</a>  
  <a target="_top" href="prop_style_quotes.asp">quotes</a>  
  <a target="_top" href="prop_style_resize.asp">resize</a>  
  <a target="_top" href="prop_style_right.asp">right</a>  
  <a target="_top" href="prop_style_tablelayout.asp">tableLayout</a>  
  <a target="_top" href="prop_style_tabsize.asp">tabSize</a>  
  <a target="_top" href="prop_style_textalign.asp">textAlign</a>  
  <a target="_top" href="prop_style_textalignlast.asp">textAlignLast</a>  
  <a target="_top" href="prop_style_textdecoration.asp">textDecoration</a>  
  <a target="_top" href="prop_style_textdecorationcolor.asp">textDecorationColor</a>  
  <a target="_top" href="prop_style_textdecorationline.asp">textDecorationLine</a>  
  <a target="_top" href="prop_style_textdecorationstyle.asp">textDecorationStyle</a>  
  <a target="_top" href="prop_style_textindent.asp">textIndent</a>  
  <a target="_top" href="prop_style_textoverflow.asp">textOverflow</a>  
  <a target="_top" href="prop_style_textshadow.asp">textShadow</a>  
  <a target="_top" href="prop_style_texttransform.asp">textTransform</a>  
  <a target="_top" href="prop_style_top.asp">top</a>  
  <a target="_top" href="prop_style_transform.asp">transform</a>  
  <a target="_top" href="prop_style_transformorigin.asp">transformOrigin</a>  
  <a target="_top" href="prop_style_transformstyle.asp">transformStyle</a>  
  <a target="_top" href="prop_style_transition.asp">transition</a>  
  <a target="_top" href="prop_style_transitionproperty.asp">transitionProperty</a>  
  <a target="_top" href="prop_style_transitionduration.asp">transitionDuration</a>  
  <a target="_top" href="prop_style_transitiontimingfunction.asp">transitionTimingFunction</a>  
  <a target="_top" href="prop_style_transitiondelay.asp">transitionDelay</a>  
  <a target="_top" href="prop_style_unicodebidi.asp">unicodeBidi</a>  
  <a target="_top" href="prop_style_userselect.asp">userSelect</a>  
  <a target="_top" href="prop_style_verticalalign.asp">verticalAlign</a>  
  <a target="_top" href="prop_style_visibility.asp">visibility</a>  
  <a target="_top" href="prop_style_width.asp">width</a>  
  <a target="_top" href="prop_style_wordbreak.asp">wordBreak</a>  
  <a target="_top" href="prop_style_wordspacing.asp">wordSpacing</a>  
  <a target="_top" href="prop_style_wordwrap.asp">wordWrap</a>  
  <a target="_top" href="prop_style_widows.asp">widows</a>  
  <a target="_top" href="prop_style_zindex.asp">zIndex</a>  
</div>

<a target="_top" href="obj_window.asp">DOM Window</a>
<div class="ref_overview" style="margin-left:10px;background-color:#ddd">
  <a target="_top" href="prop_win_closed.asp">closed</a>  
  <a target="_top" href="obj_console.asp">console</a>  
  <a target="_top" href="prop_win_defaultstatus.asp">defaultStatus</a>  
  <a target="_top" href="dom_obj_document.asp">document</a>  
  <a target="_top" href="prop_win_frameElement.asp">frameElement</a>  
  <a target="_top" href="prop_win_frames.asp">frames</a>  
  <a target="_top" href="obj_history.asp">history</a>  
  <a target="_top" href="prop_win_innerheight.asp">innerHeight</a>  
  <a target="_top" href="prop_win_innerheight.asp">innerWidth</a>  
  <a target="_top" href="prop_win_length.asp">length</a>  
  <a target="_top" href="prop_win_localstorage.asp">localStorage</a>  
  <a target="_top" href="obj_location.asp">location</a>  
  <a target="_top" href="prop_win_name.asp">name</a>  
  <a target="_top" href="obj_navigator.asp">navigator</a>  
  <a target="_top" href="prop_win_opener.asp">opener</a>  
  <a target="_top" href="prop_win_outerheight.asp">outerHeight</a>  
  <a target="_top" href="prop_win_outerheight.asp">outerWidth</a>  
  <a target="_top" href="prop_win_pagexoffset.asp">pageXOffset</a>  
  <a target="_top" href="prop_win_pagexoffset.asp">pageYOffset</a>  
  <a target="_top" href="prop_win_parent.asp">parent</a>  
  <a target="_top" href="obj_screen.asp">screen</a>  
  <a target="_top" href="prop_win_screenleft.asp">screenLeft</a>  
  <a target="_top" href="prop_win_screenleft.asp">screenTop</a>  
  <a target="_top" href="prop_win_screenx.asp">screenX</a>  
  <a target="_top" href="prop_win_screenx.asp">screenY</a>  
  <a target="_top" href="prop_win_sessionstorage.asp">sessionStorage</a>  
  <a target="_top" href="prop_win_self.asp">self</a>  
  <a target="_top" href="prop_win_status.asp">status</a>  
  <a target="_top" href="prop_win_top.asp">top</a>  
  <a target="_top" href="met_win_alert.asp">alert()</a>  
  <a target="_top" href="met_win_atob.asp">atob()</a>  
  <a target="_top" href="met_win_blur.asp">blur()</a>  
  <a target="_top" href="met_win_btoa.asp">btoa()</a>  
  <a target="_top" href="met_win_clearinterval.asp">clearInterval()</a>  
  <a target="_top" href="met_win_cleartimeout.asp">clearTimeout()</a>  
  <a target="_top" href="met_win_close.asp">close()</a>  
  <a target="_top" href="met_win_confirm.asp">confirm()</a>  
  <a target="_top" href="met_win_focus.asp">focus()</a>  
  <a target="_top" href="jsref_getcomputedstyle.asp">getComputedStyle()</a>  
  <a target="_top" href="met_win_matchmedia.asp">matchMedia()</a>  
  <a target="_top" href="met_win_moveby.asp">moveBy()</a>  
  <a target="_top" href="met_win_moveto.asp">moveTo()</a>  
  <a target="_top" href="met_win_open.asp">open()</a>  
  <a target="_top" href="met_win_print.asp">print()</a>  
  <a target="_top" href="met_win_prompt.asp">prompt()</a>  
  <a target="_top" href="met_win_resizeby.asp">resizeBy()</a>  
  <a target="_top" href="met_win_resizeto.asp">resizeTo()</a>  
  <a target="_top" href="met_win_scrollby.asp">scrollBy()</a>  
  <a target="_top" href="met_win_scrollto.asp">scrollTo()</a>  
  <a target="_top" href="met_win_setinterval.asp">setInterval()</a>  
  <a target="_top" href="met_win_settimeout.asp">setTimeout()</a>  
  <a target="_top" href="met_win_stop.asp">stop()</a>  
</div>

<a target="_top" href="obj_storage.asp">WEB Storage</a>
<div class="ref_overview" style="margin-left:10px;background-color:#ddd">
  <a target="_top" href="met_storage_key.asp">key()</a>
  <a target="_top" href="prop_storage_length.asp">length</a>
  <a target="_top" href="met_storage_getitem.asp">getItem()</a>
  <a target="_top" href="met_storage_setitem.asp">setItem()</a>
  <a target="_top" href="met_storage_removeitem.asp">removeItem()</a>
  <a target="_top" href="met_storage_clear.asp">clear()</a>
</div>

<br>
<h2 class="left"><span class="left_h2">HTML Objects</span></h2>
<a target="_top" href="dom_obj_anchor.asp">&lt;a&gt;</a>
<a target="_top" href="dom_obj_abbr.asp">&lt;abbr&gt;</a>
<a target="_top" href="dom_obj_address.asp">&lt;address&gt;</a>
<a target="_top" href="dom_obj_area.asp">&lt;area&gt;</a>
<a target="_top" href="dom_obj_article.asp">&lt;article&gt;</a>
<a target="_top" href="dom_obj_aside.asp">&lt;aside&gt;</a>
<a target="_top" href="dom_obj_audio.asp">&lt;audio&gt;</a>
<a target="_top" href="dom_obj_b.asp">&lt;b&gt;</a>
<a target="_top" href="dom_obj_base.asp">&lt;base&gt;</a>
<a target="_top" href="dom_obj_bdo.asp">&lt;bdo&gt;</a>
<a target="_top" href="dom_obj_blockquote.asp">&lt;blockquote&gt;</a>
<a target="_top" href="dom_obj_body.asp">&lt;body&gt;</a>
<a target="_top" href="dom_obj_br.asp">&lt;br&gt;</a>
<a target="_top" href="dom_obj_pushbutton.asp">&lt;button&gt;</a>
<a target="_top" href="dom_obj_canvas.asp">&lt;canvas&gt;</a>
<a target="_top" href="dom_obj_caption.asp">&lt;caption&gt;</a>
<a target="_top" href="dom_obj_cite.asp">&lt;cite&gt;</a>
<a target="_top" href="dom_obj_code.asp">&lt;code&gt;</a>
<a target="_top" href="dom_obj_col.asp">&lt;col&gt;</a>
<a target="_top" href="dom_obj_colgroup.asp">&lt;colgroup&gt;</a>
<a target="_top" href="dom_obj_datalist.asp">&lt;datalist&gt;</a>
<a target="_top" href="dom_obj_dd.asp">&lt;dd&gt;</a>
<a target="_top" href="dom_obj_del.asp">&lt;del&gt;</a>
<a target="_top" href="dom_obj_details.asp">&lt;details&gt;</a>
<a target="_top" href="dom_obj_dfn.asp">&lt;dfn&gt;</a>
<a target="_top" href="dom_obj_dialog.asp">&lt;dialog&gt;</a>
<a target="_top" href="dom_obj_div.asp">&lt;div&gt;</a>
<a target="_top" href="dom_obj_dl.asp">&lt;dl&gt;</a>
<a target="_top" href="dom_obj_dt.asp">&lt;dt&gt;</a>
<a target="_top" href="dom_obj_em.asp">&lt;em&gt;</a>
<a target="_top" href="dom_obj_embed.asp">&lt;embed&gt;</a>
<a target="_top" href="dom_obj_fieldset.asp">&lt;fieldset&gt;</a>
<a target="_top" href="dom_obj_figcaption.asp">&lt;figcaption&gt;</a>
<a target="_top" href="dom_obj_figure.asp">&lt;figure&gt;</a>
<a target="_top" href="dom_obj_footer.asp">&lt;footer&gt;</a>
<a target="_top" href="dom_obj_form.asp">&lt;form&gt;</a>
<a target="_top" href="dom_obj_head.asp">&lt;head&gt;</a>
<a target="_top" href="dom_obj_header.asp">&lt;header&gt;</a>
<a target="_top" href="dom_obj_heading.asp">&lt;h1&gt; - &lt;h6&gt;</a>
<a target="_top" href="dom_obj_hr.asp">&lt;hr&gt;</a>
<a target="_top" href="dom_obj_html.asp">&lt;html&gt;</a>
<a target="_top" href="dom_obj_i.asp">&lt;i&gt;</a>
<a target="_top" href="dom_obj_frame.asp">&lt;iframe&gt;</a>
<a target="_top" href="dom_obj_image.asp">&lt;img&gt;</a>
<a target="_top" href="dom_obj_ins.asp">&lt;ins&gt;</a>
<a target="_top" href="dom_obj_button.asp">&lt;input&gt; button</a>
<a target="_top" href="dom_obj_checkbox.asp">&lt;input&gt; checkbox</a>
<a target="_top" href="dom_obj_color.asp">&lt;input&gt; color</a>
<a target="_top" href="dom_obj_date.asp">&lt;input&gt; date</a>
<a target="_top" href="dom_obj_datetime.asp">&lt;input&gt; datetime</a>
<a target="_top" href="dom_obj_datetime-local.asp">&lt;input&gt; datetime-local</a>
<a target="_top" href="dom_obj_email.asp">&lt;input&gt; email</a>
<a target="_top" href="dom_obj_fileupload.asp">&lt;input&gt; file</a>
<a target="_top" href="dom_obj_hidden.asp">&lt;input&gt; hidden</a>
<a target="_top" href="dom_obj_input_image.asp">&lt;input&gt; image</a>
<a target="_top" href="dom_obj_month.asp">&lt;input&gt; month</a>
<a target="_top" href="dom_obj_number.asp">&lt;input&gt; number</a>
<a target="_top" href="dom_obj_password.asp">&lt;input&gt; password</a>
<a target="_top" href="dom_obj_radio.asp">&lt;input&gt; radio</a>
<a target="_top" href="dom_obj_range.asp">&lt;input&gt; range</a>
<a target="_top" href="dom_obj_reset.asp">&lt;input&gt; reset</a>
<a target="_top" href="dom_obj_search.asp">&lt;input&gt; search</a>
<a target="_top" href="dom_obj_submit.asp">&lt;input&gt; submit</a>
<a target="_top" href="dom_obj_text.asp">&lt;input&gt; text</a>
<a target="_top" href="dom_obj_input_time.asp">&lt;input&gt; time</a>
<a target="_top" href="dom_obj_url.asp">&lt;input&gt; url</a>
<a target="_top" href="dom_obj_week.asp">&lt;input&gt; week</a>
<a target="_top" href="dom_obj_kbd.asp">&lt;kbd&gt;</a>
<a target="_top" href="dom_obj_label.asp">&lt;label&gt;</a>
<a target="_top" href="dom_obj_legend.asp">&lt;legend&gt;</a>
<a target="_top" href="dom_obj_li.asp">&lt;li&gt;</a>
<a target="_top" href="dom_obj_link.asp">&lt;link&gt;</a>
<a target="_top" href="dom_obj_map.asp">&lt;map&gt;</a>
<a target="_top" href="dom_obj_mark.asp">&lt;mark&gt;</a>
<a target="_top" href="dom_obj_menu.asp">&lt;menu&gt;</a>
<a target="_top" href="dom_obj_menuitem.asp">&lt;menuitem&gt;</a>
<a target="_top" href="dom_obj_meta.asp">&lt;meta&gt;</a>
<a target="_top" href="dom_obj_meter.asp">&lt;meter&gt;</a>
<a target="_top" href="dom_obj_nav.asp">&lt;nav&gt;</a>
<a target="_top" href="dom_obj_object.asp">&lt;object&gt;</a>
<a target="_top" href="dom_obj_ol.asp">&lt;ol&gt;</a>
<a target="_top" href="dom_obj_optgroup.asp">&lt;optgroup&gt;</a>
<a target="_top" href="dom_obj_option.asp">&lt;option&gt;</a>
<a target="_top" href="dom_obj_output.asp">&lt;output&gt;</a>
<a target="_top" href="dom_obj_paragraph.asp">&lt;p&gt;</a>
<a target="_top" href="dom_obj_param.asp">&lt;param&gt;</a>
<a target="_top" href="dom_obj_pre.asp">&lt;pre&gt;</a>
<a target="_top" href="dom_obj_progress.asp">&lt;progress&gt;</a>
<a target="_top" href="dom_obj_quote.asp">&lt;q&gt;</a>
<a target="_top" href="dom_obj_s.asp">&lt;s&gt;</a>
<a target="_top" href="dom_obj_samp.asp">&lt;samp&gt;</a>
<a target="_top" href="dom_obj_script.asp">&lt;script&gt;</a>
<a target="_top" href="dom_obj_section.asp">&lt;section&gt;</a>
<a target="_top" href="dom_obj_select.asp">&lt;select&gt;</a>
<a target="_top" href="dom_obj_small.asp">&lt;small&gt;</a>
<a target="_top" href="dom_obj_source.asp">&lt;source&gt;</a>
<a target="_top" href="dom_obj_span.asp">&lt;span&gt;</a>
<a target="_top" href="dom_obj_strong.asp">&lt;strong&gt;</a>
<a target="_top" href="dom_obj_style.asp">&lt;style&gt;</a>
<a target="_top" href="dom_obj_sub.asp">&lt;sub&gt;</a>
<a target="_top" href="dom_obj_summary.asp">&lt;summary&gt;</a>
<a target="_top" href="dom_obj_sup.asp">&lt;sup&gt;</a>
<a target="_top" href="dom_obj_table.asp">&lt;table&gt;</a>
<a target="_top" href="dom_obj_tbody.asp">&lt;tbody&gt;</a>
<a target="_top" href="dom_obj_tabledata.asp">&lt;td&gt;</a>
<a target="_top" href="dom_obj_tfoot.asp">&lt;tfoot&gt;</a>
<a target="_top" href="dom_obj_tablehead.asp">&lt;th&gt;</a>
<a target="_top" href="dom_obj_thead.asp">&lt;thead&gt;</a>
<a target="_top" href="dom_obj_tablerow.asp">&lt;tr&gt;</a>
<a target="_top" href="dom_obj_textarea.asp">&lt;textarea&gt;</a>
<a target="_top" href="dom_obj_time.asp">&lt;time&gt;</a>
<a target="_top" href="dom_obj_title.asp">&lt;title&gt;</a>
<a target="_top" href="dom_obj_track.asp">&lt;track&gt;</a>
<a target="_top" href="dom_obj_u.asp">&lt;u&gt;</a>
<a target="_top" href="dom_obj_ul.asp">&lt;ul&gt;</a>
<a target="_top" href="dom_obj_var.asp">&lt;var&gt;</a>
<a target="_top" href="dom_obj_video.asp">&lt;video&gt;</a>
<br>
<h2 class="left"><span class="left_h2">Other References</span></h2>
<a target="_top" href="obj_cssstyledeclaration.asp">CSSStyleDeclaration</a>
<div class="ref_overview" style="margin-left:10px;background-color:#ddd">
  <a href="prop_cssstyle_csstext.asp">cssText</a>
  <a href="met_cssstyle_getpropertypriority.asp">getPropertyPriority()</a>
  <a href="met_cssstyle_getpropertyvalue.asp">getPropertyValue()</a>
  <a href="met_cssstyle_item.asp">item()</a>
  <a href="prop_cssstyle_length.asp">length</a>
  <a href="prop_cssstyle_parentrule.asp">parentRule</a>
  <a href="met_cssstyle_removeproperty.asp">removeProperty()</a>
  <a href="met_cssstyle_setproperty.asp">setProperty()</a>
</div>

<a target="_top" href="jsref_type_conversion.asp">JS Conversion</a>
<br>
</div>
      <br><br>
    </div>
  </div>
</div>
<div class='w3-main w3-light-grey' id='belowtopnav' style='margin-left:220px;'>
  <div class='w3-row w3-white'>
    <div class='w3-col l10 m12' id='main'>
      <div id='mainLeaderboard' style='overflow:hidden;'>
        <!-- MainLeaderboard-->

        <!--<pre>main_leaderboard, all: [728,90][970,90][320,50][468,60]</pre>-->
        <div id="snhb-main_leaderboard-0"></div>
        <!-- adspace leaderboard -->
       
      </div>
<h1>JavaScript Array <span class="color_h1">includes()</span> Method</h1>
<p><a href="jsref_obj_array.asp" class="w3-btn w3-white w3-border" title="JavaScript Array Reference">&#10094; JavaScript Array Reference</a></p>

<div class="w3-example">
<h3>Example</h3>
<p>Check if an array includes "Mango":</p>
<div class="w3-code notranslate jsHigh">
var fruits = [&quot;Banana&quot;, &quot;Orange&quot;, &quot;Apple&quot;, &quot;Mango&quot;];<br>
var n = fruits.includes(&quot;Mango&quot;);
</div>
<!--<p> 
The result of <em>n</em> will be:</p>
<div class="w3-code notranslate">
true
</div>-->
<a target="_blank" href="tryit.asp?filename=tryjsref_includes_array" class="w3-btn w3-margin-bottom">Try it Yourself &raquo;</a>
</div>
<hr>

<h2>Definition and Usage</h2>
<p>The includes() method determines whether an array contains a specified 
element.</p>
<p>This method returns <em>true</em> if the array contains the element, and
<em>false</em> if not.</p>
<p><b>Note:</b> The <em>includes()</em> method is case sensitive.</p>
<hr>

<h2>Browser Support</h2>
<table class="browserref notranslate">
  <tr>
    <th style="width:20%;font-size:16px;text-align:left;">Method</th>
    <th style="width:16%;" class="bsChrome" title="Chrome"></th>
    <th style="width:16%;" class="bsEdge" title="Edge"></th>
    <th style="width:16%;" class="bsFirefox" title="Firefox"></th>
    <th style="width:16%;" class="bsSafari" title="Safari"></th>
    <th style="width:16%;" class="bsOpera" title="Opera"></th>                
  </tr>
  <tr>
    <td style="text-align:left;">includes()</td>
    <td>47</td>
    <td>14.0</td>    
    <td>43</td>
    <td>9</td>
    <td>34</td>
  </tr>
</table>
<hr>

<h2>Syntax</h2>
<div class="w3-code w3-border notranslate"><div>
<i>array</i>.includes(<em>element</em>,<em> start</em>)</div></div>
<h2>Parameter Values</h2>
<table class="w3-table-all notranslate"> 
  <tr>
    <th style="width:23%">Parameter</th>
    <th>Description</th>  </tr>  
  <tr>
    <td><em>element</em></td>
    <td>Required. The element to search for</td>
  </tr>
    <tr>
    <td><em>start</em></td>
    <td>Optional. Default 0. At which position in the array to start the search</td>
  </tr>
</table>
<h2>Technical Details</h2>
<table class="w3-table-all">
  <tr>
 <th style="width:25%">Return Value:</th>
  <td>A Boolean</td>
  </tr>
  <tr>
 <th style="width:25%">JavaScript Version:</th>
  <td>ECMAScript 7</td>
  </tr>
</table>

<hr>

<h2>More Examples</h2>

<div class="w3-example">
<p>Check if an array includes "Banana", starting the search at position 3:</p>
 <div class="w3-code notranslate jsHigh">
var fruits = [&quot;Banana&quot;, &quot;Orange&quot;, &quot;Apple&quot;, &quot;Mango&quot;];<br>
var n = fruits.includes(&quot;Banana&quot;, 3);
</div>
<!--<p> 
The result of <em>n</em> will be:</p>
<div class="w3-code notranslate">
 false
</div>-->
 <a class="w3-btn w3-margin-bottom" href="tryit.asp?filename=tryjsref_includes_array2" target="_blank">Try it Yourself &raquo;</a>
</div>
<hr>
<a href="jsref_obj_array.asp" class="w3-btn w3-white w3-border" title="JavaScript Array Reference">&#10094; JavaScript Array Reference</a>
</div>
<div class="w3-col l2 m12" id="right">

<div class="sidesection">
  <div id="skyscraper">
  
    <!--<pre>wide_skyscraper, all: [160,600][300,600][320,50][120,600][300,1050]</pre>-->
    <div id="snhb-wide_skyscraper-0"></div>
    <!-- adspace wide-->
  
  </div>
</div>

<div class="sidesection">
<h4><a href="/colors/colors_picker.asp">COLOR PICKER</a></h4>
<a href="/colors/colors_picker.asp">
<img src="/images/colorpicker.gif" alt="colorpicker"></a>
</div>

<div class="sidesection" id="moreAboutSubject">
</div>

<!--
<div id="sidesection_exercise" class="sidesection" style="background-color:#555555;max-width:200px;margin:auto;margin-bottom:32px">
  <div class="w3-container w3-text-white">
    <h4>Exercises</h4>
  </div>
  <div>
    <div class="w3-light-grey">
      <a target="_blank" href="/html/exercise.asp" style="padding-top:8px">HTML</a>
      <a target="_blank" href="/css/exercise.asp">CSS</a>
      <a target="_blank" href="/js/exercise_js.asp">JavaScript</a>
      <a target="_blank" href="/sql/exercise.asp">SQL</a>
      <a target="_blank" href="/php/exercise.asp">PHP</a>
      <a target="_blank" href="/python/exercise.asp">Python</a>
      <a target="_blank" href="/bootstrap/exercise_bs3.asp">Bootstrap</a>
      <a target="_blank" href="/jquery/exercise_jq.asp" style="padding-bottom:8px">jQuery</a>
    </div>
  </div>
</div>
-->

<div class="sidesection w3-light-grey" style="margin-left:auto;margin-right:auto;max-width:230px">
  <div class="w3-container w3-dark-grey">
    <h4><a href="/howto/default.asp" class="w3-hover-text-white">HOW TO</a></h4>
  </div>
  <div class="w3-container w3-left-align w3-padding-16">
    <a href="/howto/howto_js_tabs.asp">Tabs</a><br>
    <a href="/howto/howto_css_dropdown.asp">Dropdowns</a><br>
    <a href="/howto/howto_js_accordion.asp">Accordions</a><br>
    <a href="/howto/howto_js_sidenav.asp">Side Navigation</a><br>
    <a href="/howto/howto_js_topnav.asp">Top Navigation</a><br>
    <a href="/howto/howto_css_modals.asp">Modal Boxes</a><br>
    <a href="/howto/howto_js_progressbar.asp">Progress Bars</a><br>
    <a href="/howto/howto_css_parallax.asp">Parallax</a><br>
    <a href="/howto/howto_css_login_form.asp">Login Form</a><br>
    <a href="/howto/howto_html_include.asp">HTML Includes</a><br>
    <a href="/howto/howto_google_maps.asp">Google Maps</a><br>
    <a href="/howto/howto_js_rangeslider.asp">Range Sliders</a><br>
    <a href="/howto/howto_css_tooltip.asp">Tooltips</a><br>
    <a href="/howto/howto_js_slideshow.asp">Slideshow</a><br>
    <a href="/howto/howto_js_filter_lists.asp">Filter List</a><br>
    <a href="/howto/howto_js_sort_list.asp">Sort List</a><br>
  </div>
</div>

<div class="sidesection">
<h4>SHARE</h4>
<div class="w3-text-grey sharethis">
<script>
<!--
try{
loc=location.pathname;
if (loc.toUpperCase().indexOf(".ASP")<0) loc=loc+"default.asp";
txt='<a href="http://www.facebook.com/sharer.php?u=https://www.w3schools.com'+loc+'" target="_blank" title="Facebook"><span class="fa fa-facebook-square fa-2x"></span></a>';
txt=txt+'<a href="https://twitter.com/home?status=Currently reading https://www.w3schools.com'+loc+'" target="_blank" title="Twitter"><span class="fa fa-twitter-square fa-2x"></span></a>';
document.write(txt);
} catch(e) {}
//-->
</script>
<br><br>
<a href="javascript:void(0);" onclick="clickFBLike()" title="Like W3Schools on Facebook">
<span class="fa fa-thumbs-o-up fa-2x"></span></a>
<div id="fblikeframe" class="w3-modal">
<div class="w3-modal-content w3-padding-64 w3-animate-zoom" id="popupDIV"></div>
</div>
</div>
</div>

<div class="sidesection">
<h4><a target="_blank" href="//www.w3schools.com/cert/default.asp">CERTIFICATES</a></h4>
<p>
<a href="/cert/cert_html.asp">HTML</a><br>
<a href="/cert/cert_css.asp">CSS</a><br>
<a href="/cert/cert_javascript.asp">JavaScript</a><br>
<a href="/cert/cert_sql.asp">SQL</a><br>
<a href="/cert/cert_python.asp">Python</a><br>
<a href="/cert/cert_php.asp">PHP</a><br>
<a href="/cert/cert_jquery.asp">jQuery</a><br>
<a href="/cert/cert_bootstrap.asp">Bootstrap</a><br>
<a href="/cert/cert_xml.asp">XML</a></p>
<a href="//www.w3schools.com/cert/default.asp" class="w3-button w3-dark-grey" style="text-decoration:none">
Read More &raquo;</a>
</div>

<div id="stickypos" class="sidesection" style="text-align:center;position:sticky;top:50px;">
  <div id="stickyadcontainer">
    <div style="position:relative;margin:auto;">
      
      <!--<pre>sidebar_sticky, desktop: [120,600][160,600][300,600][300,250]</pre>-->
      <div id="snhb-sidebar_sticky-0"></div>
      <script>
          if (Number(w3_getStyleValue(document.getElementById("main"), "height").replace("px", "")) > 2200) {
            // adspace sticky
            if (document.getElementById("snhb-mid_content-0")) {
              snhb.queue.push(function(){  snhb.startAuction(["sidebar_sticky", "mid_content" ]); });
            }
            else {
              snhb.queue.push(function(){  snhb.startAuction(["sidebar_sticky"]); });
            }
          }
          else {
              if (document.getElementById("snhb-mid_content-0")) {
                snhb.queue.push(function(){  snhb.startAuction(["mid_content"]); });
              }
          }
      </script>  
      
    </div>
  </div>
</div>

<script>
  window.addEventListener("scroll", fix_stickyad);
  window.addEventListener("resize", fix_stickyad);
</script>

</div>
</div>
<div id="footer" class="footer w3-container w3-white">

<hr>

<div style="overflow:auto">
  <div class="bottomad">
    <!-- BottomMediumRectangle -->
    <!--<pre>bottom_medium_rectangle, all: [970,250][300,250][336,280]</pre>-->
    <div id="snhb-bottom_medium_rectangle-0" style="padding:0 10px 10px 0;float:left;width:auto;"></div>
    <!-- adspace bmr -->
    <!-- RightBottomMediumRectangle -->
    <!--<pre>right_bottom_medium_rectangle, desktop: [300,250][336,280]</pre>-->
    <div id="snhb-right_bottom_medium_rectangle-0" style="padding:0 10px 10px 0;float:left;width:auto;"></div>
  </div>
</div>

<hr>
<div class="w3-row-padding w3-center w3-small" style="margin:0 -16px">
<div class="w3-col l3 m3 s12">
<a class="w3-button w3-light-grey w3-block" href="javascript:void(0);" onclick="displayError();return false" style="white-space:nowrap;text-decoration:none;margin-top:1px;margin-bottom:1px">REPORT ERROR</a>
</div>
<div class="w3-col l3 m3 s12">
<a class="w3-button w3-light-grey w3-block" href="javascript:void(0);" target="_blank" onclick="printPage();return false;" style="text-decoration:none;margin-top:1px;margin-bottom:1px">PRINT PAGE</a>
</div>
<div class="w3-col l3 m3 s12">
<a class="w3-button w3-light-grey w3-block" href="/forum/default.asp" target="_blank" style="text-decoration:none;margin-top:1px;margin-bottom:1px">FORUM</a>
</div>
<div class="w3-col l3 m3 s12">
<a class="w3-button w3-light-grey w3-block" href="/about/default.asp" target="_top" style="text-decoration:none;margin-top:1px;margin-bottom:1px">ABOUT</a>
</div>
</div>
<hr>
<div class="w3-light-grey w3-padding w3-margin-bottom" id="err_form" style="display:none;position:relative">
<span onclick="this.parentElement.style.display='none'" class="w3-button w3-display-topright w3-large">&times;</span>     
<h2>Your Suggestion:</h2>
<form>
<div class="w3-section">      
<label for="err_email">Your E-mail:</label>
<input class="w3-input w3-border" type="text" style="margin-top:5px;width:100%" id="err_email" name="err_email">
</div>
<div class="w3-section">      
<label for="err_email">Page address:</label>
<input class="w3-input w3-border" type="text" style="width:100%;margin-top:5px" id="err_url" name="err_url" disabled="disabled">
</div>
<div class="w3-section">
<label for="err_email">Description:</label>
<textarea rows="10" class="w3-input w3-border" id="err_desc" name="err_desc" style="width:100%;margin-top:5px;resize:vertical;"></textarea>
</div>
<div class="form-group">        
<button type="button" class="w3-button w3-dark-grey" onclick="sendErr()">Submit</button>
</div>
<br>
</form>
</div>
<div class="w3-container w3-light-grey w3-padding" id="err_sent" style="display:none;position:relative">
<span onclick="this.parentElement.style.display='none'" class="w3-button w3-display-topright">&times;</span>     
<h2>Thank You For Helping Us!</h2>
<p>Your message has been sent to W3Schools.</p>
</div>

<div class="w3-row w3-center w3-small">
<div class="w3-col l3 m6 s12">
<div class="top10">
<h4>Top Tutorials</h4>
<a href="/html/default.asp">HTML Tutorial</a><br>
<a href="/css/default.asp">CSS Tutorial</a><br>
<a href="/js/default.asp">JavaScript Tutorial</a><br>
<a href="/howto/default.asp">How To Tutorial</a><br>
<a href="/sql/default.asp">SQL Tutorial</a><br>
<a href="/python/default.asp">Python Tutorial</a><br>
<a href="/w3css/default.asp">W3.CSS Tutorial</a><br>
<a href="/bootstrap/bootstrap_ver.asp">Bootstrap Tutorial</a><br>
<a href="/php/default.asp">PHP Tutorial</a><br>
<a href="/jquery/default.asp">jQuery Tutorial</a><br>
<a href="/java/default.asp">Java Tutorial</a><br>
<a href="/cpp/default.asp">C++ Tutorial</a><br>
</div>
</div>
<div class="w3-col l3 m6 s12">
<div class="top10">
<h4>Top References</h4>
<a href="/tags/default.asp">HTML Reference</a><br>
<a href="/cssref/default.asp">CSS Reference</a><br>
<a href="/jsref/default.asp">JavaScript Reference</a><br>
<a href="/sql/sql_ref_keywords.asp">SQL Reference</a><br>
<a href="/python/python_reference.asp">Python Reference</a><br>
<a href="/w3css/w3css_references.asp">W3.CSS Reference</a><br>
<a href="/bootstrap/bootstrap_ref_all_classes.asp">Bootstrap Reference</a><br>
<a href="/php/php_ref_overview.asp">PHP Reference</a><br>
<a href="/colors/colors_names.asp">HTML Colors</a><br>
<a href="/jquery/jquery_ref_overview.asp">jQuery Reference</a><br>
<a href="/java/java_ref_keywords.asp">Java Reference</a><br>
<a href="/angular/angular_ref_directives.asp">Angular Reference</a><br>
</div>
</div>
<div class="w3-col l3 m6 s12">
<div class="top10">
<h4>Top Examples</h4>
<a href="/html/html_examples.asp">HTML Examples</a><br>
<a href="/css/css_examples.asp">CSS Examples</a><br>
<a href="/js/js_examples.asp">JavaScript Examples</a><br>
<a href="/howto/default.asp">How To Examples</a><br>
<a href="/sql/sql_examples.asp">SQL Examples</a><br>
<a href="/python/python_examples.asp">Python Examples</a><br>
<a href="/w3css/w3css_examples.asp">W3.CSS Examples</a><br>
<a href="/bootstrap/bootstrap_examples.asp">Bootstrap Examples</a><br>
<a href="/php/php_examples.asp">PHP Examples</a><br>
<a href="/jquery/jquery_examples.asp">jQuery Examples</a><br>
<a href="/java/java_examples.asp">Java Examples</a><br>
<a href="/xml/xml_examples.asp">XML Examples</a><br>
</div>
</div>
<div class="w3-col l3 m6 s12">
<div class="top10">
<h4>Web Certificates</h4>
<a href="/cert/default.asp">HTML Certificate</a><br>
<a href="/cert/default.asp">CSS Certificate</a><br>
<a href="/cert/default.asp">JavaScript Certificate</a><br>
<a href="/cert/default.asp">SQL Certificate</a><br>
<a href="/cert/default.asp">Python Certificate</a><br>
<a href="/cert/default.asp">jQuery Certificate</a><br>
<a href="/cert/default.asp">PHP Certificate</a><br>
<a href="/cert/default.asp">Bootstrap Certificate</a><br>
<a href="/cert/default.asp">XML Certificate</a><br>
<a href="//www.w3schools.com/cert/default.asp" class="w3-button w3-margin-top w3-dark-grey" style="text-decoration:none">
Get Certified &raquo;</a>

</div>
</div>        
</div>        

<hr>
<div class="w3-center w3-small w3-opacity">
W3Schools is optimized for learning, testing, and training. Examples might be simplified to improve reading and basic understanding.
Tutorials, references, and examples are constantly reviewed to avoid errors, but we cannot warrant full correctness of all content.
While using this site, you agree to have read and accepted our <a href="/about/about_copyright.asp">terms of use</a>,
<a href="/about/about_privacy.asp">cookie and privacy policy</a>.
<a href="/about/about_copyright.asp">Copyright 1999-2019</a> by Refsnes Data. All Rights Reserved.<br>
 <a href="//www.w3schools.com/w3css/">Powered by W3.CSS</a>.<br><br>
<a href="//www.w3schools.com">
<img style="width:150px;height:28px;border:0" src="/images/w3schoolscom_gray.gif" alt="W3Schools.com"></a>
</div>
<br><br>
</div>

</div>
<script src="/lib/w3schools_footer.js"></script>
<script src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>  
<![endif]-->
</body>
</html>
`;
