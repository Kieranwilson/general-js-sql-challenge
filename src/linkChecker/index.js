/**
 * Link Checker
 * You have a large bunch of HTML. Inside that HTML are p tags, li tags, table
 * tags, really any and all kinds of HTML tags. Most importantly there are
 * anchor/link tags.
 * Write a program to find all of the URLs to which those link tags link and
 * verify that the URLs return a 200 response. In a given chunk of this HTML,
 * we could have anywhere from 0 to 100+ links, so your solution should handle
 * the case where there are plenty of links.
 *
 * Requirements
 * First, you'll want to figure out a way to extract all of the URLs.
 * Second, you'll want to test the URLs and report back to the user which are
 * valid and which are not.
 * Third, you'll want to make it really fast by checking the URLs concurrently
 * or by parallelizing the checks. You might want to think about caching as well.
 */

// I was at a bit of a crossroads here in terms of deciding to use no external
// package and instead doing a basic regex matching to find all tags, look at
// the tag attributes for the links and then verify that way or to use a package
// such as cheerio to handle the parsing for me. In the end I went for cheerio
// because of the amount of sites with malformed html... and pre tags.
import cheerio from 'cheerio';
import linkStatusChecker from './linkFetcher';
import promiseWrapper from './promiseWrapper';

export const buildLink = hostName => string => {
  try {
    return new URL(string, hostName).toString();
  } catch (e) {
    return false;
  }
};

/** I was hoping to do a smarter search here so that you could just match against every
 * property, see if it's a valid link and then go and grab it, this is not really feasibile though
 * as a url could just be <a href="otherpage"> which is not an absolute URL and would not work
 * so that's why the approach is a bit wonky, from pursuing that avenue first:
  $('*').each(function(i, elem) {
    for (let attKey in elem.attribs) {
      const attr = elem.attribs[attKey];
      const link = hostRelevantLinks(attr);
      if (link) {
        linkArray.push({ elem, link, attr });
      }
 */

const linkAttributes = ['href', 'xmlns', 'src'];

export default (html, hostName) => {
  const hostRelevantLinks = buildLink(hostName);
  const $ = cheerio.load(html);
  const linkArray = [];
  const invalidArray = [];
  $('*').each(function(i, elem) {
    const links = Object.keys(elem.attribs).filter(attrib => linkAttributes.includes(attrib));
    if (links.length) {
      links.forEach(att => {
        const value = elem.attribs[att];
        const link = hostRelevantLinks(value);
        if (link) {
          linkArray.push({ elem, link, value });
        } else {
          console.error(`${link.value} is a malformed url`);
          invalidArray.push({ link: { elem, link, value } });
        }
      });
    }
  });
  // To optimise this I would also do it in batches but that's a bit overkill for now
  return promiseWrapper(linkArray.map(link => linkStatusChecker(link.link))).then(data => {
    return linkArray.reduce(
      (acc, link, index) => {
        if (data[index].data < 300) {
          console.log(`${link.value} is a valid url and returned ${data[index].data}`);
          return { ...acc, success: [...acc.success, { link, res: data[index] }] };
        }
        console.error(`${link.value} is a invalid url and returned ${data[index].data}`);
        return { ...acc, failure: [...acc.failure, { link, res: data[index] }] };
      },
      { success: [], failure: invalidArray }
    );
  });
};
