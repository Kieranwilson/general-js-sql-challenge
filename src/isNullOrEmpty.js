/**
 * Using the most appropriate means, implement an "isNullOrEmpty" check on the
 * standard String type. It should be functionally equivalent without calling
 * any "isNullOrEmpty" built in function.
 */

// Does what it says on the tin, I could have used Booelean(string) but I
// also wanted to ensure that the number 0 would not be returned as empty
export default string => string == null || string === '';
