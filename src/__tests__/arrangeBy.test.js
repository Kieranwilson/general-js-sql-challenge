import arrangeBy from '../arrangeBy';

describe('arrangeBy', () => {
  it('Should work for base case', () => {
    const users = [
      { id: 1, name: 'bob' },
      { id: 2, name: 'sally' },
      { id: 3, name: 'bob', age: 30 }
    ];
    expect(arrangeBy('name', users)).toEqual({
      bob: [{ id: 1, name: 'bob' }, { age: 30, id: 3, name: 'bob' }],
      sally: [{ id: 2, name: 'sally' }]
    });
  });

  it('Does not mutate any of the data', () => {
    const users = [
      { id: 1, name: 'bob' },
      { id: 2, name: 'sally' },
      { id: 3, name: 'bob', age: 30 }
    ];
    arrangeBy('name', users);
    expect(users).toEqual([
      { id: 1, name: 'bob' },
      { id: 2, name: 'sally' },
      { id: 3, name: 'bob', age: 30 }
    ]);
  });

  it('Ignores data that does not have the necessary key', () => {
    const users = [
      { id: 1, name: 'bob' },
      { id: 2, malformedName: 'sally' },
      { id: 3, name: 'bob', age: 30 }
    ];
    expect(arrangeBy('name', users)).toEqual({
      bob: [{ id: 1, name: 'bob' }, { age: 30, id: 3, name: 'bob' }]
    });
  });

  it('Ignores undefined, null, non objects', () => {
    const users = [{ id: 1, name: 'bob' }, null, undefined, [], 'string', 5];
    expect(arrangeBy('name', users)).toEqual({ bob: [{ id: 1, name: 'bob' }] });
  });

  it('Works for empty results', () => {
    expect(arrangeBy('name', [])).toEqual({});
  });

  it('Correctly adds many instances', () => {
    const users = [
      { id: 1, name: 'bob' },
      { id: 2, name: 'bob' },
      { id: 3, name: 'bob' },
      { id: 4, name: 'bob' },
      { id: 5, name: 'alice' },
      { id: 6, name: 'alice' },
      { id: 7, name: 'alice' },
      { id: 8, name: 'alice' }
    ];
    expect(arrangeBy('name', users)).toEqual({
      alice: [
        { id: 5, name: 'alice' },
        { id: 6, name: 'alice' },
        { id: 7, name: 'alice' },
        { id: 8, name: 'alice' }
      ],
      bob: [
        { id: 1, name: 'bob' },
        { id: 2, name: 'bob' },
        { id: 3, name: 'bob' },
        { id: 4, name: 'bob' }
      ]
    });
  });

  it('Is curried correctly', () => {
    const users = [{ id: 1, name: 'bob' }];
    const arrangeByName = arrangeBy('name');
    expect(arrangeByName).toBeInstanceOf(Function);
    expect(arrangeByName(users)).toEqual({ bob: [{ id: 1, name: 'bob' }] });
  });
});
