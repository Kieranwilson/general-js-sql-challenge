import areaOfTriangle, { InvalidTriangleException } from '../areaOfTriangle';

describe('areaOfTriangle', () => {
  it('Should work for base case', () => {
    expect(areaOfTriangle(24, 30, 18)).toBe(216);
  });

  it('String numbers are fine', () => {
    expect(areaOfTriangle('24', '30', '18')).toBe(216);
  });

  it('Negative numbers fail', () => {
    expect(() => areaOfTriangle(-1, 1, 2)).toThrow(InvalidTriangleException);
  });

  it('Words throw correct error', () => {
    expect(() => areaOfTriangle('one', 'two', 'three')).toThrow(InvalidTriangleException);
  });

  it('Passing less arguments throws error', () => {
    expect(() => areaOfTriangle(1)).toThrow(InvalidTriangleException);
  });

  it('Throws the correct error with incorrect input numbers fail', () => {
    expect(() => areaOfTriangle(-1, -1, -1)).toThrow(InvalidTriangleException);
  });
});
