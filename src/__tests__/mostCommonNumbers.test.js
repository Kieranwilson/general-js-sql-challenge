import mostCommonNumbers from '../mostCommonNumbers';

describe('mostCommonNumbers', () => {
  it('Should be empty for empty array', () => {
    expect(mostCommonNumbers([])).toEqual([]);
  });

  it('Should return all numbers if set once', () => {
    const both = [1, 2, 3, 4];
    // I use spread here incase the function accidentally mutated the array
    expect(mostCommonNumbers([...both])).toEqual(both);
  });

  it('Longer sequence', () => {
    expect(mostCommonNumbers([5, 4, 3, 2, 4, 5, 1, 6, 1, 2, 5, 4])).toEqual([5, 4]);
  });
});
