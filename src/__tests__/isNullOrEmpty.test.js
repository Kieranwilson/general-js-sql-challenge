import isNullOrEmpty from '../isNullOrEmpty';

describe('isNullOrEmpty', () => {
  it('Should return false for a word', () => {
    expect(isNullOrEmpty('test')).toBe(false);
  });

  it('Should return false for number 0', () => {
    expect(isNullOrEmpty(0)).toBe(false);
  });

  it('Should return false for string `0`', () => {
    expect(isNullOrEmpty('0')).toBe(false);
  });

  describe('Falsy values', () => {
    it('Should return true for null', () => {
      expect(isNullOrEmpty(null)).toBe(true);
    });

    it('Should return true for undefined', () => {
      expect(isNullOrEmpty(undefined)).toBe(true);
    });

    it('Should return true for empty string', () => {
      expect(isNullOrEmpty('')).toBe(true);
    });
  });
});
