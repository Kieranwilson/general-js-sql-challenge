import {
  findPrimeFactorization,
  findFactors,
  firstAttempt,
  findDivisorsFromPrimeFactors
} from '../allDivisors';

describe('allDivisors', () => {
  describe('firstAttempt', () => {
    it('Should work for 12', () => {
      expect(firstAttempt(12)).toEqual([1, 2, 3, 4, 6, 12]);
    });

    it('Should work for 1', () => {
      expect(firstAttempt(1)).toEqual([1]);
    });

    it('Should work for a number that has a integer square', () => {
      expect(firstAttempt(16)).toEqual([1, 2, 4, 8, 16]);
    });
  });

  describe('Prime Factorization strategy', () => {
    describe('Finding the prime factorizations', () => {
      it('Prime factorization of 12345 is 3 x 5 x 823', () => {
        expect(findPrimeFactorization(12345)).toEqual([[3, 1], [5, 1], [823, 1]]);
      });

      it('Prime factorization of 100 is 2 x 2 x 5 x 5', () => {
        expect(findPrimeFactorization(100)).toEqual([[2, 2], [5, 2]]);
      });

      it('Prime factorization of 76 is 2 x 2 x 19', () => {
        expect(findPrimeFactorization(76)).toEqual([[2, 2], [19, 1]]);
      });

      it('Prime factorization of 50 is 2 x 5 x 5', () => {
        expect(findPrimeFactorization(50)).toEqual([[2, 1], [5, 2]]);
      });

      it('Prime factorization of 48 is 2 x 2 x 2 x 2 x 3', () => {
        expect(findPrimeFactorization(48)).toEqual([[2, 4], [3, 1]]);
      });

      it('Prime factorization of 36 is 2 x 2 x 3 x 3', () => {
        expect(findPrimeFactorization(36)).toEqual([[2, 2], [3, 2]]);
      });
    });

    describe('Finding divisors from prime factorizations', () => {
      it('Divisors of 36 are correct', () => {
        const expected = [1, 2, 3, 4, 6, 9, 12, 18, 36];
        expect(findDivisorsFromPrimeFactors([[2, 2], [3, 2]])).toEqual(expected);
      });

      it('Divisors of 12345 are correct', () => {
        const expected = [1, 3, 5, 15, 823, 2469, 4115, 12345];
        expect(findDivisorsFromPrimeFactors([[3, 1], [5, 1], [823, 1]])).toEqual(expected);
      });
    });

    it('Should work for 12', () => {
      expect(findFactors(12)).toEqual([1, 2, 3, 4, 6, 12]);
    });

    it('Should work for 1', () => {
      expect(findFactors(1)).toEqual([1]);
    });

    it('Should work for 2', () => {
      expect(findFactors(2)).toEqual([1, 2]);
    });

    it('Should work for 4', () => {
      expect(findFactors(4)).toEqual([1, 2, 4]);
    });

    it('Should work for a number that has a integer square', () => {
      expect(findFactors(16)).toEqual([1, 2, 4, 8, 16]);
    });
  });

  describe('Performance and accuracy', () => {
    let timer;
    const startTimer = () => (timer = process.hrtime());
    const retrieveMsTimer = () => {
      const hrtime = process.hrtime(timer);
      return (hrtime[0] * 1e9 + hrtime[1]) / 1e6;
    };
    const outPutDifference = (first, prime) => {
      const percent = (((first - prime) / first) * 100).toFixed(2);
      percent > 0
        ? console.info(`First timer was ${Math.abs(percent)}% slower`)
        : console.info(`First timer was ${Math.abs(percent)}% faster`);
    };

    it('Firstattempt is faster for low numbers', () => {
      startTimer();
      const firstResult = firstAttempt(5);
      const firstTimer = retrieveMsTimer();
      startTimer();
      const primeResult = findFactors(5);
      const primeTimer = retrieveMsTimer();
      // outPutDifference(firstTimer, primeTimer);
      expect(firstResult).toEqual(primeResult);
      expect(firstTimer).toBeLessThan(primeTimer);
    });

    it('Prime is faster for high numbers', () => {
      startTimer();
      const firstResult = firstAttempt(12345678901234);
      const firstTimer = retrieveMsTimer();
      startTimer();
      const primeResult = findFactors(12345678901234);
      const primeTimer = retrieveMsTimer();
      // outPutDifference(firstTimer, primeTimer);
      expect(firstResult).toEqual(primeResult);
      expect(firstTimer).toBeGreaterThan(primeTimer);
    });
  });
});
