/**
 * Write a function that takes an array of integers, and returns an array of
 * integers. The return value should contain those integers which are most
 * common in the input array.
 */

export default numbers => {
  let maxNum = 0; // current most frequent
  const frequency = {}; // associative array of frequency
  return numbers.reduce((arr, current) => {
    frequency[current] = (frequency[current] || 0) + 1; // increment frequency
    if (frequency[current] > maxNum) {
      // New max
      maxNum++;
      // Reset array
      return [current];
    } else if (frequency[current] === maxNum) {
      // Same as our current max, append to array
      return [...arr, current];
    }
    return arr;
  }, []);
};
